### Master script. Only this file needs to be modified by the user.
### This script calls the other scripts that are required.
### Distributed under CC-BY licence by Johannes Breidenbach, NIBIO, 2023


# Before running this script, download and and install the free and open 
# SFTP-client winSCP in order to download the remote sensing data.

# Replace the variables and path names below with your information and run the 
# whole script - it will run for a while.

# There will be no progress indicator during download of individual files. 
# Just let it run. 

# Each Sentinel 2 tile is about 2GB and the download time depends very much on
# the individual connection.
# For Norway the whole script was running for ca 12h on a normal laptop.

# install the following packages, if not available
library(dplyr)
library(tidyr)
library(sf)
library(sp)
library(terra)

# your 2-digit country code, 
# see https://gitlab.com/nfiesta/pathfinder_demo_study/-/wikis/Codelist%20of%20countries
country_code <- "CZ" 

# prepare a data-frame (YOUR_NFI_COORDINATES_TABLE) with the following columns: 
# strata_set identifier 
# stratum identifier 
# panel identifier
# cluster identifier
# plot identifier
# easting coordinate of the sample plot
# northing coordinate of the sample plot 
# quality or type of coordinate
nfi.coords <- YOUR_NFI_COORDINATES_TABLE

### User-defined variables

# assign the actual names of the columns in your YOUR_NFI_COORDINATES_TABLE   
strataSetID <- "strata_set" # column name for strata_set identifier (usually 
# the same code for all plots unless strata were changed)

stratumID <- "stratum" # column name for strata identifier 

panelID <- "panel" # column name used for panel identifier

clusterID <- "cluster" # column name for cluster identifier 
# ATTENTION: repeat the name of the plotID column here, if your NFI does not use 
# cluster sampling to avoid missing column names

plotID <- "plot" # column name for plot identifier

easting <- "easting"  #column name for east-west coordinate

northing <- "northing"  #column name for north-south coordinate

coordinate_quality <- "quality" #column name for the quality-type of the 
# coordinate. One of the following: 1 for differential gps, 2 for handheld gps, 
# 3 for theoretical coordinate, 4 for other type of assessment (specify other 
# in your data description)
 
# projection of the coordinates: crs string of the coordinates to convert them 
# to the raster projection using the sf package (the current example is used for
# degrees from typical GPS systems)

# examples
# coord_crs_string <- "+proj=longlat +datum=WGS84 +no_defs +type=crs" 
# EPSG codes can be used as well.
# coord_crs_string <- 4326  #The EPSG code for the current string is 4326.

coord_crs_string <- 5514 # CZ example

# Important: when setting path names, remember to use slashes (/) instead of 
# backslashes (\) also when you use Windows!

#path to the SFTP-client winSCP. 
# winscp_path <- "C:/Program Files (x86)/WinSCP/WinSCP.exe"
winscp_path <- "D:/Users/KMAdolt/AppData/Local/Programs/WinSCP/WinSCP.exe" 
# CZ Example
  
# path to folder where output data are stored
# include forward slash at the end of the url
output_folder <- "D:/Users/KMAdolt/tmp/demo_study_cz_data/"

#path to folder in which the scripts are stored
scripts_folder <- "D:/Users/KMAdolt/git/pathfinder_demo_study/auxscripts/"

#path to folder where remote sensing data can be stored. 
# NB: Enough space must be available!
# rs_path <- "S:/Kildedata/53046_PathFinder_Fjernmalingsdata/"
rs_path <- "D:/Users/KMAdolt/tmp/demo_study_cz_data/" 

# sink(paste0(output_folder, "logfile.txt"))
# uncomment, if you get errors you 
# cannot solve. It saves all output to a log-file that you can send to us for 
# error checking.

ech <- F # set to T in case of problems

#download RS data
source(paste0(scripts_folder,"download_RS_data.R"),
      echo=ech, max.deparse.length=1e3)

#extract RS data
source(paste0(scripts_folder,"call_extract.R"),echo=ech, max.deparse.length=1e3)

#sink()