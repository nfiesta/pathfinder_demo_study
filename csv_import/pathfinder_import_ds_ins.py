#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from datetime import datetime

def sweight_check(conn, country_r, dir_version, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    from tabulate import tabulate
    pf_print('%s------------------------------sampling weight check' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    if dir_version == '1_0':
        cmd = '''
with w_check as (
    select
        t_stratum.stratum,
                coalesce(t_stratum.frame_area_ha, t_stratum.area_ha) as area,
        t_panel.panel,
        t_panel.cluster_count,
        t_panel.plot_count,
        t_panel.sweight_panel_sum,
        (
        select sum(sampling_weight_ha)
        from sdesign.cm_cluster2panel_mapping
        where cm_cluster2panel_mapping.panel = t_panel.id) as sweight_panel_sum_computed
    from sdesign.t_panel
    join sdesign.t_stratum on (t_stratum.id = t_panel.stratum)
    join sdesign.t_strata_set on (t_stratum.strata_set = t_strata_set.id)
    join sdesign.c_country on (c_country.id = t_strata_set.country)
    where c_country.label = '%s'
    order by stratum, panel
)
, w_diff as (
    select
        stratum,
        panel,
        area,
        sweight_panel_sum,
        sweight_panel_sum_computed,
        abs((area / sweight_panel_sum) - 1.0) as diff_stored,
        abs((area / sweight_panel_sum_computed) - 1.0) as diff_computed
    from w_check
)
select *
from w_diff
where (diff_stored > 10e-12) or (diff_computed > 10e-12)
order by diff_stored, diff_computed
;
    ''' % (country_r)
    elif (dir_version == '1_5' or dir_version == '2_0'):
        cmd = '''
with w_check as (
    select
        t_stratum.stratum,
                coalesce(t_cluster_configuration.frame_area_ha, t_stratum.area_ha) as area,
        t_panel.panel,
        t_panel.cluster_count,
        t_panel.plot_count,
        t_panel.sweight_panel_sum,
        (
        select sum(sampling_weight_ha)
        from sdesign.cm_cluster2panel_mapping
        where cm_cluster2panel_mapping.panel = t_panel.id) as sweight_panel_sum_computed
    from sdesign.t_panel
    join sdesign.t_stratum on (t_stratum.id = t_panel.stratum)
    join sdesign.t_strata_set on (t_stratum.strata_set = t_strata_set.id)
    join sdesign.c_country on (c_country.id = t_strata_set.country)
        join sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
    where c_country.label = '%s'
    order by stratum, panel
)
, w_diff as (
    select
        stratum,
        panel,
        area,
        sweight_panel_sum,
        sweight_panel_sum_computed,
        abs((area / sweight_panel_sum) - 1.0) as diff_stored,
        abs((area / sweight_panel_sum_computed) - 1.0) as diff_computed
    from w_check
)
select *
from w_diff
where (diff_stored > 10e-12) or (diff_computed > 10e-12)
order by diff_stored, diff_computed
;
    ''' % (country_r)
    out_prl = run_sql(conn, cmd, (None, ))
    if len(out_prl) == 0:
        pf_print('sampling weight check OK', log_f)
    else:
        pf_print('sampling weight check out count %s' % (len(out_prl)), log_f)
        if len(out_prl) < 100:
            #this works in newer tabulate
            #Package: python3-tabulate
            #Version: 0.8.9-0ubuntu1
            pf_print(tabulate(out_prl, headers='keys', tablefmt='orgtbl'), log_f)
            #pf_print(tabulate(out_prl, headers=out_prl[0].keys(), tablefmt='orgtbl'), log_f)
        else:
            pf_print('table showing sampling weight check out written in import_log.txt file', log_f)
            print(tabulate(out_prl, headers='keys', tablefmt='orgtbl'), file=log_f, flush=True)

def temporal_check(conn, country_r, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    from tabulate import tabulate
    pf_print('%s------------------------------temporal check' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    cmd = '''
    with w_data as (
    select 
        t_reference_year_set.reference_year_set,
        t_reference_year_set.reference_date_begin,
        t_reference_year_set.reference_date_end,
        t_plot_measurement_dates.plot,
        t_plot_measurement_dates.measurement_date
    from
    sdesign.c_country
    join sdesign.t_strata_set ON t_strata_set.country = c_country.id
    join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
    join sdesign.t_panel ON t_panel.stratum = t_stratum.id
    join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.panel = t_panel.id
    join sdesign.t_reference_year_set ON t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
    join sdesign.t_plot_measurement_dates on t_plot_measurement_dates.reference_year_set = t_reference_year_set.id
    ---
    join sdesign.f_p_plot ON f_p_plot.gid = t_plot_measurement_dates.plot
    join sdesign.t_cluster ON t_cluster.id = f_p_plot.cluster
    join sdesign.cm_cluster2panel_mapping ON 
        (	cm_cluster2panel_mapping.cluster = t_cluster.id
        and
            cm_cluster2panel_mapping.panel = t_panel.id
        )
    ---
    join sdesign.cm_plot2cluster_config_mapping ON cm_plot2cluster_config_mapping.plot = f_p_plot.gid
    join sdesign.t_cluster_configuration ON 
        (	t_cluster_configuration.id = t_panel.cluster_configuration
         and
            t_cluster_configuration.id = t_panel.cluster_configuration
         )
    where c_country.label = '%s'
)
, w_check as (
    select
        reference_date_begin, reference_date_end,
        count(*) plots_measured,
        sum((measurement_date < (reference_date_begin - interval '6' month)
             or measurement_date > (reference_date_end + interval '6' month))::integer)
        as measurement_date_out_refyearset
    from
    w_data
    group by reference_date_begin, reference_date_end
    order by reference_date_begin, reference_date_end
)
select
    *, 
    case when plots_measured != 0 then
        (measurement_date_out_refyearset::float / plots_measured::float)
    else NULL end as diff
from w_check
where measurement_date_out_refyearset != 0
order by diff nulls first;
    ''' % (country_r)
    out_prl = run_sql(conn, cmd, (None, ))
    if len(out_prl) == 0:
        pf_print('temporal check OK', log_f)
    else:
        pf_print('measurement_date out refyearset count %s' % (len(out_prl)), log_f)
        if len(out_prl) < 100:
            pf_print(tabulate(out_prl, headers='keys', tablefmt='orgtbl'), log_f)
        else:
            pf_print('table showing measurement_date out refyearset written in import_log.txt file', log_f)
            print(tabulate(out_prl, headers='keys', tablefmt='orgtbl'), file=log_f, flush=True)


def available_datasets_check(conn, country_r, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    from tabulate import tabulate
    pf_print('%s------------------------------available datasets check' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    pf_print('checking accordance in plot_target_data.csv <-> plot_target_data_availability.csv', log_f)
    cmd='''
with w_sdesign as (
        select
                c_country.label as country,
                t_strata_set.strata_set,
                t_stratum.stratum,
                t_panel.id as t_panel__id, t_panel.panel,
                t_reference_year_set.id as t_reference_year_set__id, t_reference_year_set.reference_year_set, t_reference_year_set.status_variables,
                t_inventory_campaign.inventory_campaign
        from sdesign.c_country
        join sdesign.t_strata_set                   on t_strata_set.country = c_country.id
        join sdesign.t_stratum                      on t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel                        on t_panel.stratum = t_stratum.id
        join sdesign.cm_refyearset2panel_mapping    on cm_refyearset2panel_mapping.panel = t_panel.id
        join sdesign.t_reference_year_set           on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
        join sdesign.t_inventory_campaign           on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
        --where t_panel.panel = 'y1_s1_s2a' and t_reference_year_set.reference_year_set in ('2011-1', '2016-1')
        where c_country.label = '%s'
        order by t_panel.id, t_reference_year_set.id
)
, w_variables as (
        select distinct
                c_target_variable.metadata->'en'->>'pf_label' as target_variable,
                c_target_variable.metadata->'en'->'state or change'->>'label' as target_variable_type,
                coalesce(c_sub_population.label, 'altogether') as sub_population,
                coalesce(c_area_domain.label, 'altogether') as area_domain
        from nfiesta.t_variable
        join nfiesta.c_target_variable on t_variable.target_variable = c_target_variable.id
        left join nfiesta.c_sub_population_category ON c_sub_population_category.id = t_variable.sub_population_category
        left join nfiesta.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
        left join nfiesta.c_area_domain_category ON c_area_domain_category.id = t_variable.area_domain_category
        left join nfiesta.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
)
, w_plot_target_data_availability as (
        select
                country, strata_set, stratum, panel, inventory_campaign, reference_year_set,
                w_plot_target_data_availability_file.target_variable, w_plot_target_data_availability_file.sub_population, w_plot_target_data_availability_file.area_domain, 
                w_variables.target_variable_type
        from (
            select
                country, strata_set, stratum, panel, inventory_campaign, reference_year_set,
                target_variable, 
                array_to_string(array(select unnest(string_to_array(sub_population, '+')) order by 1), '+') as sub_population,
                array_to_string(array(select unnest(string_to_array(area_domain, '+')) order by 1), '+') as area_domain
            from csv_country.plot_target_data_availability_file
        ) as w_plot_target_data_availability_file
        join w_variables on (
                w_plot_target_data_availability_file.target_variable = w_variables.target_variable and
                w_plot_target_data_availability_file.sub_population = array_to_string(array(select unnest(string_to_array(w_variables.sub_population, '+')) order by 1), '+') and
                w_plot_target_data_availability_file.area_domain = array_to_string(array(select unnest(string_to_array(w_variables.area_domain, '+')) order by 1), '+')
               )
)
, w_available_datasets as (
        select
                w_sdesign.*,
                w_plot_target_data_availability.target_variable,
                w_plot_target_data_availability.sub_population,
                w_plot_target_data_availability.area_domain
        from w_sdesign
        join w_plot_target_data_availability on (
                w_sdesign.country               = w_plot_target_data_availability.country and
                w_sdesign.strata_set            not in (select unnest(string_to_array(substring(w_plot_target_data_availability.strata_set from 'EXCEPT\[(.*)\]'), '+'))) and
                w_sdesign.stratum               not in (select unnest(string_to_array(substring(w_plot_target_data_availability.stratum from 'EXCEPT\[(.*)\]'), '+'))) and
                w_sdesign.panel                 not in (select unnest(string_to_array(substring(w_plot_target_data_availability.panel from 'EXCEPT\[(.*)\]'), '+'))) and
                w_sdesign.inventory_campaign    not in (select unnest(string_to_array(substring(w_plot_target_data_availability.inventory_campaign from 'EXCEPT\[(.*)\]'), '+'))) and
                w_sdesign.reference_year_set    not in (select unnest(string_to_array(substring(w_plot_target_data_availability.reference_year_set from 'EXCEPT\[(.*)\]'), '+')))
        )
        where   ((w_sdesign.status_variables = true) and (target_variable_type = 'state variable')) or 
                ((w_sdesign.status_variables = false) and (target_variable_type = 'change variable')) or
                ((w_sdesign.status_variables = false) and (target_variable_type = 'dynamic variable'))
)
, w_plot_target_data as (
        select
                country, strata_set, stratum, panel,
                inventory_campaign, reference_year_set,
                target_variable,
                array_to_string(array(select unnest(string_to_array(sub_population, '+')) order by 1), '+') as sub_population,
                array_to_string(array(select unnest(string_to_array(area_domain, '+')) order by 1), '+') as area_domain,
                count(*), sum(value)
        from csv_country.plot_target_data
        group by
                country, strata_set, stratum, panel,
                inventory_campaign, reference_year_set,
                target_variable, sub_population, area_domain
)
--select * from w_available_datasets
select
    w_available_datasets.country as ads_country, 
    w_available_datasets.strata_set as ads_strata_set, 
    w_available_datasets.stratum as ads_stratum, 
    w_available_datasets.panel as ads_panel, 
    w_available_datasets.inventory_campaign as ads_inventory_campaign, 
    w_available_datasets.reference_year_set as ads_reference_year_set, 
    w_available_datasets.target_variable as ads_target_variable, 
    w_available_datasets.sub_population as ads_sub_population, 
    w_available_datasets.area_domain as ads_area_domain,
    w_plot_target_data.country as ptd_country, 
    w_plot_target_data.strata_set as ptd_strata_set, 
    w_plot_target_data.stratum as ptd_stratum, 
    w_plot_target_data.panel as ptd_panel, 
    w_plot_target_data.inventory_campaign as ptd_inventory_campaign, 
    w_plot_target_data.reference_year_set as ptd_reference_year_set, 
    w_plot_target_data.target_variable as ptd_target_variable, 
    w_plot_target_data.sub_population as ptd_sub_population, 
    w_plot_target_data.area_domain as ptd_area_domain
from w_available_datasets
full join w_plot_target_data on (
        w_available_datasets.country = w_plot_target_data.country and
        w_available_datasets.strata_set = w_plot_target_data.strata_set and
        w_available_datasets.stratum = w_plot_target_data.stratum and
        w_available_datasets.panel = w_plot_target_data.panel and
        w_available_datasets.inventory_campaign = w_plot_target_data.inventory_campaign and
        w_available_datasets.reference_year_set = w_plot_target_data.reference_year_set and
        w_available_datasets.target_variable = w_plot_target_data.target_variable and
        w_available_datasets.sub_population = w_plot_target_data.sub_population and
        w_available_datasets.area_domain = w_plot_target_data.area_domain
)
where w_plot_target_data.target_variable is null or w_available_datasets.target_variable is null
order by
    w_available_datasets.country, w_available_datasets.strata_set, w_available_datasets.stratum, w_available_datasets.panel, w_available_datasets.inventory_campaign, w_available_datasets.reference_year_set, w_available_datasets.target_variable, w_available_datasets.sub_population, w_available_datasets.area_domain,
    w_plot_target_data.country, w_plot_target_data.strata_set, w_plot_target_data.stratum, w_plot_target_data.panel, w_plot_target_data.inventory_campaign, w_plot_target_data.reference_year_set, w_plot_target_data.target_variable, w_plot_target_data.sub_population, w_plot_target_data.area_domain
;
    ''' % (country_r)
    out_prl = run_sql(conn, cmd, (None, ))
    if len(out_prl) == 0:
        pf_print('available dataset check OK', log_f)
    else:
        pf_print('missing datasets count %s' % (len(out_prl)), log_f)
        out_prl_ads = [token[:9] for token in out_prl if token['ads_target_variable'] != None]
        out_prl_ads_headers = list(out_prl[0].keys())[:9]
        out_prl_ptd = [token[9:] for token in out_prl if token['ptd_target_variable'] != None]
        out_prl_ptd_headers = list(out_prl[0].keys())[:9]
        if len(out_prl) < 100:
            pf_print('defined in plot_target_data_availability.csv but not found in plot_target_data.csv (count %s)' % (len(out_prl_ads)), log_f)
            if len(out_prl_ads) > 0:
                pf_print(tabulate(out_prl_ads, headers=out_prl_ads_headers, tablefmt='orgtbl'), log_f)
            pf_print('found in plot_target_data.csv but not defined in plot_target_data_availability.csv (count %s)' % (len(out_prl_ptd)), log_f)
            if len(out_prl_ptd) > 0:
                pf_print(tabulate(out_prl_ptd, headers=out_prl_ptd_headers, tablefmt='orgtbl'), log_f)
        else:
            pf_print('table with missing datasets written in import_log.txt file', log_f)
            pf_print('defined in plot_target_data_availability.csv but not found in plot_target_data.csv (count %s)' % (len(out_prl_ads)), log_f)
            if len(out_prl_ads) > 0:
                print(tabulate(out_prl_ads, headers=out_prl_ads_headers, tablefmt='orgtbl'), file=log_f, flush=True)
            pf_print('found in plot_target_data.csv but not defined in plot_target_data_availability.csv (count %s)' % (len(out_prl_ptd)), log_f)
            if len(out_prl_ptd) > 0:
                print(tabulate(out_prl_ptd, headers=out_prl_ptd_headers, tablefmt='orgtbl'), file=log_f, flush=True)

def ins_country_sdesign(conn, country_r, dir_version, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    pf_print('==============================country %s insert sdesign data==============================' % country_r, log_f)

    pf_print('%s------------------------------t_cluster_configuration_temp' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    CREATE TEMPORARY TABLE t_cluster_configuration_temp ON COMMIT DROP AS
        SELECT (select coalesce(max(id),0) from sdesign.t_cluster_configuration) + row_number() over() AS id,
            *
        FROM csv_country.cluster_configurations;
    ''', (None, ))
    res = run_sql(conn, 'analyze t_cluster_configuration_temp;', (None, ))

    pf_print('%s------------------------------t_cluster_configuration' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    if dir_version == '1_0':
        res = run_sql(conn, '''
    insert into sdesign.t_cluster_configuration(id, cluster_configuration, cluster_design, cluster_rotation, geom, plots_per_cluster, label, comment)
    select
        id,
        cluster_configuration,
        cluster_design,
        cluster_rotation,
        ST_GeomFromText(cluster_geom)::geometry(MULTIPOINT) as geom,
        case when cluster_design then
            ST_NumGeometries(ST_GeomFromText(cluster_geom)::geometry(MultiPoint))
        else
            1
        end as plots_per_cluster,
        label,
        comment
    from t_cluster_configuration_temp;
        select setval('sdesign.t_cluster_configuration_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster_configuration), FALSE);
        ''', (None, ))
    elif (dir_version == '1_5' or dir_version == '2_0'):
        res = run_sql(conn, '''
insert into sdesign.t_cluster_configuration(id, cluster_configuration, cluster_design, cluster_rotation, geom, plots_per_cluster, label, comment, frame_area_ha)
    select
        id,
        cluster_configuration,
        cluster_design,
        cluster_rotation,
        ST_GeomFromText(cluster_geom)::geometry(MULTIPOINT) as geom,
        case when cluster_design then
            ST_NumGeometries(ST_GeomFromText(cluster_geom)::geometry(MultiPoint))
        else
            1
        end as plots_per_cluster,
        label,
        comment,
                frame_area_ha
    from t_cluster_configuration_temp;
        select setval('sdesign.t_cluster_configuration_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster_configuration), FALSE);
        ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_cluster_configuration;', (None, ))

    pf_print('%s------------------------------t_strata_set' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
        insert into sdesign.t_strata_set(country, strata_set, label, comment)
    select c_country.id, strata_sets.strata_set, strata_sets.label, strata_sets.comment 
    from sdesign.c_country
    inner join csv_country.strata_sets
    on strata_sets.country = c_country.label
    order by country, strata_set;
        ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_strata_set;', (None, ))

    pf_print('%s------------------------------t_stratum' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    if dir_version == '1_0':
        res = run_sql(conn, '''
    insert into sdesign.t_stratum(stratum, area_ha, frame_area_ha, comment, geom, strata_set, label)
    select
        strata.stratum,
        coalesce(strata.area_ha, ST_Area(ST_GeomFromEWKT(strata.geometry))/10000.0) as area_ha,
        strata.frame_area_ha,
        strata.comment,
        ST_GeomFromEWKT(strata.geometry) as geom,
        t_strata_set.id as strata_set,
        strata.label
    from sdesign.c_country
    inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
    inner join csv_country.strata	
        on strata.strata_set = t_strata_set.strata_set
        and strata.country = c_country.label;
    ''', (None, ))
    elif (dir_version == '1_5' or dir_version == '2_0'):
        res = run_sql(conn, '''
    insert into sdesign.t_stratum(stratum, area_ha, comment, geom, strata_set, label)
    select
        strata.stratum,
        coalesce(strata.area_ha, ST_Area(ST_GeomFromEWKT(strata.geometry))/10000.0) as area_ha,
        strata.comment,
        ST_GeomFromEWKT(strata.geometry) as geom,
        t_strata_set.id as strata_set,
        strata.label
    from sdesign.c_country
    inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
    inner join csv_country.strata	
        on strata.strata_set = t_strata_set.strata_set
        and strata.country = c_country.label;
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_stratum;', (None, ))

    pf_print('%s------------------------------disable sum of sampling weights check' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
alter table sdesign.t_panel disable trigger trg__panel__check_threshold__ins;
    ''', (country_r, ))

    pf_print('%s------------------------------t_panel' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    with w_clusters_data as (
        select country, strata_set, stratum, panel, count(*) as cluster_count, sum(sampling_weight_ha) as sweight_panel_sum
        from csv_country.clusters
        group by country, strata_set, stratum, panel order by panel
    )
    , w_plots_data as (
        select country, strata_set, stratum, panel, count(*) as plot_count
        from csv_country.plots
        group by country, strata_set, stratum, panel order by panel
    )
    insert into sdesign.t_panel(stratum, panel, cluster_configuration, label, comment, cluster_count, plot_count, sweight_panel_sum)
    select
        t_stratum.id as stratum,
        panels.panel,
        t_cluster_configuration_temp.id as cluster_configuration,
        panels.label,
        panels.comment,
        w_clusters_data.cluster_count,
        w_plots_data.plot_count,
        w_clusters_data.sweight_panel_sum
    from sdesign.c_country
    inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
    inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
    inner join t_cluster_configuration_temp on c_country.label = t_cluster_configuration_temp.country
    inner join csv_country.panels
        on panels.country = c_country.label
        and panels.strata_set = t_strata_set.strata_set
        and panels.stratum = t_stratum.stratum
        and panels.cluster_configuration = t_cluster_configuration_temp.cluster_configuration
    inner join w_clusters_data
        on panels.country = w_clusters_data.country
        and panels.strata_set = w_clusters_data.strata_set
        and panels.stratum = w_clusters_data.stratum
        and panels.panel = w_clusters_data.panel
    inner join w_plots_data
        on panels.country = w_plots_data.country
        and panels.strata_set = w_plots_data.strata_set
        and panels.stratum = w_plots_data.stratum
        and panels.panel = w_plots_data.panel
    order by panels.strata_set, panels.stratum, panels.panel;
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_panel;', (None, ))

    pf_print('%s------------------------------t_stratum buffers update' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    if dir_version == '1_0':
        res = run_sql(conn, '''
    with w_stratum as (
        SELECT
            t_stratum.id,
            t_stratum.stratum,
            t_stratum.geom,
            t_cluster_configuration.cluster_configuration,
            CASE	WHEN t_cluster_configuration.cluster_configuration in ( '2p-r', '2p', '4p' )
                THEN
                        st_distance(
                            ST_GeometryN(t_cluster_configuration.geom, 1),
                            ST_GeometryN(t_cluster_configuration.geom, 2))::integer
            END AS cluster_distance
        FROM sdesign.t_stratum
        INNER JOIN sdesign.t_panel on (t_stratum.id = t_panel.stratum)
        INNER JOIN sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
        WHERE t_stratum.frame_area_ha IS NULL
    )
    , w_stratum_buff as (
        SELECT
            id,
            stratum,
            CASE 	WHEN cluster_configuration = '2p-r' THEN
                    sdesign.fn_create_buffered_geometry(geom, 100, cluster_distance)
                WHEN cluster_configuration = '2p' THEN
                    sdesign.fn_create_buffered_geometry(geom, 200, cluster_distance)
                WHEN cluster_configuration = '4p' THEN
                    sdesign.fn_create_buffered_geometry(geom, 300, cluster_distance)
                WHEN cluster_configuration = '1p' THEN
                    NULL
                ELSE
                    NULL --sdesign.fn_create_buffered_geometry(geom, 0, NULL::integer)
            END AS buff_geom
        FROM w_stratum
    )
    , w_stratum_buff_area as (
        SELECT
            id,
            stratum,
            buff_geom AS geom_buffered,
            ST_Area(buff_geom)/10000.0 AS area_ha_buffered
        FROM
            w_stratum_buff
        WHERE
            buff_geom IS NOT NULL
    )
    UPDATE sdesign.t_stratum
    SET
        frame_area_ha = w_stratum_buff_area.area_ha_buffered,
        geom_buffered = w_stratum_buff_area.geom_buffered
    FROM w_stratum_buff_area
    WHERE w_stratum_buff_area.id = t_stratum.id;
    ''', (None, ))
    elif (dir_version == '1_5' or dir_version == '2_0'):
        res = run_sql(conn, '''
    with w_stratum as (
        SELECT
            t_stratum.id,
            t_stratum.stratum,
            t_stratum.geom,
                        t_cluster_configuration.id AS cluster_configuration_id,
            t_cluster_configuration.cluster_configuration,
            CASE	WHEN t_cluster_configuration.cluster_configuration in ( '2p-r', '2p', '4p' )
                THEN
                        st_distance(
                            ST_GeometryN(t_cluster_configuration.geom, 1),
                            ST_GeometryN(t_cluster_configuration.geom, 2))::integer
            END AS cluster_distance
        FROM sdesign.t_stratum
        INNER JOIN sdesign.t_panel on (t_stratum.id = t_panel.stratum)
        INNER JOIN sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
        WHERE t_cluster_configuration.frame_area_ha IS NULL
    )
    , w_stratum_buff as (
        SELECT
            id,
            stratum,
                        cluster_configuration_id,
            CASE 	WHEN cluster_configuration = '2p-r' THEN
                    sdesign.fn_create_buffered_geometry(geom, 100, cluster_distance)
                WHEN cluster_configuration = '2p' THEN
                    sdesign.fn_create_buffered_geometry(geom, 200, cluster_distance)
                WHEN cluster_configuration = '4p' THEN
                    sdesign.fn_create_buffered_geometry(geom, 300, cluster_distance)
                WHEN cluster_configuration = '1p' THEN
                    NULL
                ELSE
                    NULL --sdesign.fn_create_buffered_geometry(geom, 0, NULL::integer)
            END AS buff_geom
        FROM w_stratum
    )
    , w_stratum_buff_area as (
        SELECT
            id,
            stratum,
                        cluster_configuration_id,
            buff_geom AS stratum_geom_buffered,
            ST_Area(buff_geom)/10000.0 AS area_ha_buffered
        FROM
            w_stratum_buff
        WHERE
            buff_geom IS NOT NULL
    )
    UPDATE sdesign.t_cluster_configuration
    SET
        frame_area_ha = w_stratum_buff_area.area_ha_buffered,
        stratum_geom_buffered = w_stratum_buff_area.stratum_geom_buffered
    FROM w_stratum_buff_area
    WHERE w_stratum_buff_area.cluster_configuration_id = t_cluster_configuration.id;
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_stratum;', (None, ))

    pf_print('%s------------------------------t_cluster_temp & f_p_plot_temp' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    CREATE TEMPORARY TABLE t_cluster_temp ON COMMIT DROP AS
        SELECT (select coalesce(max(id),0) from sdesign.t_cluster) + row_number() over() AS id,
            *
        FROM csv_country.clusters;
        CREATE TEMPORARY TABLE f_p_plot_temp ON COMMIT DROP AS
        SELECT (select coalesce(max(gid),0) from sdesign.f_p_plot) + row_number() over() AS gid,
            *
        FROM csv_country.plots;
    ''', (None, ))
    res = run_sql(conn, 'analyze t_cluster_temp; analyze f_p_plot_temp;', (None, ))
    pf_print('%s------------------------------t_cluster' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    insert into sdesign.t_cluster (id, cluster, comment)
    select id, cluster, comment
    from t_cluster_temp;
    SELECT setval('sdesign.t_cluster_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster), FALSE);
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_cluster;', (None, ))

    pf_print('%s------------------------------cm_cluster2panel_mapping' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    insert into sdesign.cm_cluster2panel_mapping(panel, cluster, sampling_weight_ha)
    select
        t_panel.id,
        clusters.id,
        clusters.sampling_weight_ha
    from sdesign.c_country
    inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
    inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
    inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
    inner join t_cluster_temp AS clusters
        on clusters.country = c_country.label
        and clusters.strata_set = t_strata_set.strata_set
        and clusters.stratum = t_stratum.stratum
        and clusters.panel = t_panel.panel
        order by clusters.id, t_panel.id
        returning *;
    ''', (None, ))
    pf_print('%s records inserted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze sdesign.cm_cluster2panel_mapping;', (None, ))

    sweight_check(conn, country_r, dir_version, log_f)

    pf_print('%s------------------------------f_p_plot' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    insert into sdesign.f_p_plot(gid, plot, geom, cluster, coordinates_degraded, comment)
    select
        plots.gid,
        plots.plot,
        ST_GeomFromEWKT(plots.plot_geometry) as geom,
        t_cluster.id,
        plots.coordinates_degraded,
        plots.comment
    from sdesign.c_country
    inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
    inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
    inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
    inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
    inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
    inner join f_p_plot_temp AS plots
        on plots.country = c_country.label
        and plots.strata_set = t_strata_set.strata_set
        and plots.stratum = t_stratum.stratum
        and plots.panel = t_panel.panel
        and plots.cluster = t_cluster.cluster
        ;
    SELECT setval('sdesign.f_p_plot_gid_seq', (SELECT coalesce(max(gid),0)+1 FROM sdesign.f_p_plot), FALSE);
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.f_p_plot;', (None, ))

    pf_print('%s------------------------------cm_plot2cluster_config_mapping' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    insert into sdesign.cm_plot2cluster_config_mapping (cluster_configuration, plot)
    select
        t_cluster_configuration.id,
        plots.gid
    from sdesign.c_country
    inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
    inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
    inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
    inner join sdesign.t_cluster_configuration on t_panel.cluster_configuration = t_cluster_configuration.id
    inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
    inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
    inner join f_p_plot_temp AS plots
        on plots.country = c_country.label
        and plots.strata_set = t_strata_set.strata_set
        and plots.stratum = t_stratum.stratum
        and plots.panel = t_panel.panel
        and plots.cluster = t_cluster.cluster
    group by t_cluster_configuration.id, plots.gid
    order by t_cluster_configuration.id, plots.gid;
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.cm_plot2cluster_config_mapping;', (None, ))

    pf_print('%s------------------------------t_inventory_campaign_temp & t_inventory_campaign' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
        CREATE TEMPORARY TABLE t_inventory_campaign_temp ON COMMIT DROP AS
        SELECT (select coalesce(max(id),0) from sdesign.t_inventory_campaign) + row_number() over() AS id,
            *
        FROM csv_country.inventory_campaigns;
    insert into sdesign.t_inventory_campaign(id, inventory_campaign, label, status_variables, comment)
        select
            id, inventory_campaign, label, status_variables, comment
        from t_inventory_campaign_temp
    ;
    SELECT setval('sdesign.t_inventory_campaign_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_inventory_campaign), FALSE);
    ''', (None, ))
    res = run_sql(conn, 'analyze t_inventory_campaign_temp; analyze sdesign.t_inventory_campaign;', (None, ))

    pf_print('%s------------------------------t_reference_year_set' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    with w_insert as (
        insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment)
        select
            t_inventory_campaign_temp.id,
            reference_year_set,
            reference_date_begin,
            reference_date_end,
            reference_year_sets.label,
            reference_year_sets.status_variables,
            reference_year_sets.comment
        from sdesign.c_country
        inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
        inner join csv_country.reference_year_sets
            on reference_year_sets.country = t_inventory_campaign_temp.country
            and reference_year_sets.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
        where reference_year_sets.reference_year_set_begin is null and reference_year_sets.reference_year_set_end is null
        order by reference_year_set
        returning id, inventory_campaign, reference_year_set
    )
    , w_insert_country as (
        select 
            w_insert.id, t_inventory_campaign_temp.inventory_campaign, w_insert.reference_year_set, t_inventory_campaign_temp.country
        from w_insert
        inner join t_inventory_campaign_temp on (w_insert.inventory_campaign = t_inventory_campaign_temp.id)
    )	
    insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    select
        t_inventory_campaign_temp.id,
        reference_year_sets.reference_year_set,
        reference_year_sets.reference_date_begin,
        reference_year_sets.reference_date_end,
        reference_year_sets.label,
        reference_year_sets.status_variables,
        reference_year_sets.comment,
        (select id from w_insert_country where 
            w_insert_country.reference_year_set 	= reference_year_sets.reference_year_set_begin and 
            w_insert_country.inventory_campaign 	= reference_year_sets.inventory_campaign_begin and
            w_insert_country.country 		= reference_year_sets.country
        ),
        (select id from w_insert_country where 
            w_insert_country.reference_year_set 	= reference_year_sets.reference_year_set_end and 
            w_insert_country.inventory_campaign 	= reference_year_sets.inventory_campaign_end and
            w_insert_country.country 		= reference_year_sets.country
        )
    from sdesign.c_country
    inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
    inner join csv_country.reference_year_sets
        on reference_year_sets.country = t_inventory_campaign_temp.country
        and reference_year_sets.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
    where reference_year_sets.reference_year_set_begin is not null and reference_year_sets.reference_year_set_end is not null
    order by reference_year_sets.reference_year_set;
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_reference_year_set;', (None, ))

    pf_print('%s------------------------------cm_refyearset2panel_mapping' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    with w_data as (
        select
            t_panel.id as t_panel__id,
            t_reference_year_set.id as t_reference_year_set__id,
            refyearset2panel.comment
        from sdesign.c_country
        inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
        inner join sdesign.t_reference_year_set on t_inventory_campaign_temp.id = t_reference_year_set.inventory_campaign
        inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
        inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
        inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
        inner join csv_country.refyearset2panel
            on refyearset2panel.country = c_country.label
            and refyearset2panel.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
            and refyearset2panel.reference_year_set = t_reference_year_set.reference_year_set
            and refyearset2panel.strata_set = t_strata_set.strata_set
            and refyearset2panel.stratum = t_stratum.stratum
            and refyearset2panel.panel = t_panel.panel
    )
    insert into sdesign.cm_refyearset2panel_mapping (panel, reference_year_set,comment)
    select t_panel__id, t_reference_year_set__id, comment from w_data
    order by t_panel__id, t_reference_year_set__id, comment;
    ;
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.cm_refyearset2panel_mapping;', (None, ))

    pf_print('%s------------------------------t_plot_measurement_dates' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
--explain analyze
with w_data as materialized (
    select
        c_country.label as country,
        t_strata_set.strata_set,
        t_stratum.stratum,
        t_panel.panel,
        t_cluster.cluster,
        f_p_plot.plot, f_p_plot.gid as f_p_plot__gid,
        t_inventory_campaign.inventory_campaign,
        t_reference_year_set.reference_year_set, t_reference_year_set.id as t_reference_year_set__id
    from sdesign.c_country
    join sdesign.t_strata_set                       on t_strata_set.country = c_country.id
    join sdesign.t_stratum                          on t_stratum.strata_set = t_strata_set.id
    join sdesign.t_panel                            on t_panel.stratum = t_stratum.id
    join sdesign.cm_cluster2panel_mapping           on cm_cluster2panel_mapping.panel = t_panel.id
    join sdesign.t_cluster                          on t_cluster.id = cm_cluster2panel_mapping.cluster
    join sdesign.f_p_plot                           on f_p_plot.cluster = t_cluster.id
    join sdesign.t_cluster_configuration            on t_cluster_configuration.id = t_panel.cluster_configuration
    join sdesign.cm_plot2cluster_config_mapping     on (
        cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
        cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
    join sdesign.cm_refyearset2panel_mapping        on cm_refyearset2panel_mapping.panel = t_panel.id
    join sdesign.t_reference_year_set               on t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
    join sdesign.t_inventory_campaign               on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
    where c_country.label = %s
)
insert into sdesign.t_plot_measurement_dates(plot, reference_year_set, measurement_date, comment)
select
    w_data.f_p_plot__gid, w_data.t_reference_year_set__id,
    t.measurement_date, t.comment
from w_data
join csv_country.plot_measurement_dates as t               on (
        w_data.country = t.country and
        w_data.strata_set = t.strata_set and
        w_data.stratum = t.stratum and
        w_data.panel = t.panel and
        w_data.cluster = t.cluster and
        w_data.plot = t.plot and
        w_data.inventory_campaign = t.inventory_campaign and
        w_data.reference_year_set = t.reference_year_set)
returning *
;
    ''', (country_r, ))
    pf_print('%s records inserted, %s records in CSV' % (len(res), run_sql(conn, 'select count(*) from csv_country.plot_measurement_dates;', (None, ))[0]['count']), log_f)
    res = run_sql(conn, 'analyze sdesign.t_plot_measurement_dates;', (None, ))
    temporal_check(conn, country_r, log_f)

    pf_print('%s------------------------------SEQUENCE SET' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    -- Name: c_country_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.c_country_id_seq', (select max(id) from sdesign.c_country), true);

    -- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.cm_cluster2panel_mapping_id_seq', (select max(id) from sdesign.cm_cluster2panel_mapping), true);

    -- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.cm_plot2cluster_config_mapping_id_seq', (select max(id) from sdesign.cm_plot2cluster_config_mapping), true);

    -- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.cm_refyearset2panel_mapping_id_seq', (select max(id) from sdesign.cm_refyearset2panel_mapping), true);

    -- Name: f_p_plot_gid_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.f_p_plot_gid_seq', (select max(gid) from sdesign.f_p_plot), true);

    -- Name: t_cluster_configuration_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_cluster_configuration_id_seq', (select max(id) from sdesign.t_cluster_configuration), true);

    -- Name: t_cluster_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_cluster_id_seq', (select max(id) from sdesign.t_cluster), true);

    -- Name: t_inventory_campaign_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_inventory_campaign_id_seq', (select max(id) from sdesign.t_inventory_campaign), true);

    -- Name: t_panel_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_panel_id_seq', (select max(id) from sdesign.t_panel), true);

    -- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_plot_measurement_dates_id_seq', (select max(id) from sdesign.t_plot_measurement_dates), true);

    -- Name: t_reference_year_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_reference_year_set_id_seq', (select max(id) from sdesign.t_reference_year_set), true);

    -- Name: t_strata_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_strata_set_id_seq', (select max(id) from sdesign.t_strata_set), true);

    -- Name: t_stratum_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
    select pg_catalog.setval('sdesign.t_stratum_id_seq', (select max(id) from sdesign.t_stratum), true);
    ''', (None, ))

def cells_in_check(conn, country_r, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    from tabulate import tabulate
    pf_print('%s------------------------------cells intersects strata check' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    select
        c_country.label as c_country__label, t_stratum.stratum as t_stratum__stratum, t_stratum.id as t_stratum__id
    from sdesign.c_country
    join sdesign.t_strata_set ON t_strata_set.country = c_country.id
    join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
    where c_country.label = %s
    ''', (country_r, ))
    for r in res:
        pf_print('checking stratum %s' % r['t_stratum__stratum'], log_f)
        cmd = '''
with w_data as materialized (
    select
        c_country.label as c_country__label, t_stratum.stratum as t_stratum__stratum, t_stratum.id as t_stratum__id,
        c_estimation_cell.id as c_estimation_cell__id, c_estimation_cell.label as c_estimation_cell__label,
        json_agg(json_build_object('f_p_plot__plot', f_p_plot.plot, 't_cluster__cluster', t_cluster.cluster)) as clusters__plots
    from sdesign.c_country
    join sdesign.t_strata_set ON t_strata_set.country = c_country.id
    join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
    join sdesign.t_panel ON t_panel.stratum = t_stratum.id
    join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
    join sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
    join sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
    join nfiesta.cm_plot2cell_mapping on cm_plot2cell_mapping.plot = f_p_plot.gid
    join nfiesta.c_estimation_cell ON c_estimation_cell.id = cm_plot2cell_mapping.estimation_cell
    where c_country.label = '%s' and t_stratum.stratum = '%s'
    group by c_country.label, t_stratum.stratum, t_stratum.id, c_estimation_cell.id--, f_a_cell.geom
)
, w_intersection as (
    select
        w_data.c_country__label, w_data.t_stratum__stratum,
        w_data.c_estimation_cell__label, clusters__plots,
        t_stratum.geom as t_stratum__geom, f_a_cell.geom as f_a_cell__geom,
        st_intersects(t_stratum.geom, f_a_cell.geom) as cell_intersects_stratum
    from w_data
    join sdesign.t_stratum on w_data.t_stratum__id = t_stratum.id
    join nfiesta.f_a_cell on f_a_cell.estimation_cell = c_estimation_cell__id
)
, w_report as (
    select
        c_country__label, t_stratum__stratum, count(*) as checked,
        json_agg(
            json_build_object(	'c_estimation_cell__label', c_estimation_cell__label,
                        'clusters__plots', clusters__plots,
                        'distance', st_distance(t_stratum__geom, f_a_cell__geom)
            ) order by st_distance(t_stratum__geom, f_a_cell__geom)) filter (where not cell_intersects_stratum) as cells_out
    from w_intersection
    group by c_country__label, t_stratum__stratum
)
select 
    c_country__label, t_stratum__stratum, checked,
    jsonb_pretty(cells_out::jsonb) as cells_out
from w_report
;        ''' % (country_r, r['t_stratum__stratum'])
        out_prl = run_sql(conn, cmd, (None, ))
        if len(out_prl) == 0:
            pf_print('cells intersects strata OK', log_f)
        else:
            pf_print('cells intersects strata out count %s' % (len(out_prl)), log_f)
            if len(out_prl) < 100:
                pf_print(tabulate(out_prl, headers='keys', tablefmt='orgtbl'), log_f)
            else:
                pf_print('table with cells intersects strata out written in import_log.txt file', log_f)
                print(tabulate(out_prl, headers='keys', tablefmt='orgtbl'), file=log_f, flush=True)

def ins_country_nfiesta_target_data_duckdb():
    import duckdb
    duckdb.install_extension('postgres')
    duckdb.load_extension("postgres")
    duckdb.sql('''ATTACH 'dbname=pathfinder_ds host=db' AS pgdb (TYPE postgres);''')
    q='''   select
                c_country.label as country,
                t_strata_set.strata_set,
                t_stratum.stratum,
                t_panel.panel, t_panel.id as t_panel__id,
                t_cluster.cluster,
                f_p_plot.plot, f_p_plot.gid as f_p_plot__gid,
                t_inventory_campaign.inventory_campaign,
                t_reference_year_set.reference_year_set, t_reference_year_set.id as t_reference_year_set__id
            from sdesign.c_country
            join sdesign.t_strata_set                       on t_strata_set.country = c_country.id
            join sdesign.t_stratum                          on t_stratum.strata_set = t_strata_set.id
            join sdesign.t_panel                            on t_panel.stratum = t_stratum.id
            join sdesign.cm_cluster2panel_mapping           on cm_cluster2panel_mapping.panel = t_panel.id
            join sdesign.t_cluster                          on t_cluster.id = cm_cluster2panel_mapping.cluster
            join sdesign.f_p_plot                           on f_p_plot.cluster = t_cluster.id
            join sdesign.t_cluster_configuration            on t_cluster_configuration.id = t_panel.cluster_configuration
            join sdesign.cm_plot2cluster_config_mapping     on (
            cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
            cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
            join sdesign.cm_refyearset2panel_mapping        on cm_refyearset2panel_mapping.panel = t_panel.id                                                   
            join sdesign.t_reference_year_set               on t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
            join sdesign.t_inventory_campaign               on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
            where c_country.label = ''%s''
            ;''' % (country_r)
    w_sdesign = duckdb.sql('''SELECT * FROM postgres_query('pgdb', '%s');''' % (q))

    q = ''' select
                country, strata_set, stratum, panel, cluster, plot,
                inventory_campaign, reference_year_set,
                target_variable, 
                array(select unnest(string_to_array(coalesce(sub_population, ''altogether''), ''+'')) order by 1) as sub_population, 
                array(select unnest(string_to_array(coalesce(sub_population_category, ''altogether''), ''+'')) order by 1) as sub_population_category, 
                array(select unnest(string_to_array(coalesce(area_domain, ''altogether''), ''+'')) order by 1) as area_domain, 
                array(select unnest(string_to_array(coalesce(area_domain_category, ''altogether''), ''+'')) order by 1) as area_domain_category, 
                value
            from csv_country.plot_target_data
            where value != 0
            ;'''
    w_data_csv = duckdb.sql('''SELECT * FROM postgres_query('pgdb', '%s');''' % (q))

    q= '''  select
                t_variable.id as t_variable__id, c_target_variable.metadata->''en''->>''pf_label'' as target_variable,
                array(select unnest(string_to_array(coalesce(c_sub_population.label, ''altogether''), ''+'')) order by 1) as sub_population, 
                array(select unnest(string_to_array(coalesce(c_sub_population_category.label, ''altogether''), ''+'')) order by 1) as sub_population_category,
                array(select unnest(string_to_array(coalesce(c_area_domain.label, ''altogether''), ''+'')) order by 1) as area_domain, 
                array(select unnest(string_to_array(coalesce(c_area_domain_category.label, ''altogether''), ''+'')) order by 1) as area_domain_category
            from nfiesta.t_variable
            join nfiesta.c_target_variable on t_variable.target_variable = c_target_variable.id
            left join nfiesta.c_sub_population_category ON c_sub_population_category.id = t_variable.sub_population_category
            left join nfiesta.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
            left join nfiesta.c_area_domain_category ON c_area_domain_category.id = t_variable.area_domain_category
            left join nfiesta.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
            ;'''
    w_variables = duckdb.sql('''SELECT * FROM postgres_query('pgdb', '%s');''' % (q))

    q = '''
        with w_data as (
            select
                t_panel__id as panel,
                t_reference_year_set__id as reference_year_set,
                t_variable__id as variable,
                w_sdesign.f_p_plot__gid,
                w_data_csv.value
            from w_sdesign
            join w_data_csv on (
                w_sdesign.country               = w_data_csv.country and
                w_sdesign.strata_set            = w_data_csv.strata_set and
                w_sdesign.stratum               = w_data_csv.stratum and
                w_sdesign.panel                 = w_data_csv.panel and
                w_sdesign.cluster               = w_data_csv.cluster and
                w_sdesign.plot                  = w_data_csv.plot and
                w_sdesign.inventory_campaign    = w_data_csv.inventory_campaign and
                w_sdesign.reference_year_set    = w_data_csv.reference_year_set)
            join w_variables on (
                w_variables.target_variable = w_data_csv.target_variable
                and (w_variables.sub_population = w_data_csv.sub_population)
                and (w_variables.sub_population_category = w_data_csv.sub_population_category)
                and (w_variables.area_domain = w_data_csv.area_domain)
                and (w_variables.area_domain_category = w_data_csv.area_domain_category))
        )
        insert into pgdb.nfiesta.t_target_data(plot, available_datasets, value)
        select
            w_data.f_p_plot__gid,
            t_available_datasets.id,
            w_data.value
        from w_data
        join pgdb.nfiesta.t_available_datasets               on (
            t_available_datasets.variable = w_data.variable and
            t_available_datasets.panel = w_data.panel and
            t_available_datasets.reference_year_set = w_data.reference_year_set)
        --returning *
        ;
        '''
    w_ins = duckdb.sql(q)
    duckdb.sql('''DETACH pgdb;''')

def ins_country_nfiesta(conn, country_r, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    pf_print('==============================country %s insert nfiesta data==============================' % country_r, log_f)

    pf_print('%s------------------------------cm_plot2cell_mapping' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    --explain analyze
    with w_data as materialized (
        select
            c_country.label as country,
            t_strata_set.strata_set,
            t_stratum.stratum,
            t_panel.panel,
            t_cluster.cluster,
            f_p_plot.plot, f_p_plot.gid as f_p_plot__gid
        from sdesign.c_country
        join sdesign.t_strata_set                       on t_strata_set.country = c_country.id
        join sdesign.t_stratum                          on t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel                            on t_panel.stratum = t_stratum.id
        join sdesign.cm_cluster2panel_mapping           on cm_cluster2panel_mapping.panel = t_panel.id
        join sdesign.t_cluster                          on t_cluster.id = cm_cluster2panel_mapping.cluster
        join sdesign.f_p_plot                           on f_p_plot.cluster = t_cluster.id
        join sdesign.t_cluster_configuration            on t_cluster_configuration.id = t_panel.cluster_configuration
        join sdesign.cm_plot2cluster_config_mapping     on (
            cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
            cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
        where c_country.label = %s
    )
    insert into nfiesta.cm_plot2cell_mapping(plot, estimation_cell)
    select
        w_data.f_p_plot__gid,
        c_estimation_cell.id
    from w_data
    join csv_country.plot_cell_associations as t                on (
            w_data.country = t.country and
            w_data.strata_set = t.strata_set and
            w_data.stratum = t.stratum and
            w_data.panel = t.panel and
            w_data.cluster = t.cluster and
            w_data.plot = t.plot)
    join nfiesta.c_estimation_cell                      on (c_estimation_cell.label = t.cell)
            join nfiesta.c_estimation_cell_collection   on (
                c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id and
                c_estimation_cell_collection.label = t.cell_collection)
    returning *
    ;
    ''', (country_r, ))
    pf_print('imported rows %s' % len(res), log_f)
    res = run_sql(conn, 'analyze nfiesta.cm_plot2cell_mapping;', (None, ))

    pf_print('%s------------------------------disable plot-level additivity checks evaluation' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    alter table nfiesta.t_additivity_set_plot disable trigger trg__additivity_set_plot__ins;
    ''', (country_r, ))

    pf_print('%s------------------------------t_available_datasets' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    with w_data as (
        with w_sdesign as (
            select
                c_country.label as country,
                t_strata_set.strata_set,
                t_stratum.stratum,
                t_panel.id as t_panel__id, t_panel.panel,
                t_reference_year_set.id as t_reference_year_set__id, t_reference_year_set.reference_year_set, --t_reference_year_set.status_variables,
                t_inventory_campaign.inventory_campaign
            from sdesign.c_country
            join sdesign.t_strata_set                   on t_strata_set.country = c_country.id
            join sdesign.t_stratum                      on t_stratum.strata_set = t_strata_set.id
            join sdesign.t_panel                        on t_panel.stratum = t_stratum.id
            join sdesign.cm_refyearset2panel_mapping    on cm_refyearset2panel_mapping.panel = t_panel.id
            join sdesign.t_reference_year_set           on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
            join sdesign.t_inventory_campaign           on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
            --where t_panel.panel = 'y1_s1_s2a' and t_reference_year_set.reference_year_set = '2011-1'
            where c_country.label = %s
            order by t_panel.id, t_reference_year_set.id
        )
        , w_data_csv as (
            /*
            select 
                country, strata_set, stratum, panel, inventory_campaign, reference_year_set,
                target_variable, sub_population, area_domain
            from csv_country.plot_target_data_availability_categories
            see 
            checking accordance in plot_target_data.csv <-> plot_target_data_availability.csv
            https://gitlab.com/nfiesta/pathfinder_demo_study/-/blob/main/csv_import/pathfinder_import_ds_ins.py#L180
            */        
            select
                country, strata_set, stratum, panel, 
                inventory_campaign, reference_year_set,
                target_variable, string_to_array(sub_population, '+') as sub_population, string_to_array(area_domain, '+') as area_domain, count(*)
            from csv_country.plot_target_data
            group by 
                country, strata_set, stratum, panel, 
                inventory_campaign, reference_year_set,
                target_variable, sub_population, area_domain
            order by 
                country, strata_set, stratum, panel, 
                inventory_campaign, reference_year_set,
                target_variable, sub_population, area_domain
        )
        , w_variables as (
            select
                t_variable.id as t_variable__id, c_target_variable.metadata->'en'->>'pf_label' as target_variable,
                string_to_array(coalesce(c_sub_population.label, 'altogether'), '+') as sub_population, --string_to_array(c_sub_population_category.label, '+') as sub_population_category,
                string_to_array(coalesce(c_area_domain.label, 'altogether'), '+') as area_domain--, string_to_array(c_area_domain_category.label, '+') as area_domain_category
            from nfiesta.t_variable
            join nfiesta.c_target_variable on t_variable.target_variable = c_target_variable.id
            left join nfiesta.c_sub_population_category ON c_sub_population_category.id = t_variable.sub_population_category
            left join nfiesta.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
            left join nfiesta.c_area_domain_category ON c_area_domain_category.id = t_variable.area_domain_category
            left join nfiesta.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
            order by t_variable.id
        )
        select
                t_panel__id as panel,
                t_reference_year_set__id as reference_year_set,
                --w_data_csv.target_variable
                t_variable__id as variable
        from w_sdesign -- panels and reference_year_sets from sdesign
        join w_data_csv on ( -- are combined with target variables from plot_target_data
            w_sdesign.country                = w_data_csv.country and
            w_sdesign.strata_set             = w_data_csv.strata_set and
            w_sdesign.stratum                = w_data_csv.stratum and
            w_sdesign.panel                  = w_data_csv.panel and
            w_sdesign.inventory_campaign     = w_data_csv.inventory_campaign and
            w_sdesign.reference_year_set     = w_data_csv.reference_year_set
            )
        join w_variables on ( -- and combined with all sub_population_categories and area_domain_categories available in t_variable
            w_variables.target_variable = w_data_csv.target_variable
            and ((w_variables.sub_population <@ w_data_csv.sub_population) and (w_variables.sub_population @> w_data_csv.sub_population))
            and ((w_variables.area_domain <@ w_data_csv.area_domain) and (w_variables.area_domain @> w_data_csv.area_domain))
        )
    )
    , w_data_aux as (
            select
                    t_panel.id as panel,
                    tv.id as variable
            from sdesign.c_country
            join sdesign.t_strata_set                       on t_strata_set.country = c_country.id
            join sdesign.t_stratum                          on t_stratum.strata_set = t_strata_set.id
            join sdesign.t_panel                            on t_panel.stratum = t_stratum.id
            ----------------------------------------
            join (
                select
                    country, strata_set, stratum, panel,
                    auxiliary_variable--, auxiliary_variable_category --, sub_population--, count(*)
                from csv_country.plot_auxiliary_data
                group by country, strata_set, stratum, panel,
                auxiliary_variable--, auxiliary_variable_category
            ) as t                on (
                c_country.label = t.country and
                t_strata_set.strata_set = t.strata_set and
                t_stratum.stratum = t.stratum and
                t_panel.panel = t.panel
            )
            ----------------------------------------
            join (
                select
                    t_variable.id, c_auxiliary_variable.label as auxiliary_variable
                from nfiesta.t_variable
                join nfiesta.c_auxiliary_variable_category on t_variable.auxiliary_variable_category = c_auxiliary_variable_category.id
                join nfiesta.c_auxiliary_variable on c_auxiliary_variable.id = c_auxiliary_variable_category.auxiliary_variable
            ) as tv                            on (
                tv.auxiliary_variable = t.auxiliary_variable
            )
    )
    insert into nfiesta.t_available_datasets(panel, reference_year_set, variable, ldsity_threshold, last_change)
    select
            panel, reference_year_set, variable, 0.0000000001 as ldsity_threshold, now() as last_change
    from w_data
    union all
    select
            panel, null as reference_year_set, variable, 0.0000000001 as ldsity_threshold, now() as last_change
    from w_data_aux
    order by panel, reference_year_set, variable, ldsity_threshold, last_change
    returning *
    ;
    ''', (country_r, ))
    pf_print('imported rows %s' % len(res), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_available_datasets;', (None, ))
    
    pf_print('%s------------------------------commit changes' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    conn.commit()

    pf_print('%s------------------------------t_auxiliary_data' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    --explain analyze
    with w_data as materialized (
        select
            c_country.label as country,
            t_strata_set.strata_set,
            t_stratum.stratum,
            t_panel.panel, t_panel.id as t_panel__id,
            t_cluster.cluster,
            f_p_plot.plot, f_p_plot.gid as f_p_plot__gid
        from sdesign.c_country
        join sdesign.t_strata_set                       on t_strata_set.country = c_country.id
        join sdesign.t_stratum                          on t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel                            on t_panel.stratum = t_stratum.id
        join sdesign.cm_cluster2panel_mapping           on cm_cluster2panel_mapping.panel = t_panel.id
        join sdesign.t_cluster                          on t_cluster.id = cm_cluster2panel_mapping.cluster
        join sdesign.f_p_plot                           on f_p_plot.cluster = t_cluster.id
        join sdesign.t_cluster_configuration            on t_cluster_configuration.id = t_panel.cluster_configuration
        join sdesign.cm_plot2cluster_config_mapping     on (
            cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
            cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
        where c_country.label = %s
    )
    , w_plot_auxiliary_data as materialized (
        with w_plot_auxiliary_data_tiles as (
            select
                country, strata_set, stratum, panel, cluster, plot, auxiliary_variable, auxiliary_variable_category,
                array_agg(tile order by tile) as tiles, array_agg(value order by tile) as values,
                count(*)
            from csv_country.plot_auxiliary_data
            where
                auxiliary_variable in ('tcd', 'fty', 'gfc', 'coordinate_quality') and 
                auxiliary_variable_category not in ('n.pixels','sum.weights','n.pixels.all','sum.weights.all') and
                plot_auxiliary_data.value != 0
            group by
            country, strata_set, stratum, panel, cluster, plot, auxiliary_variable, auxiliary_variable_category
        )
        select
            country, strata_set, stratum, panel, cluster, plot,
            t_variable.id as t_variable__id,
            values[1] as value
        from w_plot_auxiliary_data_tiles
        join nfiesta.c_auxiliary_variable_category on (c_auxiliary_variable_category.label = w_plot_auxiliary_data_tiles.auxiliary_variable_category)
        join nfiesta.c_auxiliary_variable on (c_auxiliary_variable.label = w_plot_auxiliary_data_tiles.auxiliary_variable)
        join nfiesta.t_variable on (t_variable.auxiliary_variable_category = c_auxiliary_variable_category.id)
    )
    insert into nfiesta.t_auxiliary_data(plot, available_datasets, value)
    select
        w_data.f_p_plot__gid,
        t_available_datasets.id,
        t.value
    from w_data
    join w_plot_auxiliary_data as t                on (
            w_data.country = t.country and
            w_data.strata_set = t.strata_set and
            w_data.stratum = t.stratum and
            w_data.panel = t.panel and
            w_data.cluster = t.cluster and
            w_data.plot = t.plot)
    join nfiesta.t_available_datasets               on (t_available_datasets.variable = t.t_variable__id and t_available_datasets.panel = w_data.t_panel__id)
    returning *
    ;
    ''', (country_r, ))
    counts = run_sql(conn, 'select count(*), sum((value!=0)::int) as nonzero from csv_country.plot_auxiliary_data;', (None, ))[0]
    pf_print('%s records inserted, %s records in CSV (%s non-zero)' % (len(res), counts['count'], counts['nonzero']), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_auxiliary_data;', (None, ))

    pf_print('%s------------------------------t_target_data' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    counts = run_sql(conn, 'select count(*), sum((value!=0)::int) as nonzero from csv_country.plot_target_data;', (None, ))[0]
    pf_print('%s non-zero plots to be inserted' % (counts['nonzero']), log_f)

    res = run_sql(conn, '''
    --EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
    CREATE TEMPORARY TABLE w_sdesign ON COMMIT DROP AS
        select
            c_country.label as country,
            t_strata_set.strata_set,
            t_stratum.stratum,
            t_panel.panel, t_panel.id as t_panel__id,
            t_cluster.cluster,
            f_p_plot.plot, f_p_plot.gid as f_p_plot__gid,
            t_inventory_campaign.inventory_campaign,
            t_reference_year_set.reference_year_set, t_reference_year_set.id as t_reference_year_set__id
        from sdesign.c_country
        join sdesign.t_strata_set                       on t_strata_set.country = c_country.id
        join sdesign.t_stratum                          on t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel                            on t_panel.stratum = t_stratum.id
        join sdesign.cm_cluster2panel_mapping           on cm_cluster2panel_mapping.panel = t_panel.id
        join sdesign.t_cluster                          on t_cluster.id = cm_cluster2panel_mapping.cluster
        join sdesign.f_p_plot                           on f_p_plot.cluster = t_cluster.id
        join sdesign.t_cluster_configuration            on t_cluster_configuration.id = t_panel.cluster_configuration
        join sdesign.cm_plot2cluster_config_mapping     on (
        cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id and
        cm_plot2cluster_config_mapping.plot = f_p_plot.gid)
        join sdesign.cm_refyearset2panel_mapping        on cm_refyearset2panel_mapping.panel = t_panel.id                                                   
        join sdesign.t_reference_year_set               on t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
        join sdesign.t_inventory_campaign               on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
        where c_country.label = %s
        ;
    ANALYZE w_sdesign;
    --select count(*) from w_sdesign;
    --CREATE INDEX idx_w_sdesign_sdesign ON w_sdesign USING BTREE (country, strata_set, stratum, panel, cluster, plot,inventory_campaign, reference_year_set);

    CREATE TEMPORARY TABLE w_data_csv ON COMMIT DROP AS
        select
            country, strata_set, stratum, panel, cluster, plot,
            inventory_campaign, reference_year_set,
            target_variable, 
            array(select unnest(string_to_array(coalesce(sub_population, 'altogether'), '+')) order by 1) as sub_population, 
            array(select unnest(string_to_array(coalesce(sub_population_category, 'altogether'), '+')) order by 1) as sub_population_category, 
            array(select unnest(string_to_array(coalesce(area_domain, 'altogether'), '+')) order by 1) as area_domain, 
            array(select unnest(string_to_array(coalesce(area_domain_category, 'altogether'), '+')) order by 1) as area_domain_category, 
            value
        from csv_country.plot_target_data
        where value != 0
        ;

    ANALYZE w_data_csv;
    --select count(*) from w_data_csv;
    --CREATE INDEX idx_w_data_csv_sdesign ON w_data_csv USING BTREE (country, strata_set, stratum, panel, cluster, plot,inventory_campaign, reference_year_set);
    CREATE INDEX idx_w_data_csv_var ON w_data_csv USING BTREE (target_variable, sub_population, sub_population_category, area_domain_category);

    CREATE TEMPORARY TABLE w_variables ON COMMIT DROP AS
        select
            t_variable.id as t_variable__id, c_target_variable.metadata->'en'->>'pf_label' as target_variable,
            array(select unnest(string_to_array(coalesce(c_sub_population.label, 'altogether'), '+')) order by 1) as sub_population, 
            array(select unnest(string_to_array(coalesce(c_sub_population_category.label, 'altogether'), '+')) order by 1) as sub_population_category,
            array(select unnest(string_to_array(coalesce(c_area_domain.label, 'altogether'), '+')) order by 1) as area_domain, 
            array(select unnest(string_to_array(coalesce(c_area_domain_category.label, 'altogether'), '+')) order by 1) as area_domain_category
        from nfiesta.t_variable
        join nfiesta.c_target_variable on t_variable.target_variable = c_target_variable.id
        left join nfiesta.c_sub_population_category ON c_sub_population_category.id = t_variable.sub_population_category
        left join nfiesta.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
        left join nfiesta.c_area_domain_category ON c_area_domain_category.id = t_variable.area_domain_category
        left join nfiesta.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
        ;
    ANALYZE w_variables;
    --select count(*) from w_variables;
    CREATE INDEX idx_w_variables ON w_variables USING BTREE (target_variable, sub_population, sub_population_category, area_domain_category);

    --   explain analyze
    with w_data as (
        select
            t_panel__id as panel,
            t_reference_year_set__id as reference_year_set,
            t_variable__id as variable,
            w_sdesign.f_p_plot__gid,
            w_data_csv.value
        from w_sdesign
        join w_data_csv on (
            w_sdesign.country               = w_data_csv.country and
            w_sdesign.strata_set            = w_data_csv.strata_set and
            w_sdesign.stratum               = w_data_csv.stratum and
            w_sdesign.panel                 = w_data_csv.panel and
            w_sdesign.cluster               = w_data_csv.cluster and
            w_sdesign.plot                  = w_data_csv.plot and
            w_sdesign.inventory_campaign    = w_data_csv.inventory_campaign and
            w_sdesign.reference_year_set    = w_data_csv.reference_year_set)
        join w_variables on (
            w_variables.target_variable = w_data_csv.target_variable
            and (w_variables.sub_population = w_data_csv.sub_population)
            and (w_variables.sub_population_category = w_data_csv.sub_population_category)
            and (w_variables.area_domain = w_data_csv.area_domain)
            and (w_variables.area_domain_category = w_data_csv.area_domain_category))
    )
    insert into nfiesta.t_target_data(plot, available_datasets, value)
    select
        w_data.f_p_plot__gid,
        t_available_datasets.id,
        w_data.value
    from w_data
    join nfiesta.t_available_datasets               on (
        t_available_datasets.variable = w_data.variable and
        t_available_datasets.panel = w_data.panel and
        t_available_datasets.reference_year_set = w_data.reference_year_set)
    returning *
    ;
    ''', (country_r, ))
    #import json
    #with open('explain.json', 'w') as f:
    #    json.dump(res, indent=4, sort_keys=True, fp=f)
    pf_print('%s records inserted, %s records in CSV (%s non-zero)' % (len(res), counts['count'], counts['nonzero']), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_target_data;', (None, ))

    pf_print('%s------------------------------commit changes' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    conn.commit()

    pf_print('%s------------------------------enable sum of sampling weights check' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    alter table sdesign.t_panel enable trigger trg__panel__check_threshold__ins;
    ''', (country_r, ))

    pf_print('%s------------------------------enable plot-level additivity checks evaluation' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    alter table nfiesta.t_additivity_set_plot enable trigger trg__additivity_set_plot__ins;
    ''', (country_r, ))

    pf_print('%s------------------------------perform change ldsity <-> t_reference_year_set.status_variables accordance check' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    with w_data as (
        select
                c_country.label as country,
                t_stratum.stratum,
                c_target_variable.metadata->'en'->>'pf_label' as target_variable,
                c_sub_population_category.label as sub_population_category,
                t_available_datasets.id as available_dataset__id,
                t_available_datasets.panel as available_dataset__panel,
                t_reference_year_set.*,
                t_inventory_campaign.*,
                metadata->'en' as metadata_state_info,
                (metadata->'en'->'state or change'->>'label' = 'change variable' and t_reference_year_set.status_variables = false and t_inventory_campaign.status_variables = false) or
                (metadata->'en'->'state or change'->>'label' = 'state variable' and t_reference_year_set.status_variables = true and t_inventory_campaign.status_variables = true) or
                (metadata->'en'->'state or change'->>'label' = 'dynamic variable' and t_reference_year_set.status_variables = false and t_inventory_campaign.status_variables = false) as is_ok
        from nfiesta.c_target_variable
        join nfiesta.t_variable on c_target_variable.id = t_variable.target_variable
        join nfiesta.t_available_datasets on t_available_datasets.variable = t_variable.id
        join sdesign.t_reference_year_set on t_reference_year_set.id = t_available_datasets.reference_year_set
        join sdesign.t_inventory_campaign on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
        join sdesign.t_panel ON t_panel.id = t_available_datasets.panel
        join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
        join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
        join sdesign.c_country ON c_country.id = t_strata_set.country
        left join nfiesta.c_sub_population_category on (c_sub_population_category.id = t_variable.sub_population_category)
        order by c_country.label, target_variable, sub_population_category, t_available_datasets.id
    )
    --select * from w_data where is_ok is null
    select
        country, count(*) as checked_datasets, sum(is_ok::integer) as ok
    from w_data
    where country = %s
    group by country
    ;
    ''', (country_r, ))
    if len(res) == 0:
        pf_print('country not providing change variables', log_f)
    else:
        pf_print('checked_datasets %s, is_ok %s' % (res[0]['checked_datasets'], res[0]['ok']), log_f)
