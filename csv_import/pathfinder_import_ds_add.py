#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime
import psycopg2
import psycopg2.extras
from multiprocessing import Pool
import json

def single_query(cs, log_name, c, m, _sql, verbose=True):
    from pathfinder_import_ds import pf_print
    try:
        #connstr = 'host=localhost port=5433 dbname=nfi_esta user=vagrant' # for network authentication (password can be stored in ~/.pgpass)
        connstr = cs #'dbname=nfi_esta_dev' # for local linux based authentication (without password)
        conn = psycopg2.connect(connstr)
    except psycopg2.DatabaseError as e:
        print("nfiesta_parallel.single_query(), not possible to connect DB")
        print(e)
        return 1

    conn.set_session(isolation_level=None, readonly=None, deferrable=None, autocommit=True) # to not execute in transaction
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cmd = '%s' % (_sql)
    
    try:
        cur.execute(cmd)
    except psycopg2.Error as e:
        print("nfiesta_parallel.single_query(), SQL error:")
        print(e)
        return 2
    log_f = open(log_name, "a")
    if cur.description != None:
        path_row_list = cur.fetchall()
        if verbose:
            pf_print('%s\t%s/%s\t%s\t%s' % (datetime.now(), c, m, cmd, path_row_list[0][0]['additivity_status']), log_f)
        else:
            pf_print('%s\tnfiesta_parallel.py result not None' % (datetime.now()), log_f)
    else:
        path_row_list = None
        if verbose:
            pf_print('%s\t%s/%s\tnfiesta_parallel.py result None: %s' % (datetime.now(), c, m, path_row_list), log_f)
        else:
            pf_print('%s\tnfiesta_parallel.py result None' % (datetime.now()), log_f)
    log_f.close()
    cur.close()
    conn.close()
    return path_row_list[0][0]

import unicodedata
import re

def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')

def check_add_plot(cs, conn, country_r, log_name, processes=1):
    import os
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    log_f = open(log_name, "a")
    pf_print('%s------------------------------perform plot-level additivity checks' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
with w_add_set as (
        select
                c_country.label as country,
                t_additivity_set_plot.id,
                t_additivity_set_plot.available_datasets
        from nfiesta.t_additivity_set_plot
        join nfiesta.t_available_datasets on t_available_datasets.id = t_additivity_set_plot.available_datasets
        join sdesign.t_panel on t_panel.id = t_available_datasets.panel
        join sdesign.t_stratum on t_stratum.id = t_panel.stratum
        join sdesign.t_strata_set on t_strata_set.id = t_stratum.strata_set
        join sdesign.c_country on c_country.id = t_strata_set.country
        where dbg_data->>'fn_launch_additivity_check' is null
    and c_country.label = %s
        order by available_datasets, edges, valid_from
)
select country, (array_agg(id))[1] as id, available_datasets from w_add_set group by country, available_datasets;
    ''', (country_r, ))
    pf_print('%s------------------------------parallel processing using %s processes' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S"), processes), log_f)
    log_f.close()
    params = [[cs, log_name, c+1, len(res), 'select * from nfiesta.fn_launch_add_manual(%(id)s);' % {'id': r['id']}] for c, r in enumerate(res)]
    with Pool(processes) as p:
        res_multi = p.starmap(single_query, params, chunksize=1)
    log_f = open(log_name, "a")
    for i, r in enumerate(res_multi):
        if r['additivity_status'] == 'failed':
            add_err_fname = os.path.join(os.path.dirname(log_name), slugify('add_err_%s_%s_%s_%s' % (i+1, r['panel'], r['refyearset'], r['aggregated variable']['target_variable'])) + '.json')
            pf_print('panel: %s, reference_year_set: %s, variable: %s, number of problematic plots: %s, full log in file: %s' % (r['panel'], r['refyearset'], r['aggregated variable']['target_variable'], len(r['err']), add_err_fname), log_f)
            with open(add_err_fname, 'w', encoding='utf-8') as f:
                json.dump(r, f, ensure_ascii=False, indent=4)
    log_f.close()

def check_add_plot_aggregates(conn, country_r, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    pf_print('%s------------------------------adding aggregated local densities' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    run_sql(conn, 'alter table nfiesta.t_additivity_set_plot disable trigger trg__additivity_set_plot__ins;', (None, ))
    cmd = '''
-- add aggregates
insert into nfiesta.t_target_data(plot, available_datasets, value)
select
    t_target_data.plot,
    t_available_datasets_sup.id as available_datasets,
    sum(t_target_data.value) as value
from nfiesta.t_target_data
join nfiesta.t_available_datasets on (t_target_data.available_datasets = t_available_datasets.id)
join nfiesta.t_variable on (t_available_datasets.variable = t_variable.id)
join nfiesta.t_variable_hierarchy on (t_variable_hierarchy.variable = t_variable.id)
join nfiesta.t_available_datasets as t_available_datasets_sup on (
    t_variable_hierarchy.variable_superior = t_available_datasets_sup.variable and
    t_available_datasets.panel = t_available_datasets_sup.panel and
    t_available_datasets.reference_year_set = t_available_datasets_sup.reference_year_set)
join sdesign.t_panel on t_available_datasets.panel = t_panel.id
join sdesign.t_stratum on t_panel.stratum = t_stratum.id
join sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
join sdesign.c_country on t_strata_set.country = c_country.id
where t_target_data.is_latest and c_country.label = %s
group by  t_target_data.plot, t_available_datasets_sup.id
;
'''
    res = run_sql(conn, cmd, (country_r, ))
    pf_print('%s------------------------------commit changes' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    conn.commit()
    run_sql(conn, 'alter table nfiesta.t_additivity_set_plot enable trigger trg__additivity_set_plot__ins;', (None, ))

def check_add_plot_threshold(conn, country_r, log_f, threshold='1e-08'):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    pf_print('%s------------------------------setting additivity threshold: %s' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S"), threshold), log_f)
    cmd = '''
--update nfiesta.t_additivity_set_plot set add_check_threshold = 1e-06 where dbg_data->>'fn_launch_additivity_check' is null;
with w_add_set as (
    select
        c_country.label,
        t_additivity_set_plot.*,
        row_number() over() as i
    from nfiesta.t_additivity_set_plot
    join nfiesta.t_available_datasets on t_available_datasets.id = t_additivity_set_plot.available_datasets
    join sdesign.t_panel on t_panel.id = t_available_datasets.panel
    join sdesign.t_stratum on t_stratum.id = t_panel.stratum
    join sdesign.t_strata_set on t_strata_set.id = t_stratum.strata_set
    join sdesign.c_country on c_country.id = t_strata_set.country
    where dbg_data->>'fn_launch_additivity_check' is null
    and c_country.label = %s
    order by available_datasets, edges, valid_from
)
update nfiesta.t_additivity_set_plot
set add_check_threshold = %s
from w_add_set
where t_additivity_set_plot.id = w_add_set.id
;
'''
    res = run_sql(conn, cmd, (country_r, threshold))
