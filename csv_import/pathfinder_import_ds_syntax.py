#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
from datetime import datetime

def diff_header(csvfilename, country, dir_version, err_file):
    f1n = os.path.join('countries', country, csvfilename)
    f2n = os.path.join('countries', 'example_data_%s' % (dir_version), csvfilename)
    if (os.path.isfile(f1n)):
        with open(f1n, "r") as f1, open(f2n, "r") as f2:
            line1 = f1.readline()
            line2 = f2.readline()
        if line1 != line2:
            print('CSV header differs: %s' % csvfilename, file=err_file)
            print('provided: %s' % line1, file=err_file)
            print('expected: %s' % line2, file=err_file)
            return True
        else:
            return False

def etl_check_country(conn, country_r, dir_version, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    import psycopg2.extras
    import psycopg2.errorcodes
    from psycopg2 import sql
    pf_print('==============================country %s syntax check==============================' % country_r, log_f)
    number_of_exceptions = 0
    number_of_header_diffs = 0
    number_missing_files = 0
    err_log_name = os.path.join('countries', country_r, 'log', 'syntax_errors.txt')
    err_f = open(err_log_name, "a")
    err_f.seek(0, 0)
    err_f.truncate()
    print("log timestamp: %s" % datetime.now().strftime("%Y-%b-%d %H:%M:%S"), file=err_f)
    print('PLEASE DOUBLE CHECK ALL CSV FILENAMES\n  * file name have to be exactly as is given on wiki https://gitlab.com/nfiesta/pathfinder_demo_study/-/wikis/Description%20of%20CSV%20files#check-list-of-csv-files-to-be-provided\n\n', file=err_f)
    sql_cmd = '''
    select 
        relnamespace::regnamespace::text, relname, ftoptions 
    from pg_foreign_table 
    join pg_class on (ftrelid = oid) 
    where relnamespace::regnamespace::text in ('csv_country, csv_fk_check_codelist, csv_fk_check_country, csv_server, nfiesta');
    '''
    data = (None, )
    res = run_sql(conn, sql_cmd, data)
    for r in res:
        print('-----file %s-----' % r['ftoptions'][3].rsplit("/", 1)[1], file=err_f)
        if diff_header(r['ftoptions'][3].rsplit("/", 1)[1], country_r, dir_version, err_f):
            number_of_header_diffs += 1
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql_cmd = sql.SQL('''select count(*) as c from {}.{};''').format(sql.Identifier(r['relnamespace']), sql.Identifier(r['relname']))
        data = (None, )
        try:
            cur.execute(sql_cmd, data)
            if cur.description != None:
                path_row_list = cur.fetchall()
            else:
                path_row_list = None
            if path_row_list[0]['c'] > 0:
                print('data format compliant with CSV specification', file=err_f)
            else:
                print('table has 0 rows', file=err_f)
        except psycopg2.Error as e:
            if e.pgcode == psycopg2.errorcodes.UNDEFINED_FILE:
                number_missing_files += 1
                print('missing file', file=err_f)
                print(e, file=err_f)
            else:
                number_of_exceptions += 1
                print(e, file=err_f)
        cur.close()
        conn.commit()
    err_f.close()
    pf_print('number of missing files: %s' % number_missing_files, log_f)
    pf_print('number of exceptions: %s' % number_of_exceptions, log_f)
    pf_print('number of diffs in CV headers: %s' % number_of_header_diffs, log_f)
    if number_missing_files == 0 and number_of_exceptions == 0 and number_of_header_diffs == 0:
        os.remove(err_log_name)
    else:
        pf_print('For more info on syntax errors see %s' % err_log_name, log_f)
        sys.exit(1)
