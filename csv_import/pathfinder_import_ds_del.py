#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from datetime import datetime

def del_country(conn, country_r, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    pf_print('==============================country %s delete==============================' % country_r, log_f)
    #pf_print('dropping analytical indices', log_f)
    #res = run_sql(conn, '''
    #DROP INDEX sdesign.idx__cm_cluster2panel_mapping__panel;
    #DROP INDEX sdesign.idx__f_p_plot__cluster;
    #''', (None, ))

    #################################################DATA INSERTED IN GUI APP
    ##### estimation
    pf_print('%s------------------------------t_result' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.t_result where id in (
        select 
            distinct t_result.id
        from
        sdesign.c_country
        join sdesign.t_strata_set ON t_strata_set.country = c_country.id
        join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel ON t_panel.stratum = t_stratum.id
        join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.panel = t_panel.id
        join sdesign.t_reference_year_set ON t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
        join nfiesta.t_panel_refyearset_group on (
                    t_panel_refyearset_group.panel = t_panel.id and 
                    t_panel_refyearset_group.reference_year_set = t_reference_year_set.id)
        join nfiesta.c_panel_refyearset_group on (c_panel_refyearset_group.id = t_panel_refyearset_group.panel_refyearset_group)
        join nfiesta.t_total_estimate_conf on (t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id)
        join nfiesta.t_estimate_conf on (t_estimate_conf.total_estimate_conf = t_total_estimate_conf.id)
        join nfiesta.t_result on (t_result.estimate_conf = t_estimate_conf.id)
        where c_country.label = %s
    )
    returning id;
    ''', (country_r, ))
    pf_print('t_result: %s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_result;', (None, ))
    ##### configuration
    pf_print('%s------------------------------t_estimate_conf' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.t_estimate_conf where id in (
        select 
            distinct t_estimate_conf.id
        from
        sdesign.c_country
        join sdesign.t_strata_set ON t_strata_set.country = c_country.id
        join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel ON t_panel.stratum = t_stratum.id
        join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.panel = t_panel.id
        join sdesign.t_reference_year_set ON t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
        join nfiesta.t_panel_refyearset_group on (
                    t_panel_refyearset_group.panel = t_panel.id and 
                    t_panel_refyearset_group.reference_year_set = t_reference_year_set.id)
        join nfiesta.c_panel_refyearset_group on (c_panel_refyearset_group.id = t_panel_refyearset_group.panel_refyearset_group)
        join nfiesta.t_total_estimate_conf on (t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id)
        join nfiesta.t_estimate_conf on (t_estimate_conf.total_estimate_conf = t_total_estimate_conf.id)
        where c_country.label = %s
    )
    returning id;
    ''', (country_r, ))
    pf_print('t_estimate_conf: %s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_estimate_conf;', (None, ))

    pf_print('%s------------------------------t_total_estimate_conf' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.t_total_estimate_conf where id in (
        select 
            distinct t_total_estimate_conf.id
        from
        sdesign.c_country
        join sdesign.t_strata_set ON t_strata_set.country = c_country.id
        join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel ON t_panel.stratum = t_stratum.id
        join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.panel = t_panel.id
        join sdesign.t_reference_year_set ON t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
        join nfiesta.t_panel_refyearset_group on (
                    t_panel_refyearset_group.panel = t_panel.id and 
                    t_panel_refyearset_group.reference_year_set = t_reference_year_set.id)
        join nfiesta.c_panel_refyearset_group on (c_panel_refyearset_group.id = t_panel_refyearset_group.panel_refyearset_group)
        join nfiesta.t_total_estimate_conf on (t_total_estimate_conf.panel_refyearset_group = c_panel_refyearset_group.id)
        where c_country.label = %s
    )
    returning id;
    ''', (country_r, ))
    pf_print('t_total_estimate_conf: %s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_total_estimate_conf;', (None, ))

    pf_print('%s------------------------------c_estimation_period' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.c_estimation_period where id in (
        select c_estimation_period.id
        from nfiesta.c_estimation_period 
        left join nfiesta.t_total_estimate_conf on (c_estimation_period.id = t_total_estimate_conf.estimation_period) 
        where t_total_estimate_conf.id is null
    )
    returning id;
    ''', (country_r, ))
    pf_print('c_estimation_period: %s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.c_estimation_period;', (None, ))

    pf_print('%s------------------------------t_panel_refyearset_group' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.t_panel_refyearset_group where id in (
        select 
            distinct t_panel_refyearset_group.id
        from
        sdesign.c_country
        join sdesign.t_strata_set ON t_strata_set.country = c_country.id
        join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
        join sdesign.t_panel ON t_panel.stratum = t_stratum.id
        join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.panel = t_panel.id
        join sdesign.t_reference_year_set ON t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
        join nfiesta.t_panel_refyearset_group on (
                    t_panel_refyearset_group.panel = t_panel.id and 
                    t_panel_refyearset_group.reference_year_set = t_reference_year_set.id)
        where c_country.label = %s
    )
    returning id;
    ''', (country_r, ))
    pf_print('t_panel_refyearset_group: %s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_panel_refyearset_group;', (None, ))

    pf_print('%s------------------------------c_panel_refyearset_group' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)

    res = run_sql(conn, '''
    delete from nfiesta.c_panel_refyearset_group where id in (
        select c_panel_refyearset_group.id
        from nfiesta.c_panel_refyearset_group 
        left join nfiesta.t_panel_refyearset_group on (c_panel_refyearset_group.id = t_panel_refyearset_group.panel_refyearset_group) 
        where t_panel_refyearset_group.id is null
    )
    returning id;
    ''', (country_r, ))
    pf_print('c_panel_refyearset_group: %s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.c_panel_refyearset_group;', (None, ))

    #################################################DATA INSERTED IN provisioning/provision_cells_use4estimates.sql 
    pf_print('%s------------------------------cm_plot2cell_mapping' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.cm_plot2cell_mapping where plot in (
    select f_p_plot.gid
    from
    sdesign.c_country
    join sdesign.t_strata_set ON t_strata_set.country = c_country.id
    join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
    join sdesign.t_panel ON t_panel.stratum = t_stratum.id
    join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
    join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping.cluster
    join sdesign.f_p_plot ON f_p_plot.cluster = t_cluster.id
    where c_country.label = %s
    )
    ;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze nfiesta.cm_plot2cell_mapping;', (None, ))

    pf_print('%s------------------------------t_stratum_in_estimation_cell' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.t_stratum_in_estimation_cell where stratum in (
    select t_stratum.id
    from
    sdesign.c_country
    join sdesign.t_strata_set ON t_strata_set.country = c_country.id
    join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
    where c_country.label = %s
    )
    ;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze nfiesta.t_stratum_in_estimation_cell;', (None, ))

    pf_print('%s------------------------------f_a_cell' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.f_a_cell where estimation_cell in (
        select id from nfiesta.c_estimation_cell
        where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0') 
        and label = %s
    )
    returning gid;
    ''', (country_r, ))
    pf_print('%s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.f_a_cell;', (None, ))

    pf_print('%s------------------------------t_estimation_cell_hierarchy' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    with w_data as(
        select id from nfiesta.c_estimation_cell
        where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0') 
        and label = %s
    )
    delete from nfiesta.t_estimation_cell_hierarchy 
    where cell in (select id from w_data) or cell_superior in (select id from w_data)
    returning id;
    ''', (country_r, ))
    pf_print('%s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.t_estimation_cell_hierarchy;', (None, ))

    pf_print('%s------------------------------c_estimation_cell' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
    delete from nfiesta.c_estimation_cell 
    where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0') 
    and label = %s
    returning id;
    ''', (country_r, ))
    pf_print('%s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze nfiesta.c_estimation_cell;', (None, ))

    res = run_sql(conn, '''
        select
            c_estimation_cell_collection.label, count(*) as remaining_nuts0
        from nfiesta.c_estimation_cell
        join nfiesta.c_estimation_cell_collection on (c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id)
        group by c_estimation_cell_collection.label
        having c_estimation_cell_collection.label='NUTS0';
    ''', (None, ))
    if len(res) == 0:
        pf_print('%s NUTS0 remaining in DB, deleting union' % (len(res)), log_f)
        res = run_sql(conn, '''
        delete from nfiesta.f_a_cell where estimation_cell in (
            select id from nfiesta.c_estimation_cell
            where label = 'NUTS'
        );
        with w_data as(
            select id from nfiesta.c_estimation_cell
            where label = 'NUTS'
        )
        delete from nfiesta.t_estimation_cell_hierarchy 
        where cell in (select id from w_data) or cell_superior in (select id from w_data);
        delete from nfiesta.c_estimation_cell where label = 'NUTS';
        ''', (None, ))


    #################################################DATA IMPORTED FROM CSVs

    pf_print('%s------------------------------t_plot_measurement_dates' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)

    res = run_sql(conn, '''
delete from sdesign.t_plot_measurement_dates where reference_year_set in (
select t_reference_year_set.id
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.panel = t_panel.id
join sdesign.t_reference_year_set ON t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
where c_country.label = %s
)
returning reference_year_set
;
    ''', (country_r, ))
    pf_print('t_plot_measurement_dates: %s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze sdesign.t_plot_measurement_dates;', (None, ))

    pf_print('%s------------------------------t_auxiliary_data' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
alter table nfiesta.t_auxiliary_data disable trigger all;
delete from nfiesta.t_auxiliary_data where plot in (
select f_p_plot.gid
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping.cluster
join sdesign.f_p_plot ON f_p_plot.cluster = t_cluster.id
where c_country.label = %s
)
;
alter table nfiesta.t_auxiliary_data enable trigger all;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze nfiesta.t_auxiliary_data;', (None, ))

    pf_print('%s------------------------------t_target_data' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
alter table nfiesta.t_target_data disable trigger all;
delete from nfiesta.t_target_data where plot in (
select f_p_plot.gid
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping.cluster
join sdesign.f_p_plot ON f_p_plot.cluster = t_cluster.id
where c_country.label = %s
)
;
alter table nfiesta.t_target_data enable trigger all;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze nfiesta.t_target_data;', (None, ))

    res = run_sql(conn, '''
alter table nfiesta.t_additivity_set_plot disable trigger all;
delete from nfiesta.t_additivity_set_plot where available_datasets in (
select t_available_datasets.id
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
join nfiesta.t_available_datasets on t_available_datasets.panel = t_panel.id
where c_country.label = %s
);
alter table nfiesta.t_additivity_set_plot enable trigger all;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze nfiesta.t_additivity_set_plot;', (None, ))

    pf_print('%s------------------------------t_available_datasets' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
alter table nfiesta.t_available_datasets disable trigger all;
delete from nfiesta.t_available_datasets where panel in (
select t_panel.id
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
where c_country.label = %s
);
alter table nfiesta.t_available_datasets enable trigger all;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze nfiesta.t_available_datasets;', (None, ))

    res = run_sql(conn, '''
delete from sdesign.cm_refyearset2panel_mapping where panel in (
select t_panel.id
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
where c_country.label = %s
)
returning panel;
    ''', (country_r, ))
    pf_print('%s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze sdesign.cm_refyearset2panel_mapping;', (None, ))

    res = run_sql(conn, '''
delete from sdesign.t_reference_year_set where id in (
select t_reference_year_set.id
from sdesign.t_reference_year_set
left join sdesign.cm_refyearset2panel_mapping on t_reference_year_set.id = cm_refyearset2panel_mapping.reference_year_set
where cm_refyearset2panel_mapping.reference_year_set is null
);
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_reference_year_set;', (None, ))

    res = run_sql(conn, '''
delete from sdesign.t_inventory_campaign where id in (
select t_inventory_campaign.id 
from sdesign.t_inventory_campaign
left join sdesign.t_reference_year_set on t_inventory_campaign.id = t_reference_year_set.inventory_campaign
where t_reference_year_set.inventory_campaign is null
);
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_inventory_campaign;', (None, ))

    res = run_sql(conn, '''
delete from sdesign.cm_plot2cluster_config_mapping where plot in (
select f_p_plot.gid
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping.cluster
join sdesign.f_p_plot ON f_p_plot.cluster = t_cluster.id
where c_country.label = %s
)
;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze sdesign.cm_plot2cluster_config_mapping;', (None, ))

    res = run_sql(conn, '''
delete from sdesign.t_plot_measurement_dates where plot in (
select f_p_plot.gid
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping.cluster
join sdesign.f_p_plot ON f_p_plot.cluster = t_cluster.id
where c_country.label = %s
)
;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze sdesign.t_plot_measurement_dates;', (None, ))

    pf_print('%s------------------------------create temporary table clusters2delete' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
create temporary table clusters2delete as 
select cm_cluster2panel_mapping.cluster
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
join sdesign.t_panel ON t_panel.stratum = t_stratum.id
join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
where c_country.label = %s
;
create index on clusters2delete(cluster);
    ''', (country_r, ))
    res = run_sql(conn, 'analyze clusters2delete;', (None, ))

    pf_print('%s------------------------------f_p_plot' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
--explain (analyze, buffers)
delete from sdesign.f_p_plot where cluster in (
select cluster from clusters2delete)
;
    ''', (country_r, ))
    #[print(line) for line in res]
    res = run_sql(conn, 'analyze sdesign.f_p_plot;', (None, ))

    pf_print('%s------------------------------cm_cluster2panel_mapping' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
delete from sdesign.cm_cluster2panel_mapping where cluster in 
(select cluster from clusters2delete);
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.cm_cluster2panel_mapping;', (None, ))

    pf_print('%s------------------------------t_cluster' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    # trigger fkey__f_p_plot__t_cluster slows down delete. 
    # Few lines above are deleted all plots belonging to clusters to be deleted.
    # Few lines lower is foreign key re-created.
    res = run_sql(conn, 'alter table sdesign.f_p_plot drop constraint fkey__f_p_plot__t_cluster;', (None, ))
    res = run_sql(conn, 'alter table sdesign.cm_cluster2panel_mapping drop constraint fkey__cm_cluster2panel_mapping__t_cluster;', (None, ))
    res = run_sql(conn, '''
--explain analyze
delete from sdesign.t_cluster where id in
(select cluster from clusters2delete);
    ''', (None, ))
    #[print(line) for line in res]
    #import sys
    #sys.exit(1)
    res = run_sql(conn, '''
ALTER TABLE ONLY sdesign.f_p_plot ADD CONSTRAINT fkey__f_p_plot__t_cluster FOREIGN KEY (cluster) REFERENCES sdesign.t_cluster(id);
COMMENT ON CONSTRAINT fkey__f_p_plot__t_cluster ON sdesign.f_p_plot IS 'Foreign key to table sdesign.t_cluster.';
    ''', (None, ))

    res = run_sql(conn, '''
ALTER TABLE ONLY sdesign.cm_cluster2panel_mapping ADD CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster FOREIGN KEY (cluster) REFERENCES sdesign.t_cluster(id);
COMMENT ON CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster ON sdesign.cm_cluster2panel_mapping IS 'Foreign key to table sdesign.t_cluster.'
    ''', (None, ))

    res = run_sql(conn, 'analyze sdesign.t_cluster;', (None, ))

    pf_print('%s------------------------------drop table clusters2delete' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
drop table clusters2delete;
    ''', (None, ))

    pf_print('%s------------------------------t_panel' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
delete from sdesign.t_panel where stratum in (
select t_stratum.id
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
join sdesign.t_stratum ON t_stratum.strata_set = t_strata_set.id
where c_country.label = %s
)
returning stratum
;
    ''', (country_r, ))
    pf_print('%s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze sdesign.t_panel;', (None, ))

    res = run_sql(conn, '''
delete from sdesign.t_cluster_configuration where id in (
select t_cluster_configuration.id
from sdesign.t_cluster_configuration
left join sdesign.t_panel on t_cluster_configuration.id = t_panel.cluster_configuration
where t_panel.cluster_configuration is null
);
    ''', (None, ))
    res = run_sql(conn, 'analyze sdesign.t_cluster_configuration;', (None, ))

    pf_print('%s------------------------------t_stratum' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
delete from sdesign.t_stratum where strata_set in (
select t_strata_set.id
from
sdesign.c_country
join sdesign.t_strata_set ON t_strata_set.country = c_country.id
where c_country.label = %s
)
;
    ''', (country_r, ))
    res = run_sql(conn, 'analyze sdesign.t_stratum;', (None, ))

    pf_print('%s------------------------------t_strata_set' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")), log_f)
    res = run_sql(conn, '''
delete from sdesign.t_strata_set where country in (
select c_country.id
from
sdesign.c_country
where c_country.label = %s
)
returning country
;
    ''', (country_r, ))
    pf_print('%s records deleted' % (len(res)), log_f)
    res = run_sql(conn, 'analyze sdesign.t_strata_set;', (None, ))
    
    #pf_print('adding analytical indices', log_f)
    #res = run_sql(conn, '''
    #CREATE INDEX idx__cm_cluster2panel_mapping__panel ON sdesign.cm_cluster2panel_mapping USING btree (panel) INCLUDE (cluster, sampling_weight_ha);
    #CREATE INDEX idx__f_p_plot__cluster ON sdesign.f_p_plot USING btree (cluster) INCLUDE (gid);
    #''', (None, ))

    conn.commit()
