#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import sys
import os
import hashlib
import psycopg2
import psycopg2.extras
import csv
from datetime import datetime

def pf_print(message, log_f):
    print(message, file=log_f, flush=True)
    print(message)

def db_conn(connstr):
    #print(psycopg2.extensions.parse_dsn(connstr))
    try:
        conn = psycopg2.connect(connstr)
    except psycopg2.DatabaseError as e:
        print("not possible to connect DB")
        print(e)
        sys.exit(1)
    return conn

def run_sql(_conn, _sql_cmd, _data):
    cur = _conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    try:
        cur.execute(_sql_cmd, _data)
    except psycopg2.Error as e:
        print("pathfinder_import_ds.py, SQL error:")
        print(e)
        sys.exit(1)

    if cur.description != None:
        path_row_list = cur.fetchall()
    else:
        path_row_list = None

    return path_row_list
    cur.close()

def etl_set_fdw(conn, lpqcs, country_r, dir_version, log_f):
    import subprocess
    pf_print("%s reseting country FDW" % datetime.now().strftime("%Y-%b-%d %H:%M:%S"), log_f)
    try:
        subprocess.run(["psql", lpqcs, "-f", 'countries/example_data_%s/csv_country_fdw.sql' % (dir_version), "-v", "ON_ERROR_STOP=1", "-v", "client_min_messages=WARNING", "--quiet"])
    except psycopg2.Error as e:
        pf_print("pathfinder_import_ds.py, SQL error:", log_f)
        pf_print(e, log_f)
        sys.exit(1)

    sql_cmd = '''with w_data as (select ftrelid::regclass as ftname, ftrelid, ftoptions from pg_foreign_table where ftrelid::regclass::text != 'csv.countries') update pg_foreign_table set ftoptions[4] = replace(w_data.ftoptions[4], %s, %s) from w_data where pg_foreign_table.ftrelid = w_data.ftrelid;'''
    data = ('example_data_%s' % (dir_version), country_r, )
    res = run_sql(conn, sql_cmd, data)
    conn.commit()
    for r in run_sql(conn, '''select relnamespace::regnamespace::text, relname, ftoptions from pg_foreign_table join pg_class on (ftrelid = oid) order by 1, 2;''', (None, )):
        csvfilename = r['ftoptions'][3].rsplit("=", 1)[1]
        try:
            pf_print('-----file %s, md5sum: %s-----' % (csvfilename.rsplit("/", 1)[1], hashlib.md5(open(csvfilename,'rb').read()).hexdigest()), log_f)
        except:
            pf_print('-----file %s, md5sum: not possible to evaluate-----' % (csvfilename.rsplit("/", 1)[1]), log_f)

def check_ext_compatibility(conn, log_f, csvversion, args):
    import subprocess
    pf_print('git rev-parse HEAD: %s' % (subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode('ascii').strip()), log_f)
    pf_print('git diff *.py: %s' % (subprocess.check_output(['git', 'diff', '*.py']).decode('ascii').strip()), log_f)
    pf_print('application launched with arguments: %s' % (args), log_f)

    extversion_sdesign = run_sql(conn, '''select extname, extversion from pg_extension where extname in ('nfiesta_sdesign');''', (None, ))[0]['extversion']
    extversion_nfiesta = run_sql(conn, '''select extname, extversion from pg_extension where extname in ('nfiesta');''', (None, ))[0]['extversion']
    if csvversion == '2.0':
        extversion_sdesign = run_sql(conn, '''select extname, extversion from pg_extension where extname in ('nfiesta_sdesign');''', (None, ))[0]['extversion']
        extversion_nfiesta = run_sql(conn, '''select extname, extversion from pg_extension where extname in ('nfiesta');''', (None, ))[0]['extversion']
        extversion_sdesign_compatible = '1.1.15'
        extversion_nfiesta_compatible = '3.18.0'
        if extversion_sdesign == extversion_sdesign_compatible and extversion_nfiesta == extversion_nfiesta_compatible:
            pf_print('checking CSVs version %s, nfiesta_sdesign version %s, nfiesta version %s' % (csvversion, extversion_sdesign, extversion_nfiesta), log_f)
            return '2_0'
        else:
            pf_print('incompatible application with extension versions', log_f)
            pf_print('compatible  nfiesta_sdesign version %s, compatible  nfiesta version %s' % (extversion_sdesign_compatible, extversion_nfiesta_compatible), log_f)
            pf_print('actual      nfiesta_sdesign version %s, actual      nfiesta version %s' % (extversion_sdesign, extversion_nfiesta), log_f)
            sys.exit(1)
    else:
        pf_print('CSV version not implemented', log_f)
        pf_print('older CSV versions (1.0, 1.5) can be upgraded to 2.0', log_f)
        pf_print('or imported using older version of the app: https://gitlab.com/nfiesta/pathfinder_demo_study/-/tags/import_CSV_v1_0_and_v1_5', log_f)
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(description="Utility for import PathFinder data ET*L* -> nfiesta_pg.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='''\
Example:
    Keyword/Value Connection Strings
        python3 pathfinder_import_ds.py -cs='host=localhost dbname=postgres user=vagrant'
        python3 pathfinder_import_ds.py --connectionstring 'host=localhost dbname=postgres user=vagrant'
    Connection URIs
        python3 pathfinder_import_ds.py -cs='postgresql://vagrant@localhost/postgres'
            ''')
    parser.add_argument('-cs', '--connectionstring', required=True, help='libpq connection string [SOURCE DB]: https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING . For password use ~/.pgpass file.')
    parser.add_argument('-c', '--country', required=True, help='country code to process. lowercase')
    parser.add_argument('-v', '--csvversion', required=True, type=str, choices=['1.0', '1.5', '2.0'], help='version of CSV files: 1.0 -- initial demo study, 1.5 -- initial demo study, sampling frame area in cluster_configurations.csv, 2.0 -- extended data collection')
    parser.add_argument('-chs', '--syntax', action='store_true', help='check country data syntax')
    parser.add_argument('-p', '--provision', action='store_true', help='create tables needed for FKEYs checks -- run by default')
    parser.add_argument('-chf', '--fkeys', action='store_true', help='check country data FKEYs')
    parser.add_argument('-d', '--delete', action='store_true', help='delete country data')
    parser.add_argument('-i', '--insert', action='store_true', help='insert country data')
    parser.add_argument('-chc', '--cells', action='store_true', help='check if estimation cells intersects strata')
    parser.add_argument('-cha', '--additivity', action='store_true', help='check plot level additivity')
    parser.add_argument('-chap', '--additivityprocesses', type=int, help='number of processes (CPUs) to use for additivity check')
    parser.add_argument('-chaa', '--additivityagg', action='store_true', help='add aggregated local densities before additivity check')
    parser.add_argument('-chat', '--additivitythreshold', help='set additivity threshold')
    parser.add_argument('-z', '--zip', action='store_true', help='create zip archive')
    parser.add_argument('-e', '--use4est', action='store_true', help='provision cells for estimates')
    parser.set_defaults(delete=False)
    parser.set_defaults(checkfkeys=False)
    parser.set_defaults(insert=False)
    parser.set_defaults(provision=True)
    args = parser.parse_args()

    args.country=args.country.upper() #country code to upper-case
    os.environ["NFIESTA_COUNTRY"]=args.country #re-export env variable
    
    if args.provision:
        import time
        while run_sql(db_conn(args.connectionstring), '''select count(*) from csv_fk_check_codelist.variable_target;''', (None, ))[0][0] < 1:
            print('DB is not provisioned')
            time.sleep(2)
    
    conn = db_conn(args.connectionstring)

    countries = run_sql(conn, 'select * from sdesign.c_country where label in (%s) order by description;', (args.country, ))
    if len(countries) == 0:
        print('country %s not found' % (args.country))
        sys.exit(1)
    for country in countries:
        out_dir = os.path.join('countries', country['description'], 'log')
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        log_name = os.path.join(out_dir, 'import_log.txt')
        log_f = open(log_name, "a")
        log_f.seek(0, 0)
        log_f.truncate()
        dir_version = check_ext_compatibility(conn, log_f, args.csvversion, args)
        if os.path.exists(os.path.join('countries', country['description'])):
            if len(os.listdir(os.path.join('countries', country['description']))) > 0:
                etl_set_fdw(conn, args.connectionstring, country['description'], dir_version, log_f)
                if args.syntax:
                    from pathfinder_import_ds_syntax import etl_check_country
                    etl_check_country(conn, country['description'], dir_version, log_f)
                if args.fkeys:
                    from pathfinder_import_ds_fkey import etl_check_country_fkeys
                    etl_check_country_fkeys(conn, country['description'], dir_version, log_f)
                if args.delete:
                    from pathfinder_import_ds_del import del_country
                    del_country(conn, country['label'], log_f)
                if args.insert:
                    from pathfinder_import_ds_ins import ins_country_sdesign
                    from pathfinder_import_ds_ins import ins_country_nfiesta
                    from pathfinder_import_ds_ins import available_datasets_check
                    ins_country_sdesign(conn, country['label'], dir_version, log_f)
                    available_datasets_check(conn, country['label'], log_f)
                    ins_country_nfiesta(conn, country['label'], log_f)
                if args.cells:
                    from pathfinder_import_ds_ins import cells_in_check
                    cells_in_check(conn, country['label'], log_f)
                if args.additivityagg:
                    from pathfinder_import_ds_add import check_add_plot_aggregates
                    check_add_plot_aggregates(conn, country['label'], log_f)
                if args.additivitythreshold:
                    from pathfinder_import_ds_add import check_add_plot_threshold
                    check_add_plot_threshold(conn, country['label'], log_f, args.additivitythreshold)
                if args.additivity:
                    from pathfinder_import_ds_add import check_add_plot
                    conn.commit()
                    log_f.close()
                    if args.additivityprocesses is None:
                        check_add_plot(args.connectionstring, conn, country['label'], log_name)
                    else:
                        check_add_plot(args.connectionstring, conn, country['label'], log_name, args.additivityprocesses)
                    log_f = open(log_name, "a")
                if args.zip:
                    import shutil
                    import re
                    regex = re.compile('%s_.*.zip' % country['description'])
                    for root, dirs, files in os.walk(os.path.join('countries', country['description'], 'log')):
                        for f in files:
                            if regex.match(f):
                                pf_print('removing old %s' % f, log_f)
                                os.remove(os.path.join('countries', country['description'], 'log', f))
                    new_zip_fn = '%s_%s' % (country['description'], datetime.now().strftime("%Y-%b-%d-%H-%M-%S"))
                    new_zip_fn_suf = '%s.zip' % new_zip_fn
                    pf_print('creating archive %s' % new_zip_fn_suf, log_f)
                    shutil.make_archive(new_zip_fn, 'zip',  os.path.join('countries', country['description']))
                    shutil.move(new_zip_fn_suf, os.path.join('countries', country['description'], 'log', new_zip_fn_suf))
                if args.use4est:
                    import subprocess
                    subprocess.run(["psql", args.connectionstring, "-f", 'provisioning/provision_cells_use4estimates.sql', "-v", "ON_ERROR_STOP=1"]) #, "-v", "client_min_messages=WARNING", "--quiet"])
            else:
                pf_print('directory for country is empty', log_f)
                sys.exit(1)
        else:
            pf_print('path to country does not exists', log_f)
            sys.exit(1)
        pf_print("%s process completed" % datetime.now().strftime("%Y-%b-%d %H:%M:%S"), log_f)
        log_f.close()
    conn.close()

if __name__ == '__main__':
    main()
