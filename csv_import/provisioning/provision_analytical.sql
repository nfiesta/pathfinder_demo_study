--select * from nfiesta.c_estimation_cell_collection;
with w_data as (
	SELECT * 
	FROM (VALUES 
		(2,'25km-INSPIRE', 'A collection of estimation cells corresponding to 25km INSPIRE grid.', true),
		(3,'50km-INSPIRE', 'A collection of estimation cells corresponding to 50km INSPIRE grid.', true),
		(4,'100km-INSPIRE', 'A collection of estimation cells corresponding to 100km INSPIRE grid.', true)
	) AS t(id, label, description, use4estimates)
)
insert into nfiesta.c_estimation_cell_collection (id, label, description, label_en, description_en, use4estimates)
	select id, label, description, label, description, use4estimates from w_data;
analyze nfiesta.c_estimation_cell_collection;

\set afile `echo $CSF`
--------------------------------
CREATE FOREIGN TABLE csv_server.mask (
        country                 character varying(20)           not null,
        strata_set              character varying(20)           not null,
        stratum                 character varying(20)           not null,
        label                   character varying(120)          not null,
        geometry                text                            not null,
        area_ha                 double precision,
        comment                 text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename:'afile' );

select count(*) from csv_server.mask;

drop table if exists csv_server.inspire_grid;
create table csv_server.inspire_grid as
--explain --analyze verbose
with w_inspire as (
	select 
		substring(label, '[0-9]+')::integer as grid_size_km 
	from nfiesta.c_estimation_cell_collection where use4estimates and label ~ '[0-9]+km-INSPIRE'
)
, w_mask as (
	select
		w_inspire.grid_size_km*1000 as scalex, w_inspire.grid_size_km*(-1000) as  scaley,
		format('%skm', w_inspire.grid_size_km) as label,
		(select st_buffer(st_union(st_geomfromewkt(geometry)), 0) from csv_server.mask) as mask
	from w_inspire
)
, w_cover as (
	select
		scalex, scaley, label,
		mask,
		--st_envelope(mask),
		ST_MakeEnvelope(
			floor(ST_XMin(mask)/scalex)*scalex, 
			floor(ST_YMin(mask)/abs(scaley))*abs(scaley), 
			ceil(ST_XMax(mask)/scalex)*scalex, 
			ceil(ST_YMax(mask)/abs(scaley))*abs(scaley),
			3035) as extent
	from w_mask
)
, w_grid as (
	SELECT
		label,
		(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
		((ST_Xmax(w_cover.extent) - ST_Xmin(w_cover.extent)) / scalex)::int, ((ST_Ymax(w_cover.extent) - ST_Ymin(w_cover.extent)) / abs(scaley))::int,
		ST_Xmin(w_cover.extent),
		ST_Ymax(w_cover.extent),
		scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
	from w_cover
)
select
	row_number() over() as gid,
	format('%s-INSPIRE', w_cover.label) as cell_collection,
	concat(w_cover.label, 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
		w_cover.label, (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
	geom
from w_grid join w_cover on (w_grid.label = w_cover.label)
where st_intersects(w_grid.geom, w_cover.mask)
;

ALTER TABLE csv_server.inspire_grid ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_geom ON csv_server.inspire_grid USING gist (geom);
alter table csv_server.inspire_grid owner to adm_nfiesta;
grant select on table csv_server.inspire_grid to public;

--------------------------------
insert into nfiesta.c_estimation_cell (/*id, */estimation_cell_collection, label, description, label_en, description_en)
        select
		--gid,
		(select id from nfiesta.c_estimation_cell_collection where label = cell_collection),
		label, label, label, label
	from csv_server.inspire_grid
	order by gid;

analyze nfiesta.c_estimation_cell;

insert into nfiesta.f_a_cell (gid, estimation_cell, geom)
        select
		c_estimation_cell.id,
		c_estimation_cell.id,
		st_multi(geom)
	from csv_server.inspire_grid
	join nfiesta.c_estimation_cell on (c_estimation_cell.label = inspire_grid.label)
	order by gid;

analyze nfiesta.f_a_cell;

-- cells collection visualization
/*
CREATE OR REPLACE VIEW public.cells AS
 SELECT c_estimation_cell.id,
    c_estimation_cell.estimation_cell_collection,
    c_estimation_cell.label,
    c_estimation_cell.description,
    c_estimation_cell.label_en,
    c_estimation_cell.description_en,
    f_a_cell.geom,
    c_estimation_cell_collection.label AS estimation_cell_collection_label
   FROM nfiesta.f_a_cell
     JOIN nfiesta.c_estimation_cell ON f_a_cell.estimation_cell = c_estimation_cell.id
     JOIN nfiesta.c_estimation_cell_collection ON c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id
where c_estimation_cell_collection.label = '1km-INSPIRE'
;
*/

------------------------------------t_estimation_cell_hierarchy
--select * from nfiesta.t_estimation_cell_hierarchy;
with w_data as (
	with w_superior_cell as (
		select
			c_estimation_cell.id,
			c_estimation_cell.estimation_cell_collection,
			c_estimation_cell.label,
			f_a_cell.geom
		from nfiesta.c_estimation_cell
		join nfiesta.f_a_cell on f_a_cell.estimation_cell = c_estimation_cell.id
		where c_estimation_cell.estimation_cell_collection != 1
		--and label = '100kmN2964E4620'
		--limit 3
	)
	select
		w_superior_cell.id as sup_id,
		w_superior_cell.estimation_cell_collection as sup_estimation_cell_collection,
		w_superior_cell.label as sup_label,
		c_estimation_cell.id,
		c_estimation_cell.estimation_cell_collection,
		c_estimation_cell.label
	from
		w_superior_cell
		join nfiesta.f_a_cell on st_contains(w_superior_cell.geom, f_a_cell.geom)
		join nfiesta.c_estimation_cell on f_a_cell.estimation_cell = c_estimation_cell.id
		where c_estimation_cell.estimation_cell_collection = w_superior_cell.estimation_cell_collection-1
)
insert into nfiesta.t_estimation_cell_hierarchy (cell, cell_superior)
select id, sup_id from w_data
;

analyze nfiesta.t_estimation_cell_hierarchy;

drop table if exists csv_server.inspire_grid;

ALTER TABLE nfiesta.f_a_cell
  ALTER COLUMN geom TYPE geometry(MultiPolygon, 3035)
    USING ST_SetSRID(geom, 3035);

--code checking hierarchy
/*
CREATE OR REPLACE VIEW public.cell_with_hierarchy AS
with w_cell as (
 SELECT 
	c_estimation_cell.id,
    c_estimation_cell_collection.label AS estimation_cell_collection_label,
    c_estimation_cell.label,
    c_estimation_cell.description,
    f_a_cell.geom
   FROM nfiesta.f_a_cell
     JOIN nfiesta.c_estimation_cell ON f_a_cell.estimation_cell = c_estimation_cell.id
     JOIN nfiesta.c_estimation_cell_collection ON c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id
	 where c_estimation_cell.label = '50kmN3014E4670'
)
select 
	w_cell.id,
	w_cell.estimation_cell_collection_label,
	w_cell.label,
	w_cell.description,
	w_cell.geom,
	array_agg((select label from nfiesta.c_estimation_cell where id = hierarchy_down.cell order by label)) as cell_down,
	(select label from nfiesta.c_estimation_cell where id = hierarchy_up.cell_superior order by label) as cell_superior
from w_cell
left join nfiesta.t_estimation_cell_hierarchy as hierarchy_down on (hierarchy_down.cell_superior = w_cell.id)
left join nfiesta.t_estimation_cell_hierarchy as hierarchy_up on (hierarchy_up.cell = w_cell.id)
group by
 	w_cell.id,
	w_cell.estimation_cell_collection_label,
	w_cell.label,
	w_cell.description,
	w_cell.geom,
	hierarchy_up.cell_superior
;

select label, cell_down from cell_with_hierarchy;

*/

----------------------------------------------------------------------------nfiesta.t_stratum_in_estimation_cell
--select * from nfiesta.c_estimation_cell_collection where c_estimation_cell_collection.use4estimates
with w_data as materialized (
	select 
		t_stratum.id as t_stratum__id,
		t_stratum.stratum,
		c_estimation_cell.id as c_estimation_cell__id,
		c_estimation_cell.label,
		st_multi(st_intersection(f_a_cell.geom, t_stratum.geom)) as intersection_geom,
		st_area(st_intersection(f_a_cell.geom, t_stratum.geom))
	from nfiesta.c_estimation_cell
	join nfiesta.c_estimation_cell_collection ON c_estimation_cell_collection.id = c_estimation_cell.estimation_cell_collection
	join nfiesta.f_a_cell ON f_a_cell.estimation_cell = c_estimation_cell.id
	join sdesign.t_stratum on /*(*/st_intersects(f_a_cell.geom, t_stratum.geom) /*and not st_touches(f_a_cell.geom, t_stratum.geom))*/
	join sdesign.t_strata_set on t_strata_set.id = t_stratum.strata_set
	join sdesign.c_country on c_country.id = t_strata_set.country	
	where 
		c_estimation_cell_collection.use4estimates-- in ('100km-INSPIRE')
		--and c_estimation_cell.label = '25kmN3014E4445'
	--limit 2	
)
insert into nfiesta.t_stratum_in_estimation_cell (stratum, estimation_cell, geom, area_m2) 
select t_stratum__id, c_estimation_cell__id, intersection_geom, st_area from w_data where st_area > 0
;

----------------------------------------------------------------------------cm_plot2cell_mapping
/*
select count(*) from nfiesta.cm_plot2cell_mapping; --78839
*/
with w_data as (
	select 
		cm_plot2cell_mapping.plot,
		cm_plot2cell_mapping.estimation_cell as "1km_cell",
		h25.cell_superior as "25km_cell",
		h50.cell_superior as "50km_cell",
		h100.cell_superior as "100km_cell"
	from (select * from nfiesta.cm_plot2cell_mapping /*limit 3*/) as cm_plot2cell_mapping
	join nfiesta.c_estimation_cell on cm_plot2cell_mapping.estimation_cell = c_estimation_cell.id
	join nfiesta.t_estimation_cell_hierarchy as h25 on h25.cell = c_estimation_cell.id
	join nfiesta.t_estimation_cell_hierarchy as h50 on h50.cell = h25.cell_superior
	join nfiesta.t_estimation_cell_hierarchy as h100 on h100.cell = h50.cell_superior	
)
insert into nfiesta.cm_plot2cell_mapping(plot, estimation_cell)
select plot, unnest(array["25km_cell","50km_cell","100km_cell"]) from w_data;
/*
select 78839*3; --236517
select 78839*4; -- 315356
select count(*) from nfiesta.cm_plot2cell_mapping; -- 315356
*/
