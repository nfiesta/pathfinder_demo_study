------------------------------------------------------------

insert into sdesign.c_country (id, label, description, label_en, description_en)
	select id, label, description, label, description from csv_server.countries order by id;
analyze sdesign.c_country;

insert into nfiesta.c_estimation_cell_collection (label, description, label_en, description_en, use4estimates)
	(select label, description, label, description, false from csv_server.cell_collections where label = '1km-INSPIRE' order by id)
	union all
	(select label, description, label, description, true from csv_server.cell_collections where label IN ('NUTS','NUTS0') order by id);
analyze nfiesta.c_estimation_cell_collection;

\set afile `echo $CSF`
--------------------------------
CREATE FOREIGN TABLE csv_server.mask (
        country                 character varying(20)           not null,
        strata_set              character varying(20)           not null,
        stratum                 character varying(20)           not null,
        label                   character varying(120)          not null,
        geometry                text                            not null,
        area_ha                 double precision,
        comment                 text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename:'afile' );

select count(*) from csv_server.mask;

drop table if exists csv_server.inspire_grid;
create table csv_server.inspire_grid as
--explain --analyze verbose
with w_mask as (
	select
		1000 as scalex, -1000 as  scaley,
		'1km' as label,
		(select st_buffer(st_union(st_geomfromewkt(geometry)), 0) from csv_server.mask) as mask
)
, w_cover as (
	select
		scalex, scaley, label,
		mask,
		--st_envelope(mask),
		ST_MakeEnvelope(
			floor(ST_XMin(mask)/scalex)*scalex, 
			floor(ST_YMin(mask)/abs(scaley))*abs(scaley), 
			ceil(ST_XMax(mask)/scalex)*scalex, 
			ceil(ST_YMax(mask)/abs(scaley))*abs(scaley),
			3035) as extent
	from w_mask
)		
, w_grid as (
	SELECT 
		(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
		((ST_Xmax(w_cover.extent) - ST_Xmin(w_cover.extent)) / scalex)::int, ((ST_Ymax(w_cover.extent) - ST_Ymin(w_cover.extent)) / abs(scaley))::int,
		ST_Xmin(w_cover.extent),
		ST_Ymax(w_cover.extent),
		scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
	from w_cover
)
select
	row_number() over() as gid,
	format('%s-INSPIRE', w_cover.label) as cell_collection,
	concat(w_cover.label, 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
		w_cover.label, (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
	geom
from w_grid, w_cover
where st_intersects(w_grid.geom, w_cover.mask)
;

ALTER TABLE csv_server.inspire_grid ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_geom ON csv_server.inspire_grid USING gist (geom);
alter table csv_server.inspire_grid owner to adm_nfiesta;
grant select on table csv_server.inspire_grid to public;

DROP FOREIGN TABLE csv_server.mask;

--------------------------------

insert into nfiesta.c_estimation_cell (/*id, */estimation_cell_collection, label, description, label_en, description_en)
        select
		--gid,
		(select id from nfiesta.c_estimation_cell_collection where label = cell_collection),
		label, label, label, label
	from csv_server.inspire_grid
	order by gid;

analyze nfiesta.c_estimation_cell;

insert into nfiesta.f_a_cell (gid, estimation_cell, geom)
        select
		c_estimation_cell.id,
		c_estimation_cell.id,
		st_multi(geom)
	from csv_server.inspire_grid
	join nfiesta.c_estimation_cell on (c_estimation_cell.label = inspire_grid.label)
	order by gid;

analyze nfiesta.f_a_cell;

with w_data as (
	with w_superior_cell as (
		select
			c_estimation_cell.id,
			c_estimation_cell.estimation_cell_collection,
			c_estimation_cell.label,
			f_a_cell.geom
		from nfiesta.c_estimation_cell
		join nfiesta.f_a_cell on f_a_cell.estimation_cell = c_estimation_cell.id
		where c_estimation_cell.estimation_cell_collection = 3
		--and c_estimation_cell.label = '50kmN2900E4850'
	)
	select
		w_superior_cell.id as sup_id,
		w_superior_cell.estimation_cell_collection as sup_estimation_cell_collection,
		w_superior_cell.label as sup_label,
		c_estimation_cell.id,
		c_estimation_cell.estimation_cell_collection,
		c_estimation_cell.label
	from
		w_superior_cell
		join nfiesta.f_a_cell on st_contains(w_superior_cell.geom, f_a_cell.geom)
		join nfiesta.c_estimation_cell on f_a_cell.estimation_cell = c_estimation_cell.id
		where c_estimation_cell.estimation_cell_collection = 2
)
insert into nfiesta.t_estimation_cell_hierarchy (cell, cell_superior)
select id, sup_id from w_data
;

analyze nfiesta.t_estimation_cell_hierarchy;

drop table if exists csv_server.inspire_grid;

ALTER TABLE nfiesta.f_a_cell
  ALTER COLUMN geom TYPE geometry(MultiPolygon, 3035)
    USING ST_SetSRID(geom, 3035);
