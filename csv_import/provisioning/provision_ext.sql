\c postgres
alter role adm_nfiesta with superuser ;
set role adm_nfiesta;
drop database if exists pathfinder_ds;
create database pathfinder_ds;
comment on database pathfinder_ds IS 'PathFinder demonstration study.';

\c pathfinder_ds
set role adm_nfiesta;
create extension plpython3u;
create extension postgis;
create extension postgis_raster;
create extension file_fdw;
CREATE SERVER csv_files FOREIGN DATA WRAPPER file_fdw;
create extension htc;
CREATE EXTENSION nfiesta_sdesign WITH VERSION "1.0.1";
create schema nfiesta;
CREATE EXTENSION nfiesta WITH SCHEMA nfiesta VERSION "2.7.0";

--version for CSV 1.0 import
ALTER EXTENSION nfiesta_sdesign UPDATE TO "1.1.12";
ALTER EXTENSION nfiesta UPDATE TO "3.5.4";

--version for CSV 2.0 import
--select version from pg_available_extension_versions where name = 'nfiesta_sdesign' ORDER BY string_to_array(version, '.')::int[] desc limit 1;
ALTER EXTENSION nfiesta_sdesign UPDATE TO "1.1.15";

--select version from pg_available_extension_versions where name = 'nfiesta' ORDER BY string_to_array(version, '.')::int[] desc limit 1;
ALTER EXTENSION nfiesta UPDATE TO "3.18.0";
