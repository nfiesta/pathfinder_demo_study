\set nfiesta_country `echo $NFIESTA_COUNTRY`

DROP SCHEMA IF EXISTS csv_server_nfiesta CASCADE;
CREATE SCHEMA csv_server_nfiesta;
alter schema csv_server_nfiesta owner to adm_nfiesta;


---------------------------------------------------------------------------------------------------
\set afile `pwd` '/server_side/nuts_rg_01m_2021_3035_extent_clip.csv'
CREATE FOREIGN TABLE csv_server_nfiesta.nuts_eurostat (
		id						integer							not null,
		geom					text							not null,
		_uid_					integer							not null,
		nuts_id					text							not null,
		levl_code				integer							not null,
		cntr_code				text							not null,
		name_latn				text							not null,
		nuts_name				text							not null,
		mount_type				integer							not null,
		urbn_type				integer							not null,
		coast_type				integer							not null,
		fid						text							not null
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ',', filename:'afile' );

--select * from csv_server_nfiesta.nuts_eurostat ;
--select * from sdesign.t_strata_set where country in (select id from sdesign.c_country where label in (:'nfiesta_country'));
---------------------------------------------------------------------------------------------------
drop table if exists csv_server_nfiesta.nuts_eurostat4aux;

CREATE TABLE csv_server_nfiesta.nuts_eurostat4aux AS
WITH filtered_data AS (
    SELECT 
        cntr_code, 
        nuts_id, 
        name_latn, 
        levl_code, 
        geom
    FROM 
        csv_server_nfiesta.nuts_eurostat
    WHERE 
        cntr_code IN ( :'nfiesta_country' )
    ORDER BY 
        cntr_code, 
        levl_code, 
        nuts_id
)
SELECT 
    ROW_NUMBER() OVER (ORDER BY cntr_code, levl_code, nuts_id) AS id, 
    *
FROM 
    filtered_data
ORDER BY 
    cntr_code, 
    levl_code, 
    nuts_id;

ALTER TABLE csv_server_nfiesta.nuts_eurostat4aux ADD PRIMARY KEY (id);
CREATE INDEX idx_nuts_eurostat4aux_cntr_code ON csv_server_nfiesta.nuts_eurostat4aux USING btree (cntr_code);
CREATE INDEX idx_nuts_eurostat4aux_levl_code ON csv_server_nfiesta.nuts_eurostat4aux USING btree (levl_code);
alter table csv_server_nfiesta.nuts_eurostat4aux owner to adm_nfiesta;
grant select on table csv_server_nfiesta.nuts_eurostat4aux to public;

---------------------------------------------------------------------
-- select * from nfiesta.c_estimation_cell where estimation_cell_collection not in (1) order by id;
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select 
	id as estimation_cell_collection, label, description, label_en, description_en
from nfiesta.c_estimation_cell_collection where label = 'NUTS'
ON CONFLICT (estimation_cell_collection,label) DO NOTHING;

insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
		case
			when levl_code = 0 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
			when levl_code = 1 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS1')
			when levl_code = 2 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS2')
			when levl_code = 3 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3')
		end
			as estimation_cell_collection,		
		nuts_id as label,
		nuts_id || ' - ' || name_latn as description,
		nuts_id as label_en,
		nuts_id || ' - ' || name_latn as description_en
from csv_server_nfiesta.nuts_eurostat4aux
where levl_code = 0 and	nuts_id in ( :'nfiesta_country' )
order by id; -- 15r

-- select * from nfiesta.c_estimation_cell where estimation_cell_collection not in (1) order by id;
----------------------------------------------------------------------------------------
drop table if exists csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
CREATE TABLE csv_server_nfiesta.nuts_eurostat4aux_hierarchy AS
WITH nuts_filtered AS (
    SELECT 
        cntr_code, nuts_id, name_latn, levl_code, geom
    FROM csv_server_nfiesta.nuts_eurostat
    WHERE cntr_code IN ( :'nfiesta_country' )
),
nuts_hierarchy AS (
    SELECT
        t1.cntr_code AS country_code,
        t1.nuts_id AS nuts3,
        t1.name_latn AS nuts3_label,
        t1.geom,
        t2.nuts_id AS nuts2, t2.name_latn AS nuts2_label,
        t3.nuts_id AS nuts1, t3.name_latn AS nuts1_label,
        t4.nuts_id AS nuts0, t4.name_latn AS nuts0_label
    FROM (SELECT * FROM nuts_filtered WHERE levl_code = 3) AS t1
    LEFT JOIN (SELECT nuts_id, name_latn FROM nuts_filtered WHERE levl_code = 2) AS t2 ON SUBSTRING(t1.nuts_id FROM 1 FOR LENGTH(t1.nuts_id) - 1) = t2.nuts_id
    LEFT JOIN (SELECT nuts_id, name_latn FROM nuts_filtered WHERE levl_code = 1) AS t3 ON SUBSTRING(t2.nuts_id FROM 1 FOR LENGTH(t2.nuts_id) - 1) = t3.nuts_id
    LEFT JOIN (SELECT nuts_id, name_latn FROM nuts_filtered WHERE levl_code = 0) AS t4 ON SUBSTRING(t3.nuts_id FROM 1 FOR LENGTH(t3.nuts_id) - 1) = t4.nuts_id
)
SELECT 
    ROW_NUMBER() OVER (ORDER BY country_code, nuts3, nuts2, nuts1) AS id,
    country_code,
    nuts3,
    nuts3_label,
    ST_SetSRID(ST_GeomFromEWKT(geom), 3035) AS geom,
    nuts2,
    nuts2_label,
    nuts1,
    nuts1_label,
    nuts0,
    nuts0_label
FROM nuts_hierarchy
ORDER BY country_code, nuts3, nuts2, nuts1;
		
ALTER TABLE csv_server_nfiesta.nuts_eurostat4aux_hierarchy ADD PRIMARY KEY (id);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_geom ON csv_server_nfiesta.nuts_eurostat4aux_hierarchy USING gist (geom);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_nuts3 ON csv_server_nfiesta.nuts_eurostat4aux_hierarchy USING btree (nuts3);
alter table csv_server_nfiesta.nuts_eurostat4aux_hierarchy owner to adm_nfiesta;
grant select on table csv_server_nfiesta.nuts_eurostat4aux_hierarchy to public;

----------------------------------------------------------------------------------------
--select * from csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
select setval('nfiesta.f_a_cell_gid_seq',(select coalesce(max(gid)+1,1) from nfiesta.f_a_cell),false);	
with
w1 as	(
		select nuts0 as label, st_multi(st_union(geom)) as geom
		from csv_server_nfiesta.nuts_eurostat4aux_hierarchy
		group by nuts0
		)
,w2 as	(
		select w1.label, w1.geom, t1.id as id_estimation_cell from w1
		inner join	(
					select cec.* from nfiesta.c_estimation_cell as cec
					where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
					) as t1
		on w1.label = t1.label
		)
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell; -- 15r
----------------------------------------------------------------------------------------

delete from nfiesta.f_a_cell where estimation_cell = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS');
with
w1 as	(
		select
				fac.*,
				(select cec.id from nfiesta.c_estimation_cell as cec where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')) as estimation_cell_nuts
		from
				nfiesta.f_a_cell as fac
		where
				fac.estimation_cell in
					(
					select cec.id from nfiesta.c_estimation_cell cec
					where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
					)
		)
,w2 as	(
		select
				w1.estimation_cell_nuts as id_estimation_cell,
				st_multi(st_union(w1.geom)) as geom
		from
				w1 group by w1.estimation_cell_nuts
		)
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2; -- 1r

/*
select c_estimation_cell.*, st_area(f_a_cell.geom)
from nfiesta.f_a_cell
join nfiesta.c_estimation_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
join nfiesta.c_estimation_cell_collection on (c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id)
where c_estimation_cell_collection.label in ('NUTS', 'NUTS0');
*/

---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.c_estimation_cell;
analyze nfiesta.f_a_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w_nuts0 as	(			
			select
					id as cell,
					(
					select id from nfiesta.c_estimation_cell where estimation_cell_collection =
						(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')
					) as cell_superior
			from
					nfiesta.c_estimation_cell
			where
					estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
			)
insert into nfiesta.t_estimation_cell_hierarchy(cell,cell_superior)
select cell, cell_superior from w_nuts0 order by cell; -- 15r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.t_estimation_cell_hierarchy;

---------------------------------------------------------------------------------------------------
-- select count(*) from nfiesta.cm_plot2cell_mapping; => pred insertem 3 100 382 r
with
w1 as	(
		select
				cc.id as id_country,
				cc.label as country,
				-------------------------
				tss.strata_set,
				ts.stratum,
				tp.panel,
				fpp.*
		from
				(select * from sdesign.c_country where label in ( :'nfiesta_country' )) as cc
				inner join sdesign.t_strata_set as tss on cc.id = tss.country
				inner join sdesign.t_stratum as ts on tss.id = ts.strata_set
				inner join sdesign.t_panel as tp on ts.id = tp.stratum
				inner join sdesign.cm_cluster2panel_mapping as ccpm on tp.id = ccpm.panel
				inner join sdesign.t_cluster as tc on ccpm.cluster = tc.id
				inner join sdesign.f_p_plot as fpp on tc.id = fpp.cluster
		)
		--select count(*) from w1; -- 451 259 r
,w2 as	(
		select
				w1.country,
				w1.gid as plot,
				t1.id as estimation_cell
		from
				w1
				inner join	(
							select * from nfiesta.c_estimation_cell where estimation_cell_collection = 
							(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
							) as t1
							on w1.country = t1.label
		)
,w3 as	(
		select
				w2.plot,
				(
				select id from nfiesta.c_estimation_cell where estimation_cell_collection = 
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')
				) as estimation_cell
		from
				w2
		)
,w4 as	(
		select w3.plot, w3.estimation_cell from w3 union all
		select w2.plot, w2.estimation_cell from w2
		)
insert into nfiesta.cm_plot2cell_mapping(plot,estimation_cell)
select plot, estimation_cell from w4;
--select count(*) from w4; -- select 902518 / 2 = 451259
--select count(*) from w2; -- 451259r
--select count(*) from nfiesta.cm_plot2cell_mapping; => po insertu 4 002 900 r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.cm_plot2cell_mapping;


with
w1 as	(
		select
			c_country.label as label_country,
		    t_stratum.id as id_stratum,
		    t_stratum.stratum,
		    t_stratum.geom as geom_stratum
		from sdesign.t_stratum
		join sdesign.t_strata_set on (t_stratum.strata_set = t_strata_set.id)
		join sdesign.c_country ON c_country.id = t_strata_set.country
		where c_country.label in ( :'nfiesta_country' )
		)
,w2 as	(
		select
				w1.id_stratum,
				w1.geom_stratum,
				(select label from nfiesta.c_estimation_cell where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')) as label_country
		from
				w1
		)
,w3 as	(
		select w1.label_country, w1.id_stratum, w1.geom_stratum from w1 union all
		select w2.label_country, w2.id_stratum, w2.geom_stratum from w2
		)
,w4 as	(
		select * from nfiesta.c_estimation_cell where estimation_cell_collection in
		(select id from nfiesta.c_estimation_cell_collection where label in ('NUTS','NUTS0'))
		)
,w5 as	(
		select w3.*, w4.id as id_estimation_cell from w3 inner join w4
		on w3.label_country = w4.label
		)
--select max(id) from nfiesta.t_stratum_in_estimation_cell; -- id od 6588600 do 6588696
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select
		w5.id_stratum as stratum,
		w5.id_estimation_cell as estimation_cell,
		w5.geom_stratum as geom,
		st_area(w5.geom_stratum) area_m2
from
		w5; -- 96r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.t_stratum_in_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--DROP FOREIGN TABLE csv_server_nfiesta.mask;
--DROP FOREIGN TABLE csv_server_nfiesta.nuts_eurostat;
--drop table csv_server_nfiesta.nuts_eurostat4aux;
--drop table csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
--drop table csv_server_nfiesta.nuts_eurostat4aux_hierarchy_intersect;
--drop schema csv_server_nfiesta;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------