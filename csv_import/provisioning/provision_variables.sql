insert into nfiesta.c_target_variable (metadata)
	select metadata from csv_server.target_variables order by id;
analyze nfiesta.c_target_variable;

insert into nfiesta.c_sub_population (label, description, label_en, description_en, atomic)
	select label, description, label, description, true from csv_server.sub_populations order by id;
analyze nfiesta.c_sub_population;

insert into nfiesta.c_sub_population_category (sub_population, label, description, label_en, description_en)
        select
                (select id from nfiesta.c_sub_population where label = sub_population),
                label, description, label, description
        from csv_server.sub_population_categories order by sub_population_categories.id;
analyze nfiesta.c_sub_population_category;

insert into nfiesta.c_area_domain (label, description, label_en, description_en, atomic)
	select label, description, label, description, true from csv_server.area_domains order by id;
analyze nfiesta.c_area_domain;

insert into nfiesta.c_area_domain_category (area_domain, label, description, label_en, description_en)
        select
                (select id from nfiesta.c_area_domain where label = area_domain),
                label, description, label, description
        from csv_server.area_domain_categories order by area_domain_categories.id;
analyze nfiesta.c_area_domain_category;

insert into nfiesta.c_auxiliary_variable (label, description, label_en, description_en)
	select label, description, label, description 
	from csv_server.auxiliary_variables 
	where label in ('tcd', 'fty', 'gfc', 'coordinate_quality')
	order by id 
	;
analyze nfiesta.c_auxiliary_variable;

insert into nfiesta.c_auxiliary_variable_category (auxiliary_variable, label, description, label_en, description_en)
        select
                (select id from nfiesta.c_auxiliary_variable where label = auxiliary_variable),
                label, description, label, description
        from csv_server.auxiliary_variable_categories 
	where auxiliary_variable in ('tcd', 'fty', 'gfc', 'coordinate_quality') and label not in ('n.pixels','sum.weights','n.pixels.all','sum.weights.all')
	order by auxiliary_variable_categories.id;
analyze nfiesta.c_auxiliary_variable_category;

-------------------------------------------------COMBINATIONS------------------------------------------
/*
with w_data as (
	select 
		target_variable, 
		sub_population, 
		area_domain 
	from csv_country.plot_target_data_availability_categories -- need to run countries/csv_country_fdw.sql
	order by target_variable, sub_population, area_domain
)
select 
	array_agg(target_variable order by target_variable) as target_variables, 
	sub_population, 
	area_domain 
from w_data 
group by sub_population, area_domain
	order by 2, 3, 1;

                                                                        target_variables                                                                         |               sub_population               |          area_domain          
-----------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------+-------------------------------
 {agb,domain_agb,dwb_pathfinder,forest,gai_agb_domain,gai_agb_pathfinder,gai_vol_domain,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder} | altogether                                 | L1 forest type
 {agb,domain_agb,dwb_pathfinder,forest,gai_agb_domain,gai_agb_pathfinder,gai_vol_domain,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder} | altogether                                 | L1 forest type+L2 forest type
 {forest_remained_forest}                                                                                                                                        | altogether                                 | silvicultural intervention
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved                  | altogether
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved                  | L1 forest type
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved+deadwood type    | altogether
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved+diameter class   | altogether
 {dwb_pathfinder}                                                                                                                                                | deadwood type                              | altogether
 {dwb_pathfinder}                                                                                                                                                | deadwood type                              | L1 forest type
 {dwb_pathfinder}                                                                                                                                                | deadwood type+diameter class               | altogether
 {agb,dwb_pathfinder,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                 | diameter class                             | altogether
 {agb,dwb_pathfinder,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                 | diameter class                             | L1 forest type
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | diameter class+standing or lying tree      | altogether
 {agb,agb_change,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol,vol_change,vol_pathfinder}                                      | species (26 groups)                        | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)                        | L1 forest type
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)+diameter class         | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)+standing or lying tree | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | standing or lying tree                     | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | standing or lying tree                     | L1 forest type
(19 rows)
*/

insert into nfiesta.c_sub_population(label, description, label_en, description_en, atomic)
values
('coniferous or broadleaved+deadwood type', 'combination of ...', 'coniferous or broadleaved+deadwood type', 'combination of ...', false),
('coniferous or broadleaved+diameter class', 'combination of ...', 'coniferous or broadleaved+diameter class', 'combination of ...', false),
('deadwood type+diameter class', 'combination of ...', 'deadwood type+diameter class', 'combination of ...', false),
('diameter class+standing or lying tree', 'combination of ...', 'diameter class+standing or lying tree', 'combination of ...', false),
('species (26 groups)+diameter class', 'combination of ...', 'species (26 groups)+diameter class', 'combination of ...', false),
('species (26 groups)+standing or lying tree', 'combination of ...', 'species (26 groups)+standing or lying tree', 'combination of ...', false)
;

----------------------------------------------------sub_population_categories
--select label_en from nfiesta.c_sub_population where not atomic;

--for three levels see: https://gitlab.com/nfiesta/pathfinder_demo_study/-/blob/a9a13489bff712db8121de207af5eadf2bec3bfe/csv_import/provisioning/provision_variables.sql#L152-179

--coniferous or broadleaved+deadwood type
with w_1 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'coniferous or broadleaved')
)
, w_2 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'deadwood type')
)
, w_comb as (
        select
                (select id from nfiesta.c_sub_population where label = 'coniferous or broadleaved+deadwood type') as sub_population,
                format('%s+%s', w_1.label, w_2.label) as label,
                format('%s+%s', w_1.description, w_2.description) as description
        from
        w_1, w_2
        --where 
)
insert into nfiesta.c_sub_population_category (sub_population, label, description, label_en, description_en)
select sub_population, label, description, label, description from w_comb
;

--coniferous or broadleaved+diameter class
with w_1 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'coniferous or broadleaved')
)
, w_2 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'diameter class')
)
, w_comb as (
        select
                (select id from nfiesta.c_sub_population where label = 'coniferous or broadleaved+diameter class') as sub_population,
                format('%s+%s', w_1.label, w_2.label) as label,
                format('%s+%s', w_1.description, w_2.description) as description
        from
        w_1, w_2
        --where 
)
insert into nfiesta.c_sub_population_category (sub_population, label, description, label_en, description_en)
select sub_population, label, description, label, description from w_comb
;

--deadwood type+diameter class
with w_1 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'deadwood type')
)
, w_2 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'diameter class')
)
, w_comb as (
        select
                (select id from nfiesta.c_sub_population where label = 'deadwood type+diameter class') as sub_population,
                format('%s+%s', w_1.label, w_2.label) as label,
                format('%s+%s', w_1.description, w_2.description) as description
        from
        w_1, w_2
        --where 
)
insert into nfiesta.c_sub_population_category (sub_population, label, description, label_en, description_en)
select sub_population, label, description, label, description from w_comb
;

--diameter class+standing or lying tree
with w_1 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'diameter class')
)
, w_2 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'standing or lying tree')
)
, w_comb as (
        select
                (select id from nfiesta.c_sub_population where label = 'diameter class+standing or lying tree') as sub_population,
                format('%s+%s', w_1.label, w_2.label) as label,
                format('%s+%s', w_1.description, w_2.description) as description
        from
        w_1, w_2
        --where 
)
insert into nfiesta.c_sub_population_category (sub_population, label, description, label_en, description_en)
select sub_population, label, description, label, description from w_comb
;

--species (26 groups)+diameter class
with w_1 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'species (26 groups)')
)
, w_2 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'diameter class')
)
, w_comb as (
        select
                (select id from nfiesta.c_sub_population where label = 'species (26 groups)+diameter class') as sub_population,
                format('%s+%s', w_1.label, w_2.label) as label,
                format('%s+%s', w_1.description, w_2.description) as description
        from
        w_1, w_2
        --where 
)
insert into nfiesta.c_sub_population_category (sub_population, label, description, label_en, description_en)
select sub_population, label, description, label, description from w_comb
;

--species (26 groups)+standing or lying tree
with w_1 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'species (26 groups)')
)
, w_2 as (
        select *
        from nfiesta.c_sub_population_category
        where sub_population = (select id from nfiesta.c_sub_population where label = 'standing or lying tree')
)
, w_comb as (
        select
                (select id from nfiesta.c_sub_population where label = 'species (26 groups)+standing or lying tree') as sub_population,
                format('%s+%s', w_1.label, w_2.label) as label,
                format('%s+%s', w_1.description, w_2.description) as description
        from
        w_1, w_2
        --where 
)
insert into nfiesta.c_sub_population_category (sub_population, label, description, label_en, description_en)
select sub_population, label, description, label, description from w_comb
;

--select * from nfiesta.c_sub_population_category

---------------------------atomicity

with w_composed as (
	select id, label
	from nfiesta.c_sub_population_category 
	where sub_population in (select id from nfiesta.c_sub_population where atomic = false)
)
, w_atomic as (
select 
	id as composed_id, unnest(string_to_array(label, '+')) as atomic_label
from w_composed
)
insert into nfiesta.cm_sub_population_category (sub_population_category, atomic_category)
select 
	composed_id, 
	(select id from nfiesta.c_sub_population_category where label = atomic_label) as atomic_id
from w_atomic
;

----------------------------------------------------area_domain
insert into nfiesta.c_area_domain(label, description, label_en, description_en, atomic)
values
('L1 forest type+L2 forest type', 'combination of ...', 'L1 forest type+L2 forest type', 'combination of ...', false)
;

----------------------------------------------------area_domain_categories
--select * from nfiesta.c_area_domain where not atomic;
--L1 forest type+L2 forest type
with w_l1 as (
	select * 
	from nfiesta.c_area_domain_category 
	where area_domain = (select id from nfiesta.c_area_domain where label = 'L1 forest type')	
)
, w_l2 as (
	select * 
	from nfiesta.c_area_domain_category 
	where area_domain = (select id from nfiesta.c_area_domain where label = 'L2 forest type')
)
, w_comb as (
	select 
		(select id from nfiesta.c_area_domain where label = 'L1 forest type+L2 forest type') as area_domain,
		format('%s+%s', w_l1.label, w_l2.label) as label,
		format('%s+%s', w_l1.description, w_l2.description) as description
	from
	w_l1, w_l2 
	where substring(w_l1.description, '[^.]*') = substring(w_l2.description, '[^.]*')
)
insert into nfiesta.c_area_domain_category (area_domain, label, description, label_en, description_en)
select area_domain, label, description, label, description from w_comb
;

---------------------------atomicity

with w_composed as (
	select id, label
	from nfiesta.c_area_domain_category 
	where area_domain in (select id from nfiesta.c_area_domain where atomic = false)		
)
, w_atomic as (
select 
	id as composed_id, unnest(string_to_array(label, '+')) as atomic_label
from w_composed
)
insert into nfiesta.cm_area_domain_category (area_domain_category, atomic_category)
select 
	composed_id, 
	(select id from nfiesta.c_area_domain_category where label = atomic_label) as atomic_id
from w_atomic
;

----------------------------------------------------------------t_variable
--delete from nfiesta.t_variable;
--select * from nfiesta.t_variable;

--altogether for all target variables
insert into nfiesta.t_variable(target_variable)
select 
	--c_target_variable.metadata->'en'->>'pf_label',
	c_target_variable.id
from 
	nfiesta.c_target_variable
;

--select * from nfiesta.c_auxiliary_variable_category;
--select * from nfiesta.t_variable where auxiliary_variable_category is not null;
insert into nfiesta.t_variable(auxiliary_variable_category)
select
	c_auxiliary_variable_category.id
from 
	nfiesta.c_auxiliary_variable_category
;

-----------------------------------------------combinations-----------------------------------------------
/*
with w_data as (
        select
                target_variable,
                sub_population,
                area_domain
        from csv_country.plot_target_data_availability_categories
        order by target_variable, sub_population, area_domain
)
select
        array_agg(target_variable order by target_variable) as target_variables,
        sub_population,
        area_domain
from w_data
group by sub_population, area_domain
        order by 3,2;
                                                                        target_variables                                                                         |               sub_population               |          area_domain          
-----------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------+-------------------------------
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved                  | altogether
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved+deadwood type    | altogether
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved+diameter class   | altogether
 {dwb_pathfinder}                                                                                                                                                | deadwood type                              | altogether
 {dwb_pathfinder}                                                                                                                                                | deadwood type+diameter class               | altogether
 {agb,dwb_pathfinder,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                 | diameter class                             | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | diameter class+standing or lying tree      | altogether
 {agb,agb_change,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol,vol_change,vol_pathfinder}                                      | species (26 groups)                        | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)+diameter class         | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)+standing or lying tree | altogether
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | standing or lying tree                     | altogether
 {agb,domain_agb,dwb_pathfinder,forest,gai_agb_domain,gai_agb_pathfinder,gai_vol_domain,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder} | altogether                                 | L1 forest type
 {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved                  | L1 forest type
 {dwb_pathfinder}                                                                                                                                                | deadwood type                              | L1 forest type
 {agb,dwb_pathfinder,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                 | diameter class                             | L1 forest type
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)                        | L1 forest type
 {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | standing or lying tree                     | L1 forest type
 {agb,domain_agb,dwb_pathfinder,forest,gai_agb_domain,gai_agb_pathfinder,gai_vol_domain,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder} | altogether                                 | L1 forest type+L2 forest type
 {forest_remained_forest}                                                                                                                                        | altogether                                 | silvicultural intervention
(19 rows)
*/

-- {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved                  | altogether
-- {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved+deadwood type    | altogether
-- {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved+diameter class   | altogether
-- {dwb_pathfinder}                                                                                                                                                | deadwood type                              | altogether
-- {dwb_pathfinder}                                                                                                                                                | deadwood type+diameter class               | altogether
insert into nfiesta.t_variable(target_variable, sub_population_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in ('dwb_pathfinder')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in (
		'coniferous or broadleaved',
		'coniferous or broadleaved+deadwood type',
		'coniferous or broadleaved+diameter class',
		'deadwood type',
		'deadwood type+diameter class'))
;

-- {agb,dwb_pathfinder,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                 | diameter class                             | altogether
insert into nfiesta.t_variable(target_variable, sub_population_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in (
		'agb',
		'dwb_pathfinder',
		'gai_agb_pathfinder',
		'gai_vol_pathfinder',
		'nai_agb_pathfinder',
		'nai_vol_pathfinder',
		'vol_pathfinder', 
		'basal_area')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in ('diameter class'))
;

-- {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | diameter class+standing or lying tree      | altogether
insert into nfiesta.t_variable(target_variable, sub_population_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in (
		'agb',
		'gai_agb_pathfinder',
		'gai_vol_pathfinder',
		'nai_agb_pathfinder',
		'nai_vol_pathfinder',
		'vol_pathfinder',
		'basal_area')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in ('diameter class+standing or lying tree'))
;

-- {agb,agb_change,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol,vol_change,vol_pathfinder}                                      | species (26 groups)                        | altogether
insert into nfiesta.t_variable(target_variable, sub_population_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in (
		'agb',
		'agb_change',
		'gai_agb_pathfinder',
		'gai_vol_pathfinder',
		'nai_agb_pathfinder',
		'nai_vol_pathfinder',
		'vol',
		'vol_change',
		'vol_pathfinder',
		'basal_area')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in ('species (26 groups)'))
;	

-- {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)+diameter class         | altogether
-- {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)+standing or lying tree | altogether
-- {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | standing or lying tree                     | altogether
insert into nfiesta.t_variable(target_variable, sub_population_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in (
		'agb',
		'gai_agb_pathfinder',
		'gai_vol_pathfinder',
		'nai_agb_pathfinder',
		'nai_vol_pathfinder',
		'vol_pathfinder',
		'basal_area')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in (
		'species (26 groups)+diameter class',
		'species (26 groups)+standing or lying tree',
		'standing or lying tree'))
;

-- {agb,domain_agb,dwb_pathfinder,forest,gai_agb_domain,gai_agb_pathfinder,gai_vol_domain,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder} | altogether                                 | L1 forest type
-- {agb,domain_agb,dwb_pathfinder,forest,gai_agb_domain,gai_agb_pathfinder,gai_vol_domain,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder} | altogether                                 | L1 forest type+L2 forest type

insert into nfiesta.t_variable(target_variable, area_domain_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_area_domain_category.label,
	c_area_domain_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_area_domain_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in (
		'agb',
		'domain_agb',
		'dwb_pathfinder',
		'forest',
		'gai_agb_domain',
		'gai_agb_pathfinder',
		'gai_vol_domain',
		'gai_vol_pathfinder',
		'nai_agb_pathfinder',
		'nai_vol_pathfinder',
		'vol_pathfinder',
		'basal_area')
	and c_area_domain_category.area_domain in (select id from nfiesta.c_area_domain where label in (
		'L1 forest type', 
		'L1 forest type+L2 forest type'))
;	

-- {forest_remained_forest}                                                                                                                                        | altogether                                 | silvicultural_intervention

insert into nfiesta.t_variable(target_variable, area_domain_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_area_domain_category.label,
	c_area_domain_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_area_domain_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in (
		'forest_remained_forest')
	and c_area_domain_category.area_domain in (select id from nfiesta.c_area_domain where label in (
		'silvicultural intervention'))
;	

-- {dwb_pathfinder}                                                                                                                                                | coniferous or broadleaved                  | L1 forest type
-- {dwb_pathfinder}                                                                                                                                                | deadwood type                              | L1 forest type
insert into nfiesta.t_variable(target_variable, sub_population_category, area_domain_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id,
	--c_area_domain_category.label,
	c_area_domain_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category, nfiesta.c_area_domain_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in ('dwb_pathfinder')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in ('deadwood type','coniferous or broadleaved'))
	and c_area_domain_category.area_domain in (select id from nfiesta.c_area_domain where label in ('L1 forest type'))
;

-- {agb,dwb_pathfinder,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                 | diameter class                             | L1 forest type
insert into nfiesta.t_variable(target_variable, sub_population_category, area_domain_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id,
	--c_area_domain_category.label,
	c_area_domain_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category, nfiesta.c_area_domain_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in ('agb','dwb_pathfinder','gai_agb_pathfinder','gai_vol_pathfinder','nai_agb_pathfinder','nai_vol_pathfinder','vol_pathfinder','basal_area')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in ('diameter class'))
	and c_area_domain_category.area_domain in (select id from nfiesta.c_area_domain where label in ('L1 forest type'))
;

-- {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | species (26 groups)                        | L1 forest type
-- {agb,gai_agb_pathfinder,gai_vol_pathfinder,nai_agb_pathfinder,nai_vol_pathfinder,vol_pathfinder}                                                                | standing or lying tree                     | L1 forest type
insert into nfiesta.t_variable(target_variable, sub_population_category, area_domain_category)
select 
	--c_target_variable.metadata->'en'->>'pf_label' as pf_label,
	c_target_variable.id, 
	--c_sub_population_category.label,
	c_sub_population_category.id,
	--c_area_domain_category.label,
	c_area_domain_category.id
from 
	nfiesta.c_target_variable, nfiesta.c_sub_population_category, nfiesta.c_area_domain_category
where 
	c_target_variable.metadata->'en'->>'pf_label' in ('agb','gai_agb_pathfinder','gai_vol_pathfinder','nai_agb_pathfinder','nai_vol_pathfinder','vol_pathfinder','basal_area')
	and c_sub_population_category.sub_population in (select id from nfiesta.c_sub_population where label in ('species (26 groups)', 'standing or lying tree'))
	and c_area_domain_category.area_domain in (select id from nfiesta.c_area_domain where label in ('L1 forest type'))
;

analyze nfiesta.t_variable;
--select count(*) from nfiesta.t_variable;
--------------------------------------------------------hierarchies for additivity--------------------------------------------------------

/*
select
	target_variable,
	sub_population,
	area_domain	,
	count(*), round(sum(value)::numeric, 5)
from csv_country.plot_target_data
group by target_variable, sub_population, area_domain	
order by target_variable, sub_population, area_domain	
;

 target_variable |              sub_population              |  area_domain   | count  |     round
-----------------+------------------------------------------+----------------+--------+---------------
 agb             | altogether                               | altogether     |  21156 | 4672322.57101
 agb             | altogether                               | L1 forest type |  21156 | 4672322.57101
 agb             | diameter class                           | altogether     | 104458 | 4672322.57101
 agb             | diameter class                           | L1 forest type | 104458 | 4672322.57101
 agb             | species (26 groups)                      | altogether     |  56651 | 4672322.57101
 agb             | species (26 groups)                      | L1 forest type |  56651 | 4672322.57101
 agb             | species (26 groups)+diameter class       | altogether     | 143617 | 4672322.57101
 agb_change      | altogether                               | altogether     |   7264 |    5039.52643
 agb_change      | species (26 groups)                      | altogether     |  21088 |    5039.52643
 domain_agb      | altogether                               | altogether     |  21321 |   21321.00000
 domain_agb      | altogether                               | L1 forest type |  21321 |   21321.00000
 domain_vol      | altogether                               | altogether     |  21321 |   21321.00000
 dwb_pathfinder  | altogether                               | altogether     |  20640 |  407301.12863
 dwb_pathfinder  | altogether                               | L1 forest type |  20640 |  407301.12863
 dwb_pathfinder  | coniferous or broadleaved                | altogether     |  28962 |  407301.12863
 dwb_pathfinder  | coniferous or broadleaved                | L1 forest type |  28962 |  407301.12863
 dwb_pathfinder  | coniferous or broadleaved+deadwood type  | altogether     |  48142 |  407301.12863
 dwb_pathfinder  | coniferous or broadleaved+diameter class | altogether     | 115827 |  407301.12863
 dwb_pathfinder  | deadwood type                            | altogether     |  38983 |  407301.12863
 dwb_pathfinder  | deadwood type                            | L1 forest type |  38983 |  407301.12863
 dwb_pathfinder  | deadwood type+diameter class             | altogether     | 120332 |  407301.12863
 dwb_pathfinder  | diameter class                           | altogether     | 106104 |  407301.12863
 dwb_pathfinder  | diameter class                           | L1 forest type | 106104 |  407301.12863
 forest          | altogether                               | altogether     |  21842 |   21842.00000
 forest          | altogether                               | L1 forest type |  21842 |   21842.00000
 vol             | altogether                               | altogether     |  21156 | 7081390.94883
 vol             | species (26 groups)                      | altogether     |  56651 | 7081390.94883
 vol_change      | altogether                               | altogether     |   7264 |    1773.01727
 vol_change      | species (26 groups)                      | altogether     |  20836 |    1773.01727
 vol_pathfinder  | altogether                               | altogether     |   6788 | 2409249.56446
 vol_pathfinder  | altogether                               | L1 forest type |   6787 | 2409249.56446
 vol_pathfinder  | diameter class                           | altogether     |  28466 | 2409249.56446
 vol_pathfinder  | diameter class                           | L1 forest type |  28466 | 2409249.56446
 vol_pathfinder  | species (26 groups)                      | altogether     |  15047 | 2409249.56446
 vol_pathfinder  | species (26 groups)                      | L1 forest type |  15047 | 2409249.56446
 vol_pathfinder  | species (26 groups)+diameter class       | altogether     |  37343 | 2409249.56446
(36 rows)
*/

with w_data as(
	select distinct
		c_target_variable.id as target_variable__id, c_target_variable.metadata->'en'->>'pf_label' as target_variable,
		c_sub_population_category.sub_population, coalesce(c_sub_population.label, 'altogether') as sub_population_label,
		c_area_domain_category.area_domain, coalesce(c_area_domain.label, 'altogether') as area_domain_label
	from nfiesta.t_variable
	join nfiesta.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join nfiesta.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join nfiesta.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
	left join nfiesta.c_sub_population on (c_sub_population_category.sub_population = c_sub_population.id)
	left join nfiesta.c_area_domain on (c_area_domain_category.area_domain = c_area_domain.id)
	--where c_target_variable.metadata->'en'->>'pf_label' in ('agb', 'agb_change', 'domain_agb', 'domain_vol', 'dwb_pathfinder', 'forest', 'vol', 'vol_change', 'vol_pathfinder')
	order by 2, 4, 6	
)
, w_variable_superior as (
	select distinct
		t_variable.id as t_variable_sup__id,
		t_variable.target_variable
	from nfiesta.t_variable 
	join w_data on (t_variable.target_variable = w_data.target_variable__id)
	where 	 
			(t_variable.sub_population_category is null and t_variable.area_domain_category is null) 
			and t_variable.auxiliary_variable_category is null	
)
, w_variable as (
	select
		t_variable.id as t_variable__id,
		t_variable.target_variable 
	from nfiesta.t_variable 
	left join nfiesta.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join nfiesta.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
	join w_data on (	t_variable.target_variable = w_data.target_variable__id
						and coalesce(c_sub_population_category.sub_population, 0) = coalesce(w_data.sub_population, 0)
						and coalesce(c_area_domain_category.area_domain, 0) = coalesce(w_data.area_domain, 0))
	where 	 
			(not (t_variable.sub_population_category is null and t_variable.area_domain_category is null))
			and t_variable.auxiliary_variable_category is null	
)
--delete from nfiesta.t_variable_hierarchy; 
insert into nfiesta.t_variable_hierarchy(variable, variable_superior)
select t_variable__id, t_variable_sup__id
	from w_variable_superior join w_variable using (target_variable)
;

--select * from nfiesta.t_variable_hierarchy;
--select * from nfiesta.t_additivity_set_plot;

analyze nfiesta.t_variable_hierarchy;


INSERT INTO nfiesta.c_topic (label, description, label_en, description_en) VALUES
	('lesní zdroje FE/C1', 'Lesní zdroje FE/C1.', 'forest resources FE/C1', 'Forest resources FE/C1.'),
	('zdravotní stav a vitalita FE/C2', 'Zdravotní stav a vitalita lesů FE/C2.', 'forest health and vitality FE/C2', 'Forest health and vitality FE/C2.'),
	('produkční funkce FE/C3', 'Produkční funkce lesů FE/C3.', 'productive functions FE/C3', 'Productive functions of forests FE/C3.'),
	('biodiverzita lesů FE/C4', 'Biodiverzita lesů FE/C4.', 'forest biodiversity FE/C4', 'Biodiversity of forests FE/C4.'),
	('ochranné funkce lesů FE/C5', 'Ochranné funkce lesů FE/C5.', 'forest protective functions FE/C5', 'Protective functions of forests FE/C5.'),
	('socioekonomické funkce FE/C6', 'Socioekonomické funkce lesů FE/C6.', 'socioeconomic functions FE/C6', 'Socioeconomic functions of forests FE/C6.');
 
 
--SELECT * FROM nfiesta.c_topic;
analyze nfiesta.c_topic;

delete from  nfiesta.cm_tvariable2topic;
INSERT INTO nfiesta.cm_tvariable2topic (target_variable, topic) VALUES
(1,1), (1,3),
(2,1), (2,3),
(3,1), (3,3),
(4,1), (4,3),
(5,1), (5,3),
(6,1), (6,3),
(7,1), (7,3),
(8,1), (8,3),
(10,4),
(12,4),
(13,1), (13,3),
(14,3),
(15,3),
(16,3),
(17,3),
(18,3),
(19,3),
(20,4),
(21,1),(21,3)
;

/*select id from nfiesta.c_target_variable where metadata->'en'->>'pf_label' = 'basal_area'
 id
----
 21*/
analyze nfiesta.cm_tvariable2topic;
