--CREATE SCHEMA csv_server_nfiesta;
/*
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

\set afile '/home/vagrant/pathfinder_demo_study/csv_import/server_side/EU_mask.csv'
CREATE FOREIGN TABLE csv_server_nfiesta.mask (
        country                 character varying(20)           not null,
        strata_set              character varying(20)           not null,
        stratum                 character varying(20)           not null,
        label                   character varying(120)          not null,
        geometry                text                            not null,
        area_ha                 double precision,
        comment                 text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename:'afile' );

---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
*/
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
\set afile '/home/lukas/pathfinder_demo_study/csv_import/server_side/biogeoregions2016.csv'
CREATE FOREIGN TABLE csv_server_nfiesta.biogeoregions2016_origin (
	geometry                text			        not null,
        pk_uid                 	integer         	        not null,
        short_name              character varying               not null,
        pre_2012                character varying               not null,
        code                   	character varying               not null,
        name                 	character varying	        not null
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ',', filename:'afile' );
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_nfiesta.biogeoregions2016 as
select
        row_number() over() as gid,
        short_name,
        pre_2012,
        code,
        name,
        st_setsrid(ST_GeomFromEWKT(geometry),3035) as geom	
from
        csv_server_nfiesta.biogeoregions2016_origin
where
        short_name is distinct from 'outside'
order
        by pk_uid;
		
ALTER TABLE csv_server_nfiesta.biogeoregions2016 ADD PRIMARY KEY (gid);
CREATE INDEX idx_biogeoregions2016_geom ON csv_server_nfiesta.biogeoregions2016 USING gist (geom);
alter table csv_server_nfiesta.biogeoregions2016 owner to adm_nfiesta;
grant select on table csv_server_nfiesta.biogeoregions2016 to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_nfiesta.biogeoregions2016_intersection as
with
w1 as	(
        select * from csv_server_nfiesta.biogeoregions2016
        )
,w2 as	(
        select 1 as gid, st_geomfromewkt(geometry) as geom from csv_server_nfiesta.mask
        )
,w3 as	(
        select
                        w1.gid as region_gid,
                        w1.code,
                        st_intersection(w1.geom,w2.geom) as geom
        from w1, w2
        where w1.geom && w2.geom
        and st_intersects(w1.geom, w2.geom)
        )	
select
        row_number() over() as gid,
        w3.region_gid,
        w3.code,
        w3.geom
from
        w3;

ALTER TABLE csv_server_nfiesta.biogeoregions2016_intersection ADD PRIMARY KEY (gid);
CREATE INDEX idx_biogeoregions2016_intersection_geom ON csv_server_nfiesta.biogeoregions2016_intersection USING gist (geom);
alter table csv_server_nfiesta.biogeoregions2016_intersection owner to adm_nfiesta;
grant select on table csv_server_nfiesta.biogeoregions2016_intersection to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_nfiesta.biogeoregions2016_hierarchy
(
	id serial primary key,
	region_gid integer,
	code varchar,
	gid integer,
	label varchar,
	geom public.geometry
);

with
w1 as	(
	select * from csv_server_nfiesta.biogeoregions2016_intersection
	)
,w2 as	(
	select * from csv_server_nfiesta.nuts_eurostat4aux_hierarchy_intersect
	)
,w3 as	(
        select
                w1.gid as region_gid,
                w1.code,
                w2.gid,
                w2.label,
                st_setsrid(w2.geom,3035) as geom
        from w1, w2
        where w1.geom && w2.geom
        and st_intersects(w1.geom, st_setsrid(st_centroid(w2.geom),3035))
        )
insert into csv_server_nfiesta.biogeoregions2016_hierarchy(region_gid,code,gid,label,geom)
select
        w3.region_gid,
        w3.code,
        w3.gid,
        w3.label,
        w3.geom
from
        w3; -- cca 5 min
	
CREATE INDEX idx_biogeoregions2016_hierarchy_region_gid	ON csv_server_nfiesta.biogeoregions2016_hierarchy USING btree (region_gid);
CREATE INDEX idx_biogeoregions2016_hierarchy_code		ON csv_server_nfiesta.biogeoregions2016_hierarchy USING btree (code);
CREATE INDEX idx_biogeoregions2016_hierarchy_gid		ON csv_server_nfiesta.biogeoregions2016_hierarchy USING btree (gid);
CREATE INDEX idx_biogeoregions2016_hierarchy_label		ON csv_server_nfiesta.biogeoregions2016_hierarchy USING btree (label);
alter table csv_server_nfiesta.biogeoregions2016_hierarchy owner to adm_nfiesta;
grant select on table csv_server_nfiesta.biogeoregions2016_hierarchy to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
        select
                gid,
                label,
                st_setsrid(st_centroid(geom),3035) as geom_centroid,
                geom,
                st_buffer(st_setsrid(st_centroid(geom),3035),100000) as geom_centroid_buffer
        from
                csv_server_nfiesta.nuts_eurostat4aux_hierarchy_intersect
        where
                gid not in (select gid from csv_server_nfiesta.biogeoregions2016_hierarchy)
        )
,w2 as	(
        select
                w1.gid,
                w1.label,
                w1.geom_centroid,
                w1.geom,
                st_distance(w1.geom_centroid,w2.geom) as geom_distance,
                w2.gid as region_gid,
                w2.code
        from w1, csv_server_nfiesta.biogeoregions2016_intersection as w2
        where w1.geom_centroid_buffer && w2.geom
        --and st_intersects(w1.geom_centroid_buffer,w2.geom)
        )		
,w3 as	(
        select gid, min(geom_distance) as min_geom_distance from w2 group by gid
        )
,w4 as	(
        select w2.* from w2 inner join w3 on w2.gid = w3.gid and w2.geom_distance = w3.min_geom_distance
        )
insert into csv_server_nfiesta.biogeoregions2016_hierarchy(region_gid,code,gid,label,geom)
select
        w4.region_gid,
        w4.code,
        w4.gid,
        w4.label,
        w4.geom
from
        w4 order by gid;  -- 16 174 r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--delete from csv_server_nfiesta.biogeoregions2016_hierarchy where gid = 5206523 and code = 'Pannonian';
delete from csv_server_nfiesta.biogeoregions2016_hierarchy
where gid =     (
                select t.gid
                from    (
                        select gid, count(gid) as count_of_gids
                        from csv_server_nfiesta.biogeoregions2016_hierarchy group by gid
                        ) as t
                where t.count_of_gids = 2
                )
and code = 'Pannonian';
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
INSERT INTO nfiesta.c_estimation_cell_collection(label,description,label_en,description_en)
VALUES
('BIOGEOREGIONS','BIOGEOREGIONS','BIOGEOREGIONS','BIOGEOREGIONS'),
('BIOGEOREGION','BIOGEOREGION','BIOGEOREGION','BIOGEOREGION'),
('BIOGEOREGION-1km-inspire-APPROXIMATE','BIOGEOREGION-1km-inspire-APPROXIMATE','BIOGEOREGION-1km-inspire-APPROXIMATE','BIOGEOREGION-1km-inspire-APPROXIMATE');
update nfiesta.c_estimation_cell_collection set use4estimates = false where label = 'BIOGEOREGION-1km-inspire-APPROXIMATE';
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
        (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS') as estimation_cell_collection,
        (select label from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS') as label,
        (select description from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS') as description,
        (select label_en from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS') as label_en,
        (select description_en from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS') as description_en;
---------------------------------------
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
        (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION') as estimation_cell_collection,
        code as label,
        code as description,
        code as label_en,
        code as description_en
from
        csv_server_nfiesta.biogeoregions2016_intersection
--where
        --code in ('Alpine','Arctic','Atlantic','Boreal','Continental','Macaronesia','Mediterranean','Pannonian')
order
        by gid;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
        (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION-1km-inspire-APPROXIMATE') as estimation_cell_collection,	
        label as label,
        label as description,
        label label_en,
        label as description_en
from
        csv_server_nfiesta.biogeoregions2016_hierarchy order by id; -- 3 934 692r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------	
analyze nfiesta.c_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
        select t1.label, t1.geom, t2.id as id_estimation_cell from
        (select label, st_multi(geom) as geom from csv_server_nfiesta.biogeoregions2016_hierarchy) as t1
        inner join (select cec.* from nfiesta.c_estimation_cell as cec where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION-1km-inspire-APPROXIMATE')) as t2
        on t1.label = t2.label
        )
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w1.geom, w1.id_estimation_cell from w1 order by id_estimation_cell;  -- 3 934 692r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
        select code as label, st_multi(st_union(geom)) as geom
        from csv_server_nfiesta.biogeoregions2016_hierarchy
        group by code
        )
,w2 as	(
        select w1.label, w1.geom, t1.id as id_estimation_cell from w1
        inner join	(
                                select cec.* from nfiesta.c_estimation_cell as cec
                                where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION')
                                ) as t1
        on w1.label = t1.label
        )
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell; -- 8r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
        select
                        (
                        select cec.id from nfiesta.c_estimation_cell as cec
                        where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS')
                        ) as id_estimation_cell,
                        geom
        from
                        csv_server_nfiesta.biogeoregions2016_hierarchy
        )
,w2 as	(
        select w1.id_estimation_cell, st_multi(st_union(w1.geom)) as geom from w1 group by w1.id_estimation_cell
        )
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2; -- 1r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.f_a_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w_biogeoregion as	(
                    select
                    		id as cell,
                    		(
                            select id from nfiesta.c_estimation_cell where estimation_cell_collection =
                            (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS')
                    		) as cell_superior
                    from
                    		nfiesta.c_estimation_cell
                    where
                    		estimation_cell_collection =
                    		(select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION')
                    and
                    		id in
                    		(
                    		select estimation_cell from nfiesta.f_a_cell where estimation_cell in
                   			(select id from nfiesta.c_estimation_cell where estimation_cell_collection =
                   			(select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION'))
                   			)
                        )
,w_biogeoregion_1km as	(
                        select
                                t1.id_estimation_cell_down as cell,
                                t1.label_down,
                                t2.code as label_up,
                                t3.id as cell_superior
                        from
                                (
                                select id as id_estimation_cell_down, label as label_down from nfiesta.c_estimation_cell where estimation_cell_collection =
                                (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION-1km-inspire-APPROXIMATE')
                                ) as t1
                        inner join csv_server_nfiesta.biogeoregions2016_hierarchy as t2 on t1.label_down = t2.label
                        inner join 
                                (
                                select * from nfiesta.c_estimation_cell where estimation_cell_collection =
                                (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION')
                                ) as t3 on t2.code = t3.label						
                        )
,w_res as	(
                select cell, cell_superior from w_biogeoregion union all
                select cell, cell_superior from w_biogeoregion_1km
                )
insert into nfiesta.t_estimation_cell_hierarchy(cell,cell_superior)
select cell, cell_superior from w_res order by cell; -- 3 934 700 r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.t_estimation_cell_hierarchy;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				t1.id,
				t1.plot,
				t1.label,
				t2.id as id_estimation_cell_biogeoregion_1km
		from
				csv_server_nfiesta.cm_plot2cell_mapping_backup as t1
		inner
		join	(
				select * from nfiesta.c_estimation_cell where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION-1km-inspire-APPROXIMATE')
				) as t2
		on		t1.label = t2.label
		)
,w2 as	(
		select
				w1.*,
				t2.cell_superior as id_estimation_cell_biogeoregion,
				(
				select id from nfiesta.c_estimation_cell where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS')
				) as id_estimation_cell_biogeoregions
		from
				w1				
				inner join nfiesta.t_estimation_cell_hierarchy as t2 on w1.id_estimation_cell_biogeoregion_1km = t2.cell
		)
,w3 as	(
		select
				w2.plot,
				array[
					id_estimation_cell_biogeoregion_1km,
					id_estimation_cell_biogeoregion,
					id_estimation_cell_biogeoregions
					] as id_estimation_cell_array
		from w2
		)
,w4 as	(
		select w3.plot, unnest(w3.id_estimation_cell_array) as id_estimation_cell from w3
		)
insert into nfiesta.cm_plot2cell_mapping(plot,estimation_cell)
select plot, id_estimation_cell from w4; -- 6 442 854 r -- cca 2 min
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.cm_plot2cell_mapping;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
DROP FOREIGN TABLE csv_server_nfiesta.biogeoregions2016_origin;
DROP FOREIGN TABLE csv_server_nfiesta.mask;
drop table csv_server_nfiesta.biogeoregions2016;
drop table csv_server_nfiesta.biogeoregions2016_hierarchy;
drop table csv_server_nfiesta.biogeoregions2016_intersection;
drop table csv_server_nfiesta.inspire_grid_1km;
drop table csv_server_nfiesta.nuts_eurostat4aux_hierarchy_intersect;
drop table csv_server_nfiesta.cm_plot2cell_mapping_backup;
drop schema csv_server_nfiesta;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
