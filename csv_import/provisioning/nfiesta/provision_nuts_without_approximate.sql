---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
CREATE SCHEMA csv_server_nfiesta;
alter schema csv_server_nfiesta owner to adm_nfiesta;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
\set afile '/home/lukas/pathfinder_demo_study/csv_import/server_side/nuts_rg_01m_2021_3035_extent_clip.csv'
CREATE FOREIGN TABLE csv_server_nfiesta.nuts_eurostat (
		id						integer							not null,
		geom					text							not null,
		_uid_					integer							not null,
		nuts_id					text							not null,
		levl_code				integer							not null,
		cntr_code				text							not null,
		name_latn				text							not null,
		nuts_name				text							not null,
		mount_type				integer							not null,
		urbn_type				integer							not null,
		coast_type				integer							not null,
		fid						text							not null
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ',', filename:'afile' );
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
drop table if exists csv_server_nfiesta.nuts_eurostat4aux;
create table csv_server_nfiesta.nuts_eurostat4aux as
with
w1 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'AT' order by cntr_code, levl_code, nuts_id),
w2 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'BE' order by cntr_code, levl_code, nuts_id),
w3 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'CH' order by cntr_code, levl_code, nuts_id),
w4 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'CZ' order by cntr_code, levl_code, nuts_id),
w5 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'DE' order by cntr_code, levl_code, nuts_id),
w6 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'DK' order by cntr_code, levl_code, nuts_id),
w7 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'EE' order by cntr_code, levl_code, nuts_id),
w8 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'ES' order by cntr_code, levl_code, nuts_id),
w9 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'FI' order by cntr_code, levl_code, nuts_id),
w10 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'FR' order by cntr_code, levl_code, nuts_id),
w11 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'HU' order by cntr_code, levl_code, nuts_id),
w12 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'IE' order by cntr_code, levl_code, nuts_id),
w13 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'IS' order by cntr_code, levl_code, nuts_id),
w14 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'IT' order by cntr_code, levl_code, nuts_id),
w15 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'NO' order by cntr_code, levl_code, nuts_id),
w16 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'PL' order by cntr_code, levl_code, nuts_id),
w17 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'RS' order by cntr_code, levl_code, nuts_id),
w18 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'SE' order by cntr_code, levl_code, nuts_id),
w19 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'SI' order by cntr_code, levl_code, nuts_id),
w20 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'SK' order by cntr_code, levl_code, nuts_id),
w21 as	(
		select * from w1 union all
		select * from w2 union all
		select * from w3 union all
		select * from w4 union all
		select * from w5 union all
		select * from w6 union all
		select * from w7 union all
		select * from w8 union all
		select * from w9 union all
		select * from w10 union all
		select * from w11 union all
		select * from w12 union all
		select * from w13 union all
		select * from w14 union all
		select * from w15 union all
		select * from w16 union all
		select * from w17 union all
		select * from w18 union all
		select * from w19 union all
		select * from w20
		)
select row_number() over(order by cntr_code, levl_code, nuts_id) as id, * from w21 order by cntr_code, levl_code, nuts_id;

ALTER TABLE csv_server_nfiesta.nuts_eurostat4aux ADD PRIMARY KEY (id);
CREATE INDEX idx_nuts_eurostat4aux_cntr_code ON csv_server_nfiesta.nuts_eurostat4aux USING btree (cntr_code);
CREATE INDEX idx_nuts_eurostat4aux_levl_code ON csv_server_nfiesta.nuts_eurostat4aux USING btree (levl_code);
alter table csv_server_nfiesta.nuts_eurostat4aux owner to adm_nfiesta;
grant select on table csv_server_nfiesta.nuts_eurostat4aux to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
drop table if exists csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
create table csv_server_nfiesta.nuts_eurostat4aux_hierarchy as
with
w1 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'AT' order by cntr_code, levl_code, nuts_id),
w2 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'BE' order by cntr_code, levl_code, nuts_id),
w3 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'CH' order by cntr_code, levl_code, nuts_id),
w4 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'CZ' order by cntr_code, levl_code, nuts_id),
w5 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'DE' order by cntr_code, levl_code, nuts_id),
w6 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'DK' order by cntr_code, levl_code, nuts_id),
w7 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'EE' order by cntr_code, levl_code, nuts_id),
w8 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'ES' order by cntr_code, levl_code, nuts_id),
w9 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'FI' order by cntr_code, levl_code, nuts_id),
w10 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'FR' order by cntr_code, levl_code, nuts_id),
w11 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'HU' order by cntr_code, levl_code, nuts_id),
w12 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'IE' order by cntr_code, levl_code, nuts_id),
w13 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'IS' order by cntr_code, levl_code, nuts_id),
w14 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'IT' order by cntr_code, levl_code, nuts_id),
w15 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'NO' order by cntr_code, levl_code, nuts_id),
w16 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'PL' order by cntr_code, levl_code, nuts_id),
w17 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'RS' order by cntr_code, levl_code, nuts_id),
w18 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'SE' order by cntr_code, levl_code, nuts_id),
w19 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'SI' order by cntr_code, levl_code, nuts_id),
w20 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_nfiesta.nuts_eurostat where cntr_code = 'SK' order by cntr_code, levl_code, nuts_id),
---------
w21 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w1 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w1 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w1 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w1 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w22 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w2 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w2 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w2 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w2 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)		
---------
,w23 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w3 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w3 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w3 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w3 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w24 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w4 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w4 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w4 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w4 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w25 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w5 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w5 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w5 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w5 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w26 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w6 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w6 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w6 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w6 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w27 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w7 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w7 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w7 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w7 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w28 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w8 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w8 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w8 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w8 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w29 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w9 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w9 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w9 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w9 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w30 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w10 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w10 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w10 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w10 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w31 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w11 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w11 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w11 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w11 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w32 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w12 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w12 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w12 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w12 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w33 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w13 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w13 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w13 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w13 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)			
---------
,w34 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w14 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w14 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w14 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w14 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w35 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w15 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w15 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w15 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w15 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w36 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w16 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w16 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w16 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w16 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w37 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w17 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w17 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w17 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w17 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w38 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w18 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w18 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w18 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w18 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w39 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w19 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w19 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w19 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w19 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w40 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w20 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w20 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w20 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w20 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
,w41 as	(
		select * from w21 union all
		select * from w22 union all
		select * from w23 union all
		select * from w24 union all
		select * from w25 union all
		select * from w26 union all
		select * from w27 union all
		select * from w28 union all
		select * from w29 union all
		select * from w30 union all
		select * from w31 union all
		select * from w32 union all
		select * from w33 union all
		select * from w34 union all
		select * from w35 union all
		select * from w36 union all
		select * from w37 union all
		select * from w38 union all
		select * from w39 union all
		select * from w40
		)
select
		row_number() over() as id,
		country_code,
		nuts3,
		nuts3_label,
		st_setsrid(ST_GeomFromEWKT(geom),3035) as geom,
		nuts2,
		nuts2_label,
		nuts1,
		nuts1_label,
		nuts0,
		nuts0_label		
from
		w41;
		
ALTER TABLE csv_server_nfiesta.nuts_eurostat4aux_hierarchy ADD PRIMARY KEY (id);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_geom ON csv_server_nfiesta.nuts_eurostat4aux_hierarchy USING gist (geom);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_nuts3 ON csv_server_nfiesta.nuts_eurostat4aux_hierarchy USING btree (nuts3);
alter table csv_server_nfiesta.nuts_eurostat4aux_hierarchy owner to adm_nfiesta;
grant select on table csv_server_nfiesta.nuts_eurostat4aux_hierarchy to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------
-- cm_plot2cell_mapping --
delete from nfiesta.cm_plot2cell_mapping where id in
(
	select id from nfiesta.cm_plot2cell_mapping where estimation_cell not in
	(select estimation_cell from nfiesta.t_stratum_in_estimation_cell)
); -- 417r
---------------------------------------------------------------------
-- f_a_cell
delete from nfiesta.f_a_cell where estimation_cell in
(
select estimation_cell from nfiesta.f_a_cell except
select estimation_cell from nfiesta.t_stratum_in_estimation_cell -- 6 943 423 r
);
---------------------------------------------------------------------
-- t_aux_total
delete from nfiesta.t_aux_total where estimation_cell in
(
select estimation_cell from nfiesta.t_aux_total except
select estimation_cell from nfiesta.t_stratum_in_estimation_cell -- 187 335 r  12 489r je estimation_cells select 187335/12489 = 15 configu
);
---------------------------------------------------------------------
-- t_estimation_cell_hierarchy
delete from nfiesta.t_estimation_cell_hierarchy where cell in
(
select cell from nfiesta.t_estimation_cell_hierarchy except
select estimation_cell from nfiesta.t_stratum_in_estimation_cell -- 6 943 423 r
);

delete from nfiesta.t_estimation_cell_hierarchy where cell_superior in
(
select cell_superior from nfiesta.t_estimation_cell_hierarchy except
select estimation_cell from nfiesta.t_stratum_in_estimation_cell -- 0 r
);
---------------------------------------------------------------------
-- t_total_estimate_conf
delete from nfiesta.t_aux_total where estimation_cell in
(
select estimation_cell from nfiesta.t_total_estimate_conf except
select estimation_cell from nfiesta.t_stratum_in_estimation_cell -- 0 r
);
---------------------------------------------------------------------
-- c_estimation_cell
ALTER TABLE nfiesta.cm_plot2cell_mapping DROP CONSTRAINT fkey__cm_plot2cell_mapping__c_estimation_cell;
ALTER TABLE nfiesta.t_stratum_in_estimation_cell DROP CONSTRAINT fkey__t_stratum_in_estimation_cell__c_estimation_cell;
ALTER TABLE nfiesta.cm_cell2param_area_mapping DROP CONSTRAINT fkey__cm_cell2param_area_mapping__c_estimation_cell;
ALTER TABLE nfiesta.t_total_estimate_conf DROP CONSTRAINT fkey__t_total_estimate_conf__c_estimation_cell;
ALTER TABLE nfiesta.f_a_cell DROP CONSTRAINT fkey__f_a_cell__c_estimation_cell;
ALTER TABLE nfiesta.t_aux_total DROP CONSTRAINT fkey__t_aux_total__c_estimation_cell;
ALTER TABLE nfiesta.t_estimation_cell_hierarchy DROP CONSTRAINT fkey__t_estimation_cell_hierarchy__c_estimation_cell;
ALTER TABLE nfiesta.t_estimation_cell_hierarchy DROP CONSTRAINT fkey__t_estimation_cell_hierarchy_sup__c_estimation_cell;

delete from nfiesta.c_estimation_cell where id in
(
select id from nfiesta.c_estimation_cell except
select estimation_cell from nfiesta.t_stratum_in_estimation_cell  -- 6 943 425 r
);

ALTER TABLE nfiesta.cm_plot2cell_mapping ADD CONSTRAINT fkey__cm_plot2cell_mapping__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES nfiesta.c_estimation_cell(id);
COMMENT ON CONSTRAINT fkey__cm_plot2cell_mapping__c_estimation_cell on nfiesta.cm_plot2cell_mapping IS 'Foreign key to table nfiesta.c_estimation_cell.';
ALTER TABLE nfiesta.t_stratum_in_estimation_cell ADD CONSTRAINT fkey__t_stratum_in_estimation_cell__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES nfiesta.c_estimation_cell(id);
COMMENT ON CONSTRAINT fkey__t_stratum_in_estimation_cell__c_estimation_cell on nfiesta.t_stratum_in_estimation_cell IS 'Foreign key to table nfiesta.c_estimation_cell.';
ALTER TABLE nfiesta.cm_cell2param_area_mapping ADD CONSTRAINT fkey__cm_cell2param_area_mapping__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES nfiesta.c_estimation_cell(id);
COMMENT ON CONSTRAINT fkey__cm_cell2param_area_mapping__c_estimation_cell on nfiesta.cm_cell2param_area_mapping IS 'Foreign key to table nfiesta.c_estimation_cell.';
ALTER TABLE nfiesta.t_total_estimate_conf ADD CONSTRAINT fkey__t_total_estimate_conf__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES nfiesta.c_estimation_cell(id);
COMMENT ON CONSTRAINT fkey__t_total_estimate_conf__c_estimation_cell on nfiesta.t_total_estimate_conf IS 'Foreign key to table nfiesta.c_estimation_cell.';
ALTER TABLE nfiesta.f_a_cell ADD CONSTRAINT fkey__f_a_cell__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES nfiesta.c_estimation_cell(id) ON UPDATE CASCADE;
COMMENT ON CONSTRAINT fkey__f_a_cell__c_estimation_cell on nfiesta.f_a_cell IS 'Foreign key to table nfiesta.c_estimation_cell.';
ALTER TABLE nfiesta.t_aux_total ADD CONSTRAINT fkey__t_aux_total__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES nfiesta.c_estimation_cell(id);
COMMENT ON CONSTRAINT fkey__t_aux_total__c_estimation_cell on nfiesta.t_aux_total IS 'Foreign key to table nfiesta.c_estimation_cell.';
ALTER TABLE nfiesta.t_estimation_cell_hierarchy ADD CONSTRAINT fkey__t_estimation_cell_hierarchy__c_estimation_cell FOREIGN KEY (cell) REFERENCES nfiesta.c_estimation_cell(id);
COMMENT ON CONSTRAINT fkey__t_estimation_cell_hierarchy__c_estimation_cell on nfiesta.t_estimation_cell_hierarchy IS 'Foreign key to table nfiesta.c_estimation_cell.';
ALTER TABLE nfiesta.t_estimation_cell_hierarchy ADD CONSTRAINT fkey__t_estimation_cell_hierarchy_sup__c_estimation_cell FOREIGN KEY (cell_superior) REFERENCES nfiesta.c_estimation_cell(id);
COMMENT ON CONSTRAINT fkey__t_estimation_cell_hierarchy_sup__c_estimation_cell on nfiesta.t_estimation_cell_hierarchy IS 'Foreign key to table nfiesta.c_estimation_cell.';

-- select count(*) from nfiesta.c_estimation_cell; -- 10 374 498 r
---------------------------------------------------------------------
---------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- select * from nfiesta.c_estimation_cell_collection order by id;
INSERT INTO nfiesta.c_estimation_cell_collection(label,description,label_en,description_en)
VALUES
('NUTS','NUTS','NUTS','NUTS'),
('NUTS0','NUTS0','NUTS0','NUTS0');
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- select * from nfiesta.c_estimation_cell where estimation_cell_collection in (15,16) order by id;
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
		(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS') as estimation_cell_collection,
		(select label from nfiesta.c_estimation_cell_collection where label = 'NUTS') as label,
		(select description from nfiesta.c_estimation_cell_collection where label = 'NUTS') as description,
		(select label_en from nfiesta.c_estimation_cell_collection where label = 'NUTS') as label_en,
		(select description_en from nfiesta.c_estimation_cell_collection where label = 'NUTS') as description_en; -- 1r
---------------------------------------		
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
		case
			when levl_code = 0 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
			when levl_code = 1 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS1')
			when levl_code = 2 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS2')
			when levl_code = 3 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3')
		end
			as estimation_cell_collection,		
		nuts_id as label,
		nuts_id || ' - ' || name_latn as description,
		nuts_id as label_en,
		nuts_id || ' - ' || name_latn as description_en
from
		csv_server_nfiesta.nuts_eurostat4aux
where
		levl_code = 0
		
and		nuts_id in	(
					'AT','BE','CH','CZ','DE','ES','FI','FR','IE','IT','NO','PL','SE','SI'
					--select label from nfiesta.c_estimation_cell
					--where id in (select estimation_cell from nfiesta.t_stratum_in_estimation_cell)
					--and estimation_cell_collection in (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0-1km-INSPIRE')
					)
order by id; -- 14r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--select * from csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
with
w1 as	(
		select nuts0 as label, st_multi(st_union(geom)) as geom
		from csv_server_nfiesta.nuts_eurostat4aux_hierarchy
		group by nuts0
		)
,w2 as	(
		select w1.label, w1.geom, t1.id as id_estimation_cell from w1
		inner join	(
					select cec.* from nfiesta.c_estimation_cell as cec
					where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
					) as t1
		on w1.label = t1.label
		)
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell; -- 14r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				fac.*,
				(select cec.id from nfiesta.c_estimation_cell as cec where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')) as estimation_cell_nuts
		from
				nfiesta.f_a_cell as fac
		where
				fac.estimation_cell in
					(
					select cec.id from nfiesta.c_estimation_cell cec
					where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
					)
		)
,w2 as	(
		select
				w1.estimation_cell_nuts as id_estimation_cell,
				st_multi(st_union(w1.geom)) as geom
		from
				w1 group by w1.estimation_cell_nuts
		)
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2; -- 1r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.f_a_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w_nuts0 as	(			
			select
					id as cell,
					(
					select id from nfiesta.c_estimation_cell where estimation_cell_collection =
						(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')
					) as cell_superior
			from
					nfiesta.c_estimation_cell
			where
					estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
			)
insert into nfiesta.t_estimation_cell_hierarchy(cell,cell_superior)
select cell, cell_superior from w_nuts0 order by cell; -- 14r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.t_estimation_cell_hierarchy;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- select count(*) from nfiesta.cm_plot2cell_mapping; => pred insertem 30 151 675 r
with
w1 as	(
		select
				cc.id as id_country,
				cc.label as country,
				-------------------------
				tss.strata_set,
				ts.stratum,
				tp.panel,
				fpp.*
		from
				(select * from sdesign.c_country where label in ('AT','BE','CH','CZ','DE','ES','FI','FR','IE','IT','NO','PL','SE','SI')) as cc
				inner join sdesign.t_strata_set as tss on cc.id = tss.country
				inner join sdesign.t_stratum as ts on tss.id = ts.strata_set
				inner join sdesign.t_panel as tp on ts.id = tp.stratum
				inner join sdesign.cm_cluster2panel_mapping as ccpm on tp.id = ccpm.panel
				inner join sdesign.t_cluster as tc on ccpm.cluster = tc.id
				inner join sdesign.f_p_plot as fpp on tc.id = fpp.cluster
		)
		--select count(*) from w1; -- 2 165 325 r
,w2 as	(
		select
				w1.country,
				w1.gid as plot,
				t1.id as estimation_cell
		from
				w1
				inner join	(
							select * from nfiesta.c_estimation_cell where estimation_cell_collection = 
							(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
							) as t1
							on w1.country = t1.label
		)
,w3 as	(
		select
				w2.plot,
				(
				select id from nfiesta.c_estimation_cell where estimation_cell_collection = 
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')
				) as estimation_cell
		from
				w2
		)
,w4 as	(
		select w3.plot, w3.estimation_cell from w3 union all
		select w2.plot, w2.estimation_cell from w2
		)
insert into nfiesta.cm_plot2cell_mapping(plot,estimation_cell)
select plot, estimation_cell from w4;
--select count(*) from w4; -- select 4330650 / 2 = 2 165 325
--select count(*) from w2; -- 2 165 325r
--select count(*) from nfiesta.cm_plot2cell_mapping; => po insertu 34 482 325 r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.cm_plot2cell_mapping;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
			c_country.label as label_country,
		    t_stratum.id as id_stratum,
		    t_stratum.stratum,
		    t_stratum.geom as geom_stratum
		from sdesign.t_stratum
		join sdesign.t_strata_set on (t_stratum.strata_set = t_strata_set.id)
		join sdesign.c_country ON c_country.id = t_strata_set.country
		where c_country.label in ('AT','BE','CH','CZ','DE','ES','FI','FR','IE','IT','NO','PL','SE','SI')
		)
,w2 as	(
		select
				w1.id_stratum,
				w1.geom_stratum,
				(select label from nfiesta.c_estimation_cell where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')) as label_country
		from
				w1
		)
,w3 as	(
		select w1.label_country, w1.id_stratum, w1.geom_stratum from w1 union all
		select w2.label_country, w2.id_stratum, w2.geom_stratum from w2
		)
,w4 as	(
		select * from nfiesta.c_estimation_cell where estimation_cell_collection in
		(select id from nfiesta.c_estimation_cell_collection where label in ('NUTS','NUTS0'))
		)
,w5 as	(
		select w3.*, w4.id as id_estimation_cell from w3 inner join w4
		on w3.label_country = w4.label
		)
-- select max(id) from nfiesta.t_stratum_in_estimation_cell; -- id od 13825513 do 13825723
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select
		w5.id_stratum as stratum,
		w5.id_estimation_cell as estimation_cell,
		w5.geom_stratum as geom,
		st_area(w5.geom_stratum) area_m2
from
		w5; -- 210r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.t_stratum_in_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--DROP FOREIGN TABLE csv_server_nfiesta.nuts_eurostat;
--drop table csv_server_nfiesta.nuts_eurostat4aux;
--drop table csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
--drop schema csv_server_nfiesta;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
