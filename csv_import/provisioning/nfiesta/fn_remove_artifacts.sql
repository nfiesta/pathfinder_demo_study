CREATE FUNCTION nfiesta.fn_remove_artifacts(_stratum_id integer, _estimation_cell_id integer, _geom public.geometry) RETURNS public.geometry
    LANGUAGE plpgsql STABLE
    AS $$
declare
		_res_geom_multi		public.geometry;
		_res_artifacts_ewkt	text;
begin
		with
		w_dump as	(
				select st_dump(_geom) as geom
				)
		,w_type as	(
				select
					(w_dump.geom).geom as geom,
					st_geometrytype((w_dump.geom).geom) as geom_type
				from
					w_dump
				)
		,w_geom_without_artifacts as	(
						select 1 as id4join, st_multi(st_union(w_type.geom)) as geom from w_type
						where w_type.geom_type = 'ST_Polygon'
						)
		,w_artifacts as		(
					select st_collect(array_agg(w_type.geom)) as geom from w_type
					where w_type.geom_type is distinct from 'ST_Polygon'
							)
		,w_artifacts_ewkt as	(
					select
						1 as id4join,
						concat(
						'select ',
						_stratum_id,' as stratum_id ,',
						_estimation_cell_id,' as estimation_cell_id ,',
						'st_geomfromewkt(''',st_asewkt(w_artifacts.geom),''') as geom union all'
					        ) as ewkt
				        from
						w_artifacts
					)
		select
			w_geom_without_artifacts.geom,
			w_artifacts_ewkt.ewkt
		from
			w_geom_without_artifacts
			inner join w_artifacts_ewkt	
		on
			w_geom_without_artifacts.id4join = w_artifacts_ewkt.id4join
		into
			_res_geom_multi,
			_res_artifacts_ewkt;

		raise notice '%',_res_artifacts_ewkt;
	
		return _res_geom_multi;
end
$$;
