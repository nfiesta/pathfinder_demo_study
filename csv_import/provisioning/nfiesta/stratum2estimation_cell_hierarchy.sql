---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------			
-- 1. 1km_inspire => are create using run of python script stratum2estimation_cell_artifacts
-----------------------------------------------------------	
-- 2. 1km_nuts3
with
w1 as	(
		select stratum, estimation_cell, geom, area_m2 from nfiesta.t_stratum_in_estimation_cell
		)
,w2 as	(
    	select * from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = '1km-INSPIRE')
		)
,w3 as	(
		select
				w1.stratum,
				w1.estimation_cell,
				w1.geom,
				w1.area_m2,
				w2.label,
				w3.id as id_1km_nuts3
		from w1
		inner join	w2 on w1.estimation_cell = w2.id
		inner join	(select * from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE')) as w3
					on w2.label = w3.label
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, id_1km_nuts3, geom, area_m2 from w3 order by stratum, id_1km_nuts3; -- 4 519 415 r
-----------------------------------------------------------		
-- 3. 1km_biogeoregion
with
w1 as	(
		select stratum, estimation_cell, geom, area_m2 from nfiesta.t_stratum_in_estimation_cell
		)
,w2 as	(
    	select * from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = '1km-INSPIRE')
		)
,w3 as	(
		select
				w1.stratum,
				w1.estimation_cell,
				w1.geom,
				w1.area_m2,
				w2.label,
				w3.id as id_1km_biogeoregion
		from w1
		inner join	w2 on w1.estimation_cell = w2.id
		inner join	(select * from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION-1km-inspire-APPROXIMATE')) as w3
					on w2.label = w3.label
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, id_1km_biogeoregion, geom, area_m2 from w3 order by stratum, id_1km_biogeoregion; -- 4 519 415 r
-----------------------------------------------------------
-- 4. INSPIRE hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = '1km-INSPIRE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_25km,
				w3.cell_superior as ec_50km,
				w4.cell_superior as ec_100km,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'INSPIRE')) as ec_inspire
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w3 on w2.cell_superior = w3.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w4 on w3.cell_superior = w4.cell
		)
,w3 as	(
		select stratum, ec_25km as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_25km	union all
		select stratum, ec_50km as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_50km	union all
		select stratum, ec_100km as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_100km	union all
		select stratum, ec_inspire as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_inspire
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 15 500 r
-----------------------------------------------------------
-- 5. NUTS hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_nuts3,
				w3.cell_superior as ec_nuts2,
				w4.cell_superior as ec_nuts1,
				w5.cell_superior as ec_nuts0,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')) as ec_nuts
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w3 on w2.cell_superior = w3.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w4 on w3.cell_superior = w4.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w5 on w4.cell_superior = w5.cell
		)
,w3 as	(
		select stratum, ec_nuts3 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts3	union all
		select stratum, ec_nuts2 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts2	union all
		select stratum, ec_nuts1 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts1	union all
		select stratum, ec_nuts0 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts0	union all
		select stratum, ec_nuts as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 3 094 r
-----------------------------------------------------------	
-- 6. BIOGEOREGIONS hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGION-1km-inspire-APPROXIMATE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_biogeoregion,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'BIOGEOREGIONS')) as ec_biogeoregions
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		)
,w3 as	(
		select stratum, ec_biogeoregion as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_biogeoregion	union all
		select stratum, ec_biogeoregions as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_biogeoregions
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 261 r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------