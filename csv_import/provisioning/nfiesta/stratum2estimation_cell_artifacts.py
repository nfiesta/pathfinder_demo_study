#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from datetime import datetime
import re
import os
import psycopg2
import psycopg2.extras
from multiprocessing import Pool

# from https://gitlab.com/nfiesta/pathfinder_demo_study/-/blob/main/csv_import/pathfinder_import_ds_add.py?ref_type=heads#L150-226

# https://www.psycopg.org/docs/connection.html#connection.notices
class pflist(list):
    def __getitem__(self, key):
        return list.__getitem__(self, key-1)

def single_query(cs, c, m, _sql, log_name, verbose=True):
    try:
        #connstr = 'host=localhost port=5433 dbname=nfi_esta user=vagrant' # for network authentication (password can be stored in ~/.pgpass)
        connstr = cs #'dbname=nfi_esta_dev' # for local linux based authentication (without password)
        conn = psycopg2.connect(connstr)
        conn.notices = pflist()
    except psycopg2.DatabaseError as e:
        print("nfiesta_parallel.single_query(), not possible to connect DB")
        print(e)
        return 1

    conn.set_session(isolation_level=None, readonly=None, deferrable=None, autocommit=True) # to not execute in transaction
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cmd = '%s' % (_sql)

    try:
        cur.execute(cmd)
    except psycopg2.Error as e:
        print("nfiesta_parallel.single_query(), SQL error:")
        print(e)
        return 2

    if cur.description != None:
        path_row_list = cur.fetchall()
        if verbose:
            print('%s\t%s/%s\t%s\t%s' % (datetime.now(), c, m, cmd, path_row_list))
        else:
            print('%s\t%s/%s' % (datetime.now(), c, m,))
    else:
        path_row_list = None
        if verbose:
            print('%s\t%s/%s\tnfiesta_parallel.py result None: %s' % (datetime.now(), c, m, path_row_list))
        else:
            print('%s\t%s/%s' % (datetime.now(), c, m,))

    if log_name != None:
        log_f = open(log_name, "a")
        [print(re.sub(r'NOTICE:', '', notice), file=log_f, flush=True) for notice in conn.notices]
        log_f.close()

    cur.close()
    conn.close()
    return path_row_list

def process_stratum(cs, processes, country, stratum, log_name, insert):
    q = '''
    with w_table_size as (
        with w_stratum as (
            select
                t_stratum.id,
                t_stratum.stratum,
                t_stratum.geom
            from sdesign.t_stratum
            join sdesign.t_strata_set on (t_stratum.strata_set = t_strata_set.id)
            join sdesign.c_country ON c_country.id = t_strata_set.country
            where c_country.label = '%(country)s' and stratum = '%(stratum)s'
        )
        , w_cells as (
            select
                row_number() over() as i,
                c_estimation_cell.id,
                c_estimation_cell.label,
                f_a_cell.geom
            from nfiesta.f_a_cell
            join nfiesta.c_estimation_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
            join nfiesta.c_estimation_cell_collection on (c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id)
            join w_stratum on (w_stratum.geom && f_a_cell.geom)
            where c_estimation_cell_collection.label = '1km-INSPIRE'
        )
        select
                min(i),
                max(i),
                count(*),
                1000 as bucket_size
        from w_cells
    )
    select
            generate_series(min, max, bucket_size) as min,
            generate_series(min+(bucket_size-1), max+(bucket_size-1), bucket_size) as max
    from w_table_size
    ;
    '''% {'country': country, 'stratum': stratum}
    res = single_query(cs, 1, 1, q, log_name=None, verbose = False)

    print('%s------------------------------parallel processing using %s processes' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S"), processes))
    if insert:
        i = '''
        , w_insert_contains as (
            select stratum_id, cell_id, geom, st_area(geom)
            from w_intersection where st_contains = true
        )
        , w_insert_multipolygon as (
            select stratum_id, cell_id, geom, st_area(geom)
            from w_intersection where st_contains = false
            and st_geometrytype = 'ST_MultiPolygon'
        )
        , w_insert as (
            select t.stratum_id, t.cell_id, t.geom, st_area(t.geom)
            from
                (
                select stratum_id, cell_id, nfiesta.fn_remove_artifacts(stratum_id, cell_id, geom) as geom
                from w_intersection where st_contains = false
                and st_geometrytype != 'ST_MultiPolygon'
                ) as t
            where t.geom is not null
        )
        insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
        select * from w_insert_contains union all
        select * from w_insert_multipolygon union all
        select * from w_insert;
        '''
    else:
        i = '''
        select stratum_id, cell_id, nfiesta.fn_remove_artifacts(stratum_id, cell_id, geom) as geom
        from w_intersection where st_contains = false and st_geometrytype != 'ST_MultiPolygon';
        '''

    q = '''
    with w_stratum as (
        select
            t_stratum.id,
            t_stratum.stratum,
            t_stratum.geom
        from sdesign.t_stratum
        join sdesign.t_strata_set on (t_stratum.strata_set = t_strata_set.id)
        join sdesign.c_country ON c_country.id = t_strata_set.country
        where c_country.label = '%(country)s' and stratum = '%(stratum)s'
    )
    , w_cells as (
        select
            row_number() over() as i,
            c_estimation_cell.id,
            c_estimation_cell.label,
            f_a_cell.geom
        from nfiesta.f_a_cell
        join nfiesta.c_estimation_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
        join nfiesta.c_estimation_cell_collection on (c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id)
        join w_stratum on (w_stratum.geom && f_a_cell.geom)
        where c_estimation_cell_collection.label = '1km-INSPIRE'
    )
    , w_intersection as (
        select
            w_stratum.id as stratum_id,
            w_stratum.stratum,
            w_cells.id as cell_id,
            w_cells.label,
            st_geometrytype(st_multi(st_intersection(w_stratum.geom, w_cells.geom))),
            st_contains(w_stratum.geom, w_cells.geom),
            st_multi(st_intersection(w_stratum.geom, w_cells.geom)) as geom
        from w_stratum, w_cells
        where w_cells.i >= %(min)s and w_cells.i <= %(max)s and st_intersects(w_stratum.geom, w_cells.geom)
    )
    ''' + i
    #print(q)

    params = [[cs, c+1, len(res), q % {'min': r['min'], 'max': r['max'], 'country': country, 'stratum': stratum}, log_name, False] for c, r in enumerate(res)]
    with Pool(processes) as p:
        res_multi = p.starmap(single_query, params, chunksize=1)
    print(len(res_multi))

def process_all(cs, processes, filter_country, filter_stratum, log_name, insert):
    print('%s------------------------------getting artifacts' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")))

    if filter_country != 'ALL':
        csl_f = '''where c_country.label = '%s' ''' % (filter_country)
        if filter_stratum != 'ALL':
            csl_f += '''and t_stratum.stratum = '%s' ''' % (filter_stratum)
    else:
        csl_f = ''

    csl = '''
     select
            c_country.label as country, t_stratum.stratum
    from sdesign.c_country
    join sdesign.t_strata_set on (c_country.id = t_strata_set.country)
    join sdesign.t_stratum on (t_strata_set.id = t_stratum.strata_set)
    %s
    order by 1, 2
    ;
    ''' % (csl_f)

    res = single_query(cs, 1, 1, csl, log_name=None, verbose=True)

    for c, r in enumerate(res):
        print('%s------------------------------processing country %s, stratum %s' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S"), r['country'], r['stratum']))
        process_stratum(cs, processes = processes, country=r['country'], stratum=r['stratum'], log_name=log_name, insert=insert)

def main():
    parser = argparse.ArgumentParser(description="Utility for creating intersections.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='''\
Example:
        python3 stratum2estimation_cell_artifacts.py --connectionstring='dbname=MyPathfinderDB' --country=CZ --processes=60 --insert
            ''')
    parser.add_argument('-cs', '--connectionstring', required=True, help='''libpq connection string [SOURCE DB]: https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING.
            For password use ~/.pgpass file.
            Examples:
            -cs='host=localhost dbname=postgres user=vagrant'
            --connectionstring 'host=localhost dbname=postgres user=vagrant'
            -cs='postgresql://vagrant@localhost/postgres'
            ''')
    parser.add_argument('-c', '--country', required=False, help='Country to process. If not used, all countries will be processed.')
    parser.add_argument('-s', '--stratum', required=False, help='Stratum to process. If not used, all strata will be processed. If used without --country, ignored.')
    parser.add_argument('-p', '--processes', type=int, help='Number of processes (CPUs) to use.')
    parser.add_argument('-i', '--insert', action='store_true', help='Insert intersections. If not used, intersections are only checked.')
    parser.set_defaults(processes=1)
    parser.set_defaults(insert=False)
    parser.set_defaults(country='ALL')
    parser.set_defaults(stratum='ALL')
    args = parser.parse_args()
    
    log_name='list_of_artifacts.sql'
    if os.path.isfile(log_name):
        os.remove(log_name)
    process_all(args.connectionstring, args.processes, args.country, args.stratum, log_name=log_name, insert=args.insert)

if __name__ == '__main__':
    main()
