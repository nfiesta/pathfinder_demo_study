---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------			
-- 1. 1km_inspire => are create using run of python script stratum2estimation_cell_artifacts
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- 2. 1km_nuts3
with
w1 as	(
		select stratum, estimation_cell, geom, area_m2 from nfiesta.t_stratum_in_estimation_cell
		)
,w2 as	(
    	select * from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = '1km-INSPIRE')
		)
,w3 as	(
		select
				w1.stratum,
				w1.estimation_cell,
				w1.geom,
				w1.area_m2,
				w2.label,
				w3.id as id_1km_nuts3
		from w1
		inner join	w2 on w1.estimation_cell = w2.id
		inner join	(select * from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE')) as w3
					on w2.label = w3.label
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, id_1km_nuts3, geom, area_m2 from w3 order by stratum, id_1km_nuts3; -- 3 185 541 r
-----------------------------------------------------------
-----------------------------------------------------------
analyze nfiesta.t_stratum_in_estimation_cell;
-----------------------------------------------------------
-----------------------------------------------------------
-- 3. NUTS3 hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_nuts3,
				w3.cell_superior as ec_nuts2,
				w4.cell_superior as ec_nuts1,
				w5.cell_superior as ec_nuts0,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS-1km-INSPIRE')) as ec_nuts
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w3 on w2.cell_superior = w3.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w4 on w3.cell_superior = w4.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w5 on w4.cell_superior = w5.cell
		)
,w3 as	(
		select stratum, ec_nuts3 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts3	-- 1382r
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 1382r
-----------------------------------------------------------
-- 4. NUTS2 hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_nuts3,
				w3.cell_superior as ec_nuts2,
				w4.cell_superior as ec_nuts1,
				w5.cell_superior as ec_nuts0,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS-1km-INSPIRE')) as ec_nuts
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w3 on w2.cell_superior = w3.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w4 on w3.cell_superior = w4.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w5 on w4.cell_superior = w5.cell
		)
,w3 as	(
		select stratum, ec_nuts2 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts2	-- 379r
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 397r
-----------------------------------------------------------
-- 5. NUTS1 hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_nuts3,
				w3.cell_superior as ec_nuts2,
				w4.cell_superior as ec_nuts1,
				w5.cell_superior as ec_nuts0,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS-1km-INSPIRE')) as ec_nuts
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w3 on w2.cell_superior = w3.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w4 on w3.cell_superior = w4.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w5 on w4.cell_superior = w5.cell
		)
,w3 as	(
		select stratum, ec_nuts1 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts1	-- 194r
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 194r
-----------------------------------------------------------
-- 6. NUTS0 hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_nuts3,
				w3.cell_superior as ec_nuts2,
				w4.cell_superior as ec_nuts1,
				w5.cell_superior as ec_nuts0,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS-1km-INSPIRE')) as ec_nuts
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w3 on w2.cell_superior = w3.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w4 on w3.cell_superior = w4.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w5 on w4.cell_superior = w5.cell
		)
,w3 as	(
		select stratum, ec_nuts0 as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts0	--  94r
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 94r
-----------------------------------------------------------
-- 7. NUTS hiearachy
with
w1 as	(
		select * from nfiesta.t_stratum_in_estimation_cell where estimation_cell in
    	(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE'))
		)
,w2 as	(
		select
				w1.id,
				w1.stratum,
				w1.estimation_cell as ec_1km,
				w1.geom,
				w2.cell_superior as ec_nuts3,
				w3.cell_superior as ec_nuts2,
				w4.cell_superior as ec_nuts1,
				w5.cell_superior as ec_nuts0,
				(select id from nfiesta.c_estimation_cell where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS-1km-INSPIRE')) as ec_nuts
		from w1
		inner join nfiesta.t_estimation_cell_hierarchy as w2 on w1.estimation_cell = w2.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w3 on w2.cell_superior = w3.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w4 on w3.cell_superior = w4.cell
		inner join nfiesta.t_estimation_cell_hierarchy as w5 on w4.cell_superior = w5.cell
		)
,w3 as	(
		select stratum, ec_nuts as estimation_cell, st_multi(st_union(geom)) as geom from w2 group by stratum, ec_nuts	--  48r
		)
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select stratum, estimation_cell, geom, st_area(geom) as area_m2 from w3 order by stratum, estimation_cell; -- 48r
-----------------------------------------------------------
analyze nfiesta.c_estimation_cell;
analyze nfiesta.cm_plot2cell_mapping;
analyze nfiesta.f_a_cell;
analyze nfiesta.t_estimation_cell_hierarchy;
analyze nfiesta.t_stratum_in_estimation_cell;
-----------------------------------------------------------
-----------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
