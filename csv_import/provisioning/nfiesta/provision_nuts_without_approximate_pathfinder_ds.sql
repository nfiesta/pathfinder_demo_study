---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- select * from nfiesta.c_estimation_cell_collection order by id;
INSERT INTO nfiesta.c_estimation_cell_collection(label,description,label_en,description_en)
VALUES
('NUTS','NUTS','NUTS','NUTS'),
('NUTS0','NUTS0','NUTS0','NUTS0');
---------------------------------------------------------------------------------------------------
-- select * from nfiesta.c_estimation_cell where estimation_cell_collection in (8,9) order by id;
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
		(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS') as estimation_cell_collection,
		(select label from nfiesta.c_estimation_cell_collection where label = 'NUTS') as label,
		(select description from nfiesta.c_estimation_cell_collection where label = 'NUTS') as description,
		(select label_en from nfiesta.c_estimation_cell_collection where label = 'NUTS') as label_en,
		(select description_en from nfiesta.c_estimation_cell_collection where label = 'NUTS') as description_en; -- 1r
---------------------------------------		
insert into nfiesta.c_estimation_cell(estimation_cell_collection,label,description,label_en,description_en)
select
		case
			when levl_code = 0 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
			when levl_code = 1 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS1')
			when levl_code = 2 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS2')
			when levl_code = 3 then (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS3')
		end
			as estimation_cell_collection,		
		nuts_id as label,
		nuts_id || ' - ' || name_latn as description,
		nuts_id as label_en,
		nuts_id || ' - ' || name_latn as description_en
from
		csv_server_nfiesta.nuts_eurostat4aux
where
		levl_code = 0
		
and		nuts_id in ('AT','BE','CZ','DE','ES','FI','FR','CH','IE','IS','IT','NO','PL','SE','SI')
order
		by id; -- 15r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--select * from csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
with
w1 as	(
		select nuts0 as label, st_multi(st_union(geom)) as geom
		from csv_server_nfiesta.nuts_eurostat4aux_hierarchy
		group by nuts0
		)
,w2 as	(
		select w1.label, w1.geom, t1.id as id_estimation_cell from w1
		inner join	(
					select cec.* from nfiesta.c_estimation_cell as cec
					where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
					) as t1
		on w1.label = t1.label
		)
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell; -- 15r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				fac.*,
				(select cec.id from nfiesta.c_estimation_cell as cec where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')) as estimation_cell_nuts
		from
				nfiesta.f_a_cell as fac
		where
				fac.estimation_cell in
					(
					select cec.id from nfiesta.c_estimation_cell cec
					where estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
					)
		)
,w2 as	(
		select
				w1.estimation_cell_nuts as id_estimation_cell,
				st_multi(st_union(w1.geom)) as geom
		from
				w1 group by w1.estimation_cell_nuts
		)
insert into nfiesta.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2; -- 1r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.c_estimation_cell;
analyze nfiesta.f_a_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w_nuts0 as	(			
			select
					id as cell,
					(
					select id from nfiesta.c_estimation_cell where estimation_cell_collection =
						(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')
					) as cell_superior
			from
					nfiesta.c_estimation_cell
			where
					estimation_cell_collection = (select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
			)
insert into nfiesta.t_estimation_cell_hierarchy(cell,cell_superior)
select cell, cell_superior from w_nuts0 order by cell; -- 15r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.t_estimation_cell_hierarchy;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
-- select count(*) from nfiesta.cm_plot2cell_mapping; => pred insertem 3 100 382 r
with
w1 as	(
		select
				cc.id as id_country,
				cc.label as country,
				-------------------------
				tss.strata_set,
				ts.stratum,
				tp.panel,
				fpp.*
		from
				(select * from sdesign.c_country where label in ('AT','BE','CZ','DE','ES','FI','FR','CH','IE','IS','IT','NO','PL','SE','SI')) as cc
				inner join sdesign.t_strata_set as tss on cc.id = tss.country
				inner join sdesign.t_stratum as ts on tss.id = ts.strata_set
				inner join sdesign.t_panel as tp on ts.id = tp.stratum
				inner join sdesign.cm_cluster2panel_mapping as ccpm on tp.id = ccpm.panel
				inner join sdesign.t_cluster as tc on ccpm.cluster = tc.id
				inner join sdesign.f_p_plot as fpp on tc.id = fpp.cluster
		)
		--select count(*) from w1; -- 451 259 r
,w2 as	(
		select
				w1.country,
				w1.gid as plot,
				t1.id as estimation_cell
		from
				w1
				inner join	(
							select * from nfiesta.c_estimation_cell where estimation_cell_collection = 
							(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS0')
							) as t1
							on w1.country = t1.label
		)
,w3 as	(
		select
				w2.plot,
				(
				select id from nfiesta.c_estimation_cell where estimation_cell_collection = 
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')
				) as estimation_cell
		from
				w2
		)
,w4 as	(
		select w3.plot, w3.estimation_cell from w3 union all
		select w2.plot, w2.estimation_cell from w2
		)
insert into nfiesta.cm_plot2cell_mapping(plot,estimation_cell)
select plot, estimation_cell from w4;
--select count(*) from w4; -- select 902518 / 2 = 451259
--select count(*) from w2; -- 451259r
--select count(*) from nfiesta.cm_plot2cell_mapping; => po insertu 4 002 900 r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.cm_plot2cell_mapping;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
			c_country.label as label_country,
		    t_stratum.id as id_stratum,
		    t_stratum.stratum,
		    t_stratum.geom as geom_stratum
		from sdesign.t_stratum
		join sdesign.t_strata_set on (t_stratum.strata_set = t_strata_set.id)
		join sdesign.c_country ON c_country.id = t_strata_set.country
		where c_country.label in ('AT','BE','CZ','DE','ES','FI','FR','CH','IE','IS','IT','NO','PL','SE','SI')
		)
,w2 as	(
		select
				w1.id_stratum,
				w1.geom_stratum,
				(select label from nfiesta.c_estimation_cell where estimation_cell_collection =
				(select id from nfiesta.c_estimation_cell_collection where label = 'NUTS')) as label_country
		from
				w1
		)
,w3 as	(
		select w1.label_country, w1.id_stratum, w1.geom_stratum from w1 union all
		select w2.label_country, w2.id_stratum, w2.geom_stratum from w2
		)
,w4 as	(
		select * from nfiesta.c_estimation_cell where estimation_cell_collection in
		(select id from nfiesta.c_estimation_cell_collection where label in ('NUTS','NUTS0'))
		)
,w5 as	(
		select w3.*, w4.id as id_estimation_cell from w3 inner join w4
		on w3.label_country = w4.label
		)
--select max(id) from nfiesta.t_stratum_in_estimation_cell; -- id od 6588600 do 6588696
insert into nfiesta.t_stratum_in_estimation_cell(stratum,estimation_cell,geom,area_m2)
select
		w5.id_stratum as stratum,
		w5.id_estimation_cell as estimation_cell,
		w5.geom_stratum as geom,
		st_area(w5.geom_stratum) area_m2
from
		w5; -- 96r
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze nfiesta.t_stratum_in_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--DROP FOREIGN TABLE csv_server_nfiesta.mask;
--DROP FOREIGN TABLE csv_server_nfiesta.nuts_eurostat;
--drop table csv_server_nfiesta.nuts_eurostat4aux;
--drop table csv_server_nfiesta.nuts_eurostat4aux_hierarchy;
--drop table csv_server_nfiesta.nuts_eurostat4aux_hierarchy_intersect;
--drop schema csv_server_nfiesta;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------