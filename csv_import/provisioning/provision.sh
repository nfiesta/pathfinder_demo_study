sudo service postgresql restart

# Loop until PostgreSQL is ready
until (pg_isready -U postgres -d postgres > /dev/null 2>&1) do
echo "PostgreSQL is not ready yet. Waiting..."
sleep 2
done

if [ ! -f /home/vagrant/.provisioned ]; then

    #readonly DATA=/usr/home/data/file.dat
    echo "==========$(date)==========start of provisioning==============="

    # turn off JIT
    sudo sed -i "s/#jit = on/jit = off/" /etc/postgresql/16/main/postgresql.conf
    sudo sed -i "s/#max_locks_per_transaction = 64/max_locks_per_transaction = 256/" /etc/postgresql/16/main/postgresql.conf
    sudo sed -i "s/#from_collapse_limit = 8/from_collapse_limit = 80/" /etc/postgresql/16/main/postgresql.conf
    sudo sed -i "s/#join_collapse_limit = 8/join_collapse_limit = 80/" /etc/postgresql/16/main/postgresql.conf
    sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/16/main/postgresql.conf
    sudo sed -i "s/all             127.0.0.1\/32/vagrant         all/" /etc/postgresql/16/main/pg_hba.conf
    # start postgres
    sudo service postgresql restart

    # Loop until PostgreSQL is ready
    until (pg_isready -U postgres -d postgres > /dev/null 2>&1) do
    echo "PostgreSQL is not ready yet. Waiting..."
    sleep 2
    done

    # create DB user (preferably with same name as Linux user to bypass password provision)
    sudo -u postgres psql -c "CREATE USER vagrant SUPERUSER;"
    sudo -u postgres psql -c "ALTER USER vagrant WITH PASSWORD 'vagrant';"
    psql -d postgres -c "CREATE ROLE adm_nfiesta; CREATE ROLE app_nfiesta; CREATE ROLE app_nfiesta_mng; GRANT app_nfiesta TO app_nfiesta_mng;"
    psql -d postgres -c "GRANT app_nfiesta_mng TO vagrant;"

    # git
    cd ~
    git clone https://gitlab.com/nfiesta/nfiesta_pg.git
    cd nfiesta_pg
    git submodule sync
    git submodule update --init --progress
    # build install extensions
    cd deps/nfiesta_htc
    make
    sudo make install
    # run automated tests
    cd ..
    cd nfiesta_sdesign
    sudo make install
    cd ../..
    sudo make install

    cd ~/pathfinder_demo_study/csv_import
    echo "==========$(date)==========setting extensions=================="
    psql -d postgres -f /home/vagrant/pathfinder_demo_study/csv_import/provisioning/provision_ext.sql
    echo "==========$(date)==========setting FDW========================="
    psql -d pathfinder_ds -f /home/vagrant/pathfinder_demo_study/csv_import/server_side/provision_csvfdw.sql
    echo "==========$(date)==========inserting cells====================="

    # Get the list of files
    NFIESTA_COUNTRY_LONG=$(grep ${NFIESTA_COUNTRY^^} server_side/countries.csv | cut -d ';' -f3)
    files=/home/vagrant/pathfinder_demo_study/csv_import/countries/$NFIESTA_COUNTRY_LONG/strata.csv

    # Convert the list of files to an array
    IFS=$'\n' read -rd '' -a file_array <<<"$files"
    #file_array+=('/home/vagrant/pathfinder_demo_study/csv_import/server_side/EU_mask.csv')

    # If only one file is found, use it
    if [[ ${#file_array[@]} -eq 1 ]]; then
        selected_file="${file_array[0]}"
        echo "Selected file will be used as extent for generating INSPIRE grid: $selected_file"
        export CSF=$selected_file
    else
        # Multiple files found, prompt user to select one
        echo "Multiple files found:"
        for i in "${!file_array[@]}"; do
            echo "$i) ${file_array[$i]}"
        done

        echo -n "Select the file number to use: "
        read -r file_index

        # Validate the input
        if [[ ! "$file_index" =~ ^[0-9]+$ ]] || ((file_index < 0 || file_index >= ${#file_array[@]})); then
            echo "Invalid selection. Exiting."
            exit 1
        fi

        selected_file="${file_array[$file_index]}"
        echo "Selected file: $selected_file"
        export CSF=$selected_file
        echo "country strata will be used as extent for generating INSPIRE grid"
        echo $CSF
    fi

    psql -d pathfinder_ds -f /home/vagrant/pathfinder_demo_study/csv_import/provisioning/provision_cells.sql
    echo "==========$(date)==========inserting variable=================="
    psql -d pathfinder_ds -f /home/vagrant/pathfinder_demo_study/csv_import/provisioning/provision_variables.sql
    echo "==========$(date)==========inserting fkeys codelists==========="
    psql -d pathfinder_ds -f /home/vagrant/pathfinder_demo_study/csv_import/provisioning/provision_fk_check_codelist.sql

    touch /home/vagrant/.provisioned
fi