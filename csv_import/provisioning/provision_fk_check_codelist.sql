set role adm_nfiesta;
drop schema if exists csv_fk_check_codelist cascade;create schema csv_fk_check_codelist;

create table csv_fk_check_codelist.cell_collection_cells 	as  
select 
c_estimation_cell_collection.label as cell_collection, 
c_estimation_cell.label as cell 
from nfiesta.c_estimation_cell 
join nfiesta.c_estimation_cell_collection on (c_estimation_cell.estimation_cell_collection = c_estimation_cell_collection.id);

alter table csv_fk_check_codelist.cell_collection_cells		add constraint cell_collection_cells_cell_collection_cell_key unique (cell_collection, cell);

create table csv_fk_check_codelist.variable_target as
with w_data as (
    select
        --t_variable.*,
        coalesce(metadata->'en'->>'pf_label', 'altogether') as target_variable,
        coalesce(c_sub_population.label, 'altogether') as sub_population,
        coalesce(c_sub_population_category.label, 'altogether') as sub_population_category,
        coalesce(c_area_domain.label, 'altogether') as area_domain,
        coalesce(c_area_domain_category.label, 'altogether') as area_domain_category
    from nfiesta.t_variable
    join nfiesta.c_target_variable on (t_variable.target_variable = c_target_variable.id)
    left join nfiesta.c_sub_population_category ON c_sub_population_category.id = t_variable.sub_population_category
    left join nfiesta.c_sub_population on c_sub_population.id = c_sub_population_category.sub_population
    left join nfiesta.c_area_domain_category ON c_area_domain_category.id = t_variable.area_domain_category
    left join nfiesta.c_area_domain on c_area_domain.id = c_area_domain_category.area_domain
)
select
    target_variable,
    array_to_string(array(select unnest(string_to_array(sub_population, '+')) order by 1), '+') as sub_population,
    array_to_string(array(select unnest(string_to_array(sub_population_category, '+')) order by 1), '+') as sub_population_category,
    array_to_string(array(select unnest(string_to_array(area_domain, '+')) order by 1), '+') as area_domain,
    array_to_string(array(select unnest(string_to_array(area_domain_category, '+')) order by 1), '+') as area_domain_category
from w_data
;

alter table csv_fk_check_codelist.variable_target			add constraint variablet_target_tv_sp_spc_ad_adc_key unique (target_variable, sub_population, sub_population_category, area_domain, area_domain_category);

create table csv_fk_check_codelist.variable_aux as
select
    --t_variable.*,
    coalesce(c_auxiliary_variable.label, 'altogether') as auxiliary_variable,
    coalesce(c_auxiliary_variable_category.label, 'altogether') as auxiliary_variable_category
from nfiesta.t_variable
join nfiesta.c_auxiliary_variable_category ON (c_auxiliary_variable_category.id = t_variable.auxiliary_variable_category)
join nfiesta.c_auxiliary_variable on (c_auxiliary_variable_category.auxiliary_variable = c_auxiliary_variable.id)
;

alter table csv_fk_check_codelist.variable_aux			add constraint variable_aux_av_avc_key unique (auxiliary_variable, auxiliary_variable_category);
