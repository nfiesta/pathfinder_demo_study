
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
/*
\set afile '/home/vagrant/pathfinder_demo_study/csv_import/server_side/EU_mask.csv'
CREATE FOREIGN TABLE csv_server_gisdata.mask (
        country                 character varying(20)           not null,
        strata_set              character varying(20)           not null,
        stratum                 character varying(20)           not null,
        label                   character varying(120)          not null,
        geometry                text                            not null,
        area_ha                 double precision,
        comment                 text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename:'afile' );
*/
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
/*
drop table if exists csv_server_gisdata.inspire_grid_1km;
create table csv_server_gisdata.inspire_grid_1km as
with w_mask as (
	select
		1000 as scalex, -1000 as  scaley,
		'1km' as label,
		(select st_buffer(st_union(st_geomfromewkt(geometry)), 0) from csv_server_gisdata.mask) as mask
)
, w_cover as (
	select
		scalex, scaley, label,
		mask,
		--st_envelope(mask),
		ST_MakeEnvelope(
			floor(ST_XMin(mask)/scalex)*scalex, 
			floor(ST_YMin(mask)/abs(scaley))*abs(scaley), 
			ceil(ST_XMax(mask)/scalex)*scalex, 
			ceil(ST_YMax(mask)/abs(scaley))*abs(scaley),
			3035) as extent
	from w_mask
)		
, w_grid as (
	SELECT 
		(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
		((ST_Xmax(w_cover.extent) - ST_Xmin(w_cover.extent)) / scalex)::int, ((ST_Ymax(w_cover.extent) - ST_Ymin(w_cover.extent)) / abs(scaley))::int,
		ST_Xmin(w_cover.extent),
		ST_Ymax(w_cover.extent),
		scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
	from w_cover
)
select
	row_number() over() as gid,
	format('%s-INSPIRE', w_cover.label) as cell_collection,
	concat(w_cover.label, 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
		w_cover.label, (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
	geom
from w_grid, w_cover
where st_intersects(w_grid.geom, w_cover.mask);

ALTER TABLE csv_server_gisdata.inspire_grid_1km ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_1km_geom ON csv_server_gisdata.inspire_grid_1km USING gist (geom);
CREATE INDEX idx_inspire_grid_1km_gid ON csv_server_gisdata.inspire_grid_1km USING btree (gid);
alter table csv_server_gisdata.inspire_grid_1km owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.inspire_grid_1km to public;
*/
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_gisdata.inspire_grid_25km as
with w_mask as (
	select
		25000 as scalex, -25000 as  scaley,
		'25km' as label,
		(select st_buffer(st_union(st_geomfromewkt(geometry)), 0) from csv_server_gisdata.mask) as mask
)
, w_cover as (
	select
		scalex, scaley, label,
		mask,
		--st_envelope(mask),
		ST_MakeEnvelope(
			floor(ST_XMin(mask)/scalex)*scalex, 
			floor(ST_YMin(mask)/abs(scaley))*abs(scaley), 
			ceil(ST_XMax(mask)/scalex)*scalex, 
			ceil(ST_YMax(mask)/abs(scaley))*abs(scaley),
			3035) as extent
	from w_mask
)		
, w_grid as (
	SELECT 
		(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
		((ST_Xmax(w_cover.extent) - ST_Xmin(w_cover.extent)) / scalex)::int, ((ST_Ymax(w_cover.extent) - ST_Ymin(w_cover.extent)) / abs(scaley))::int,
		ST_Xmin(w_cover.extent),
		ST_Ymax(w_cover.extent),
		scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
	from w_cover
)
select
	row_number() over() as gid,
	format('%s-INSPIRE', w_cover.label) as cell_collection,
	concat(w_cover.label, 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
		w_cover.label, (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
	geom
from w_grid, w_cover
where st_intersects(w_grid.geom, w_cover.mask);

ALTER TABLE csv_server_gisdata.inspire_grid_25km ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_25km_geom ON csv_server_gisdata.inspire_grid_25km USING gist (geom);
CREATE INDEX idx_inspire_grid_25km_gid ON csv_server_gisdata.inspire_grid_25km USING btree (gid);
alter table csv_server_gisdata.inspire_grid_25km owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.inspire_grid_25km to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_gisdata.inspire_grid_50km as
with w_mask as (
	select
		50000 as scalex, -50000 as  scaley,
		'50km' as label,
		(select st_buffer(st_union(st_geomfromewkt(geometry)), 0) from csv_server_gisdata.mask) as mask
)
, w_cover as (
	select
		scalex, scaley, label,
		mask,
		--st_envelope(mask),
		ST_MakeEnvelope(
			floor(ST_XMin(mask)/scalex)*scalex, 
			floor(ST_YMin(mask)/abs(scaley))*abs(scaley), 
			ceil(ST_XMax(mask)/scalex)*scalex, 
			ceil(ST_YMax(mask)/abs(scaley))*abs(scaley),
			3035) as extent
	from w_mask
)		
, w_grid as (
	SELECT 
		(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
		((ST_Xmax(w_cover.extent) - ST_Xmin(w_cover.extent)) / scalex)::int, ((ST_Ymax(w_cover.extent) - ST_Ymin(w_cover.extent)) / abs(scaley))::int,
		ST_Xmin(w_cover.extent),
		ST_Ymax(w_cover.extent),
		scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
	from w_cover
)
select
	row_number() over() as gid,
	format('%s-INSPIRE', w_cover.label) as cell_collection,
	concat(w_cover.label, 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
		w_cover.label, (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
	geom
from w_grid, w_cover
where st_intersects(w_grid.geom, w_cover.mask);

ALTER TABLE csv_server_gisdata.inspire_grid_50km ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_50km_geom ON csv_server_gisdata.inspire_grid_50km USING gist (geom);
CREATE INDEX idx_inspire_grid_50km_gid ON csv_server_gisdata.inspire_grid_50km USING btree (gid);
alter table csv_server_gisdata.inspire_grid_50km owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.inspire_grid_50km to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_gisdata.inspire_grid_100km as
with w_mask as (
	select
		100000 as scalex, -100000 as  scaley,
		'100km' as label,
		(select st_buffer(st_union(st_geomfromewkt(geometry)), 0) from csv_server_gisdata.mask) as mask
)
, w_cover as (
	select
		scalex, scaley, label,
		mask,
		--st_envelope(mask),
		ST_MakeEnvelope(
			floor(ST_XMin(mask)/scalex)*scalex, 
			floor(ST_YMin(mask)/abs(scaley))*abs(scaley), 
			ceil(ST_XMax(mask)/scalex)*scalex, 
			ceil(ST_YMax(mask)/abs(scaley))*abs(scaley),
			3035) as extent
	from w_mask
)		
, w_grid as (
	SELECT 
		(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
		((ST_Xmax(w_cover.extent) - ST_Xmin(w_cover.extent)) / scalex)::int, ((ST_Ymax(w_cover.extent) - ST_Ymin(w_cover.extent)) / abs(scaley))::int,
		ST_Xmin(w_cover.extent),
		ST_Ymax(w_cover.extent),
		scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
	from w_cover
)
select
	row_number() over() as gid,
	format('%s-INSPIRE', w_cover.label) as cell_collection,
	concat(w_cover.label, 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
		w_cover.label, (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
	geom
from w_grid, w_cover
where st_intersects(w_grid.geom, w_cover.mask);

ALTER TABLE csv_server_gisdata.inspire_grid_100km ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_100km_geom ON csv_server_gisdata.inspire_grid_100km USING gist (geom);
CREATE INDEX idx_inspire_grid_100km_gid ON csv_server_gisdata.inspire_grid_100km USING btree (gid);
alter table csv_server_gisdata.inspire_grid_100km owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.inspire_grid_100km to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_gisdata.inspire_grid_up as
select
		1 as gid,
		'INSPIRE' as cell_collection,
		'INSPIRE' as label,
		st_union(geom) as geom
from
		csv_server_gisdata.inspire_grid_100km;
	
ALTER TABLE csv_server_gisdata.inspire_grid_up ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_up_gid   ON csv_server_gisdata.inspire_grid_up USING btree (gid);
CREATE INDEX idx_inspire_grid_up_label ON csv_server_gisdata.inspire_grid_up USING btree (label);
CREATE INDEX gidx_inspire_grid_up_geom ON csv_server_gisdata.inspire_grid_up USING gist (geom);
alter table csv_server_gisdata.inspire_grid_up owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.inspire_grid_up to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en)
VALUES
(7,	'INSPIRE','INSPIRE','INSPIRE','INSPIRE.'),
(8,	'100km-INSPIRE','100km-INSPIRE','100km-INSPIRE','100km-INSPIRE'),
(9,	'50km-INSPIRE','50km-INSPIRE','50km-INSPIRE','50km-INSPIRE'),
(10,'25km-INSPIRE','25km-INSPIRE','25km-INSPIRE','25km-INSPIRE'),
(11,'1km-INSPIRE','1km-INSPIRE','1km-INSPIRE','1km-INSPIRE');
update gisdata.c_estimation_cell_collection set use4estimates = false where label = '1km-INSPIRE';
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(7,7,11,7), -- inspire
(8,8,11,7), -- inspire100
(9,9,11,7), -- inspire50
(10,10,11,7), -- inspire25
(11,11,11,7); -- inspire1
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with 
w1 as	(
		select
				gid + (select max(id) from gisdata.c_estimation_cell) as id,
				(select id from gisdata.c_estimation_cell_collection where label = cell_collection) as estimation_cell_collection,
				label,
				label as description,
				label as label_en,
				label as description_en
		from
				csv_server_gisdata.inspire_grid_1km order by gid
		)
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select id,estimation_cell_collection,label,description,label_en,description_en from w1 order by id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with 
w1 as	(
		select
				gid + (select max(id) from gisdata.c_estimation_cell) as id,
				(select id from gisdata.c_estimation_cell_collection where label = cell_collection) as estimation_cell_collection,
				label,
				label as description,
				label as label_en,
				label as description_en
		from
				csv_server_gisdata.inspire_grid_25km order by gid
		)
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select id,estimation_cell_collection,label,description,label_en,description_en from w1 order by id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with 
w1 as	(
		select
				gid + (select max(id) from gisdata.c_estimation_cell) as id,
				(select id from gisdata.c_estimation_cell_collection where label = cell_collection) as estimation_cell_collection,
				label,
				label as description,
				label as label_en,
				label as description_en
		from
				csv_server_gisdata.inspire_grid_50km order by gid
		)
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select id,estimation_cell_collection,label,description,label_en,description_en from w1 order by id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with 
w1 as	(
		select
				gid + (select max(id) from gisdata.c_estimation_cell) as id,
				(select id from gisdata.c_estimation_cell_collection where label = cell_collection) as estimation_cell_collection,
				label,
				label as description,
				label as label_en,
				label as description_en
		from
				csv_server_gisdata.inspire_grid_100km order by gid
		)
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select id,estimation_cell_collection,label,description,label_en,description_en from w1 order by id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with 
w1 as	(
		select
				gid + (select max(id) from gisdata.c_estimation_cell) as id,
				(select id from gisdata.c_estimation_cell_collection where label = cell_collection) as estimation_cell_collection,
				label,
				label as description,
				label as label_en,
				label as description_en
		from
				csv_server_gisdata.inspire_grid_up order by gid
		)
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select id,estimation_cell_collection,label,description,label_en,description_en from w1 order by id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select t1.label, st_multi(geom) as geom, t2.id as id_estimation_cell from
		(select label, geom from csv_server_gisdata.inspire_grid_1km) as t1
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = '1km-INSPIRE')
					) as t2
		on t1.label = t2.label
		)
insert into gisdata.f_a_cell(geom, estimation_cell)
select geom, id_estimation_cell from w1 order by id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select t1.label, st_multi(geom) as geom, t2.id as id_estimation_cell from
		(select label, geom from csv_server_gisdata.inspire_grid_25km) as t1
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = '25km-INSPIRE')
					) as t2
		on t1.label = t2.label
		)
insert into gisdata.f_a_cell(geom, estimation_cell)
select geom, id_estimation_cell from w1 order by id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select t1.label, st_multi(geom) as geom, t2.id as id_estimation_cell from
		(select label, geom from csv_server_gisdata.inspire_grid_50km) as t1
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = '50km-INSPIRE')
					) as t2
		on t1.label = t2.label
		)
insert into gisdata.f_a_cell(geom, estimation_cell)
select geom, id_estimation_cell from w1 order by id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select t1.label, st_multi(geom) as geom, t2.id as id_estimation_cell from
		(select label, geom from csv_server_gisdata.inspire_grid_100km) as t1
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = '100km-INSPIRE')
					) as t2
		on t1.label = t2.label
		)
insert into gisdata.f_a_cell(geom, estimation_cell)
select geom, id_estimation_cell from w1 order by id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select t1.label, st_multi(geom) as geom, t2.id as id_estimation_cell from
		(select label, geom from csv_server_gisdata.inspire_grid_up) as t1
		inner join	(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'INSPIRE')
					) as t2
		on t1.label = t2.label
		)
insert into gisdata.f_a_cell(geom, estimation_cell)
select geom, id_estimation_cell from w1 order by id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze gisdata.f_a_cell;
analyze gisdata.c_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				w1.label as label,
				w2.label as label_sup
		from csv_server_gisdata.inspire_grid_1km as w1, csv_server_gisdata.inspire_grid_25km as w2
		where w1.geom && w2.geom
		and st_intersects(st_centroid(w1.geom),w2.geom)
		)
,w2 as	(
		select
				w1.label,
				w1.label_sup,
				t3.gid as cell,
				t4.gid as cell_sup
		from
				w1
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = '1km-INSPIRE')
							) as t1 on w1.label = t1.label 
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = '25km-INSPIRE')
							) as t2 on w1.label_sup = t2.label
				inner join gisdata.f_a_cell as t3 on t1.id = t3.estimation_cell
				inner join gisdata.f_a_cell as t4 on t2.id = t4.estimation_cell
		)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select cell, cell_sup from w2 order by cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				w1.label as label,
				w2.label as label_sup
		from csv_server_gisdata.inspire_grid_25km as w1, csv_server_gisdata.inspire_grid_50km as w2
		where w1.geom && w2.geom
		and st_intersects(st_centroid(w1.geom),w2.geom)
		)
,w2 as	(
		select
				w1.label,
				w1.label_sup,
				t3.gid as cell,
				t4.gid as cell_sup
		from
				w1
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = '25km-INSPIRE')
							) as t1 on w1.label = t1.label 
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = '50km-INSPIRE')
							) as t2 on w1.label_sup = t2.label
				inner join gisdata.f_a_cell as t3 on t1.id = t3.estimation_cell
				inner join gisdata.f_a_cell as t4 on t2.id = t4.estimation_cell
		)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select cell, cell_sup from w2 order by cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				w1.label as label,
				w2.label as label_sup
		from csv_server_gisdata.inspire_grid_50km as w1, csv_server_gisdata.inspire_grid_100km as w2
		where w1.geom && w2.geom
		and st_intersects(st_centroid(w1.geom),w2.geom)
		)
,w2 as	(
		select
				w1.label,
				w1.label_sup,
				t3.gid as cell,
				t4.gid as cell_sup
		from
				w1
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = '50km-INSPIRE')
							) as t1 on w1.label = t1.label 
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = '100km-INSPIRE')
							) as t2 on w1.label_sup = t2.label
				inner join gisdata.f_a_cell as t3 on t1.id = t3.estimation_cell
				inner join gisdata.f_a_cell as t4 on t2.id = t4.estimation_cell
		)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select cell, cell_sup from w2 order by cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				w1.label as label,
				w2.label as label_sup
		from csv_server_gisdata.inspire_grid_100km as w1, csv_server_gisdata.inspire_grid_up as w2
		where w1.geom && w2.geom
		and st_intersects(st_centroid(w1.geom),w2.geom)
		)
,w2 as	(
		select
				w1.label,
				w1.label_sup,
				t3.gid as cell,
				t4.gid as cell_sup
		from
				w1
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = '100km-INSPIRE')
							) as t1 on w1.label = t1.label 
				inner join	(
							select * from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = 'INSPIRE')
							) as t2 on w1.label_sup = t2.label
				inner join gisdata.f_a_cell as t3 on t1.id = t3.estimation_cell
				inner join gisdata.f_a_cell as t4 on t2.id = t4.estimation_cell
		)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select cell, cell_sup from w2 order by cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select
		(
		select gid from gisdata.f_a_cell where estimation_cell =
		(select id from gisdata.c_estimation_cell where estimation_cell_collection =
		(select id from gisdata.c_estimation_cell_collection where label = 'INSPIRE'))
		) as cell,
		null::integer as cell_sup;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze gisdata.cm_f_a_cell;
analyze gisdata.c_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--DROP FOREIGN TABLE csv_server_gisdata.mask;
drop table csv_server_gisdata.inspire_grid_up;
drop table csv_server_gisdata.inspire_grid_100km;
drop table csv_server_gisdata.inspire_grid_50km;
drop table csv_server_gisdata.inspire_grid_25km;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
