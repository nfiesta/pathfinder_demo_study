#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime
import psycopg2
import psycopg2.extras
from multiprocessing import Pool

# from https://gitlab.com/nfiesta/pathfinder_demo_study/-/blob/main/csv_import/pathfinder_import_ds_add.py?ref_type=heads#L150-226

def single_query(cs, c, m, _sql, verbose=True):
    try:
        #connstr = 'host=localhost port=5433 dbname=nfi_esta user=vagrant' # for network authentication (password can be stored in ~/.pgpass)
        connstr = cs #'dbname=nfi_esta_dev' # for local linux based authentication (without password)
        conn = psycopg2.connect(connstr)
    except psycopg2.DatabaseError as e:
        print("nfiesta_parallel.single_query(), not possible to connect DB")
        print(e)
        return 1

    conn.set_session(isolation_level=None, readonly=None, deferrable=None, autocommit=True) # to not execute in transaction
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cmd = '%s' % (_sql)

    try:
        cur.execute(cmd)
    except psycopg2.Error as e:
        print("nfiesta_parallel.single_query(), SQL error:")
        print(e)
        return 2
    if cur.description != None:
        path_row_list = cur.fetchall()
        if verbose:
            print('%s\t%s/%s\t%s\t%s' % (datetime.now(), c, m, cmd, path_row_list))
        else:
            print('%s\t%s/%s' % (datetime.now(), c, m,))
    else:
        path_row_list = None
        if verbose:
            print('%s\t%s/%s\tnfiesta_parallel.py result None: %s' % (datetime.now(), c, m, path_row_list))
        else:
            print('%s\t%s/%s' % (datetime.now(), c, m,))
    cur.close()
    conn.close()
    return path_row_list


print('%s------------------------------raster retiling' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S")))
cs = 'dbname=pathfinder_ds_1_5'
processes = 60
q = '''
with w_table_size as (
        select
                min(rid),
                max(rid),
                count(*),
                1000 as bucket_size
        from gisdata.r_ref_10
)
select
        generate_series(min, max, bucket_size) as min,
        generate_series(min+(bucket_size-1), max+(bucket_size-1), bucket_size) as max
from w_table_size
;
'''
res = single_query(cs, 1, 1, q, False)
print(res)
print('%s------------------------------parallel processing using %s processes' % (datetime.now().strftime("%Y-%b-%d %H:%M:%S"), processes))
# create table gisdata.r_tcd_10_eu(rid bigint, rast raster);
# truncate table gisdata.r_tcd_10_eu;
q = '''
with w_onetile as (
    select
        rid,
        rast
    from gisdata.r_ref_10
    where rid >= %(min)s and rid <= %(max)s
)
, w_transformation as (
    select
        w_onetile.rid,
        count(r_tcd.rast) as tile_count,
        st_clip(
            st_transform(
                st_union(r_tcd.rast),
                w_onetile.rast),
            st_convexHull(w_onetile.rast)
        ) as rast
        from
            w_onetile,
            gisdata.r_tcd
        where st_intersects(st_convexhull(r_tcd.rast), st_convexhull(w_onetile.rast))
        group by w_onetile.rid, w_onetile.rast
)
,w_reclass as	(
				select
						rid,
						ST_Reclass(rast, 1, '[0-0]:0, [1-100]:1-100, [101-255]:0','8BUI',0) as rast
				from
						w_transformation
				where
						ST_BandIsNoData(rast, 1, true) = false
				)
insert into gisdata.r_tcd_10_eu(rid, rast)
select rid, rast from w_reclass where ST_BandIsNoData(rast, 1, true) = false
returning rid;
'''

params = [[cs, c+1, len(res), q % {'min': r['min'], 'max': r['max']}, False] for c, r in enumerate(res)]
with Pool(processes) as p:
    res_multi = p.starmap(single_query, params, chunksize=1)
print(len(res_multi))