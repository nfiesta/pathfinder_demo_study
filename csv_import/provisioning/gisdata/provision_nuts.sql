
-- CREATE SCHEMA gisdata;
-- create extension nfiesta_gisdata with schema gisdata;
CREATE SCHEMA csv_server_gisdata;
alter schema csv_server_gisdata owner to adm_nfiesta_gisdata;

---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
\set afile '/home/lukas/pathfinder_demo_study/csv_import/server_side/EU_mask.csv'
CREATE FOREIGN TABLE csv_server_gisdata.mask (
        country                 character varying(20)           not null,
        strata_set              character varying(20)           not null,
        stratum                 character varying(20)           not null,
        label                   character varying(120)          not null,
        geometry                text                            not null,
        area_ha                 double precision,
        comment                 text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename:'afile' );
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
\set afile '/home/lukas/pathfinder_demo_study/csv_import/server_side/nuts_rg_01m_2021_3035_extent_clip.csv'
CREATE FOREIGN TABLE csv_server_gisdata.nuts_eurostat (
		id						integer							not null,
		geom					text							not null,
		_uid_					integer							not null,
		nuts_id					text							not null,
		levl_code				integer							not null,
		cntr_code				text							not null,
		name_latn				text							not null,
		nuts_name				text							not null,
		mount_type				integer							not null,
		urbn_type				integer							not null,
		coast_type				integer							not null,
		fid						text							not null
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ',', filename:'afile' );
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
drop table if exists csv_server_gisdata.nuts_eurostat4aux;
create table csv_server_gisdata.nuts_eurostat4aux as
with
w1 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'AT' order by cntr_code, levl_code, nuts_id),
w2 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'BE' order by cntr_code, levl_code, nuts_id),
w3 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'CH' order by cntr_code, levl_code, nuts_id),
w4 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'CZ' order by cntr_code, levl_code, nuts_id),
w5 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'DE' order by cntr_code, levl_code, nuts_id),
w6 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'DK' order by cntr_code, levl_code, nuts_id),
w7 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'EE' order by cntr_code, levl_code, nuts_id),
w8 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'ES' order by cntr_code, levl_code, nuts_id),
w9 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'FI' order by cntr_code, levl_code, nuts_id),
w10 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'FR' order by cntr_code, levl_code, nuts_id),
w11 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'HU' order by cntr_code, levl_code, nuts_id),
w12 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'IE' order by cntr_code, levl_code, nuts_id),
w13 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'IS' order by cntr_code, levl_code, nuts_id),
w14 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'IT' order by cntr_code, levl_code, nuts_id),
w15 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'NO' order by cntr_code, levl_code, nuts_id),
w16 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'PL' order by cntr_code, levl_code, nuts_id),
w17 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'RS' order by cntr_code, levl_code, nuts_id),
w18 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'SE' order by cntr_code, levl_code, nuts_id),
w19 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'SI' order by cntr_code, levl_code, nuts_id),
w20 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'SK' order by cntr_code, levl_code, nuts_id),
w21 as	(
		select * from w1 union all
		select * from w2 union all
		select * from w3 union all
		select * from w4 union all
		select * from w5 union all
		select * from w6 union all
		select * from w7 union all
		select * from w8 union all
		select * from w9 union all
		select * from w10 union all
		select * from w11 union all
		select * from w12 union all
		select * from w13 union all
		select * from w14 union all
		select * from w15 union all
		select * from w16 union all
		select * from w17 union all
		select * from w18 union all
		select * from w19 union all
		select * from w20
		)
select row_number() over(order by cntr_code, levl_code, nuts_id) as id, * from w21 order by cntr_code, levl_code, nuts_id;

ALTER TABLE csv_server_gisdata.nuts_eurostat4aux ADD PRIMARY KEY (id);
CREATE INDEX idx_nuts_eurostat4aux_cntr_code ON csv_server_gisdata.nuts_eurostat4aux USING btree (cntr_code);
CREATE INDEX idx_nuts_eurostat4aux_levl_code ON csv_server_gisdata.nuts_eurostat4aux USING btree (levl_code);
alter table csv_server_gisdata.nuts_eurostat4aux owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.nuts_eurostat4aux to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
drop table if exists csv_server_gisdata.nuts_eurostat4aux_hierarchy;
create table csv_server_gisdata.nuts_eurostat4aux_hierarchy as
with
w1 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'AT' order by cntr_code, levl_code, nuts_id),
w2 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'BE' order by cntr_code, levl_code, nuts_id),
w3 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'CH' order by cntr_code, levl_code, nuts_id),
w4 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'CZ' order by cntr_code, levl_code, nuts_id),
w5 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'DE' order by cntr_code, levl_code, nuts_id),
w6 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'DK' order by cntr_code, levl_code, nuts_id),
w7 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'EE' order by cntr_code, levl_code, nuts_id),
w8 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'ES' order by cntr_code, levl_code, nuts_id),
w9 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'FI' order by cntr_code, levl_code, nuts_id),
w10 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'FR' order by cntr_code, levl_code, nuts_id),
w11 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'HU' order by cntr_code, levl_code, nuts_id),
w12 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'IE' order by cntr_code, levl_code, nuts_id),
w13 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'IS' order by cntr_code, levl_code, nuts_id),
w14 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'IT' order by cntr_code, levl_code, nuts_id),
w15 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'NO' order by cntr_code, levl_code, nuts_id),
w16 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'PL' order by cntr_code, levl_code, nuts_id),
w17 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'RS' order by cntr_code, levl_code, nuts_id),
w18 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'SE' order by cntr_code, levl_code, nuts_id),
w19 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'SI' order by cntr_code, levl_code, nuts_id),
w20 as	(select cntr_code, nuts_id, name_latn, levl_code, geom from csv_server_gisdata.nuts_eurostat where cntr_code = 'SK' order by cntr_code, levl_code, nuts_id),
---------
w21 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w1 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w1 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w1 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w1 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w22 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w2 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w2 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w2 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w2 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)		
---------
,w23 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w3 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w3 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w3 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w3 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w24 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w4 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w4 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w4 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w4 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w25 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w5 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w5 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w5 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w5 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w26 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w6 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w6 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w6 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w6 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w27 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w7 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w7 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w7 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w7 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w28 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w8 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w8 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w8 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w8 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w29 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w9 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w9 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w9 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w9 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w30 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w10 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w10 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w10 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w10 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w31 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w11 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w11 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w11 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w11 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w32 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w12 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w12 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w12 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w12 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w33 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w13 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w13 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w13 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w13 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)			
---------
,w34 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w14 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w14 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w14 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w14 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w35 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w15 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w15 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w15 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w15 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w36 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w16 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w16 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w16 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w16 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)	
---------
,w37 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w17 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w17 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w17 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w17 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w38 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w18 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w18 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w18 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w18 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w39 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w19 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w19 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w19 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w19 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
---------
,w40 as	(
		select
				t1.cntr_code as country_code,
				t1.nuts_id as nuts3,
				t1.name_latn as nuts3_label,
				t1.geom,
				t2.nuts_id as nuts2,
				t2.name_latn as nuts2_label,
				t3.nuts_id as nuts1,
				t3.name_latn as nuts1_label,
				t4.nuts_id as nuts0,
				t4.name_latn as nuts0_label
		from (select cntr_code, nuts_id, name_latn, geom from w20 where levl_code = 3) as t1
		inner join (select nuts_id, name_latn from w20 where levl_code = 2) as t2 on substring(t1.nuts_id from 1 for (length(t1.nuts_id)-1)) = t2.nuts_id
		inner join (select nuts_id, name_latn from w20 where levl_code = 1) as t3 on substring(t2.nuts_id from 1 for (length(t2.nuts_id)-1)) = t3.nuts_id
		inner join (select nuts_id, name_latn from w20 where levl_code = 0) as t4 on substring(t3.nuts_id from 1 for (length(t3.nuts_id)-1)) = t4.nuts_id
		)
,w41 as	(
		select * from w21 union all
		select * from w22 union all
		select * from w23 union all
		select * from w24 union all
		select * from w25 union all
		select * from w26 union all
		select * from w27 union all
		select * from w28 union all
		select * from w29 union all
		select * from w30 union all
		select * from w31 union all
		select * from w32 union all
		select * from w33 union all
		select * from w34 union all
		select * from w35 union all
		select * from w36 union all
		select * from w37 union all
		select * from w38 union all
		select * from w39 union all
		select * from w40
		)
select
		row_number() over() as id,
		country_code,
		nuts3,
		nuts3_label,
		st_setsrid(ST_GeomFromEWKT(geom),3035) as geom,
		nuts2,
		nuts2_label,
		nuts1,
		nuts1_label,
		nuts0,
		nuts0_label		
from
		w41;
		
ALTER TABLE csv_server_gisdata.nuts_eurostat4aux_hierarchy ADD PRIMARY KEY (id);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_geom ON csv_server_gisdata.nuts_eurostat4aux_hierarchy USING gist (geom);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_nuts3 ON csv_server_gisdata.nuts_eurostat4aux_hierarchy USING btree (nuts3);
alter table csv_server_gisdata.nuts_eurostat4aux_hierarchy owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.nuts_eurostat4aux_hierarchy to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
drop table if exists csv_server_gisdata.inspire_grid_1km;
create table csv_server_gisdata.inspire_grid_1km as
with w_mask as (
	select
		1000 as scalex, -1000 as  scaley,
		'1km' as label,
		(select st_buffer(st_union(st_geomfromewkt(geometry)), 0) from csv_server_gisdata.mask) as mask
)
, w_cover as (
	select
		scalex, scaley, label,
		mask,
		--st_envelope(mask),
		ST_MakeEnvelope(
			floor(ST_XMin(mask)/scalex)*scalex, 
			floor(ST_YMin(mask)/abs(scaley))*abs(scaley), 
			ceil(ST_XMax(mask)/scalex)*scalex, 
			ceil(ST_YMax(mask)/abs(scaley))*abs(scaley),
			3035) as extent
	from w_mask
)		
, w_grid as (
	SELECT 
		(ST_PixelAsPolygons(ST_AddBand(ST_MakeEmptyRaster(
		((ST_Xmax(w_cover.extent) - ST_Xmin(w_cover.extent)) / scalex)::int, ((ST_Ymax(w_cover.extent) - ST_Ymin(w_cover.extent)) / abs(scaley))::int,
		ST_Xmin(w_cover.extent),
		ST_Ymax(w_cover.extent),
		scalex, scaley, 0, 0, 3035), '8BSI'::text, 1, 0), 1, false)).geom
	from w_cover
)
select
	row_number() over() as gid,
	format('%s-INSPIRE', w_cover.label) as cell_collection,
	concat(w_cover.label, 'N', (ST_ymin(geom)/1000)::int::varchar, 'E', (ST_xmin(geom)/1000)::int::varchar) as label,
    	format('The %s-INSPIRE grid cell, the south-west corner of which is located %s km north and %s km east of the false origin of the ETRS89-extended / LAEA - Europe projection system.',
		w_cover.label, (ST_ymin(geom)/1000)::int::varchar, (ST_xmin(geom)/1000)::int::varchar) as description,
	geom
from w_grid, w_cover
where st_intersects(w_grid.geom, w_cover.mask);

ALTER TABLE csv_server_gisdata.inspire_grid_1km ADD PRIMARY KEY (gid);
CREATE INDEX idx_inspire_grid_1km_geom ON csv_server_gisdata.inspire_grid_1km USING gist (geom);
CREATE INDEX idx_inspire_grid_1km_gid ON csv_server_gisdata.inspire_grid_1km USING btree (gid);
alter table csv_server_gisdata.inspire_grid_1km owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.inspire_grid_1km to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
INSERT INTO gisdata.c_estimation_cell_collection(id,label,description,label_en,description_en)
VALUES
(1,	'NUTS','NUTS','NUTS','NUTS'),
(2,	'NUTS0','NUTS0','NUTS0','NUTS0'),
(3,	'NUTS1','NUTS1','NUTS1','NUTS1'),
(4,	'NUTS2','NUTS2','NUTS2','NUTS2'),
(5,	'NUTS3','NUTS3','NUTS3','NUTS3'),
(6,	'NUTS3-1km-inspire-APPROXIMATE','NUTS3-1km-inspire-APPROXIMATE','NUTS3-1km-inspire-APPROXIMATE','NUTS3-1km-inspire-APPROXIMATE');
update gisdata.c_estimation_cell_collection set use4estimates = false where label = 'NUTS3-1km-inspire-APPROXIMATE';
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
INSERT INTO gisdata.cm_estimation_cell_collection(id,estimation_cell_collection,estimation_cell_collection_lowest,estimation_cell_collection_highest)
VALUES
(1,1,6,1), -- nuts
(2,2,6,1), -- nuts0
(3,3,6,1), -- nuts1
(4,4,6,1), -- nuts2
(5,5,6,1), -- nuts3
(6,6,6,1); -- nuts3-1km-inspire-approximate
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		coalesce((select max(id) from gisdata.c_estimation_cell),0) + 1 as id,
		(select id from gisdata.c_estimation_cell_collection where label = 'NUTS') as estimation_cell_collection,
		(select label from gisdata.c_estimation_cell_collection where label = 'NUTS') as label,
		(select description from gisdata.c_estimation_cell_collection where label = 'NUTS') as description,
		(select label_en from gisdata.c_estimation_cell_collection where label = 'NUTS') as label_en,
		(select description_en from gisdata.c_estimation_cell_collection where label = 'NUTS') as description_en;
---------------------------------------
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		id + (select max(id) from gisdata.c_estimation_cell) as id,
		case
			when levl_code = 0 then (select id from gisdata.c_estimation_cell_collection where label = 'NUTS0')
			when levl_code = 1 then (select id from gisdata.c_estimation_cell_collection where label = 'NUTS1')
			when levl_code = 2 then (select id from gisdata.c_estimation_cell_collection where label = 'NUTS2')
			when levl_code = 3 then (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3')
		end
			as estimation_cell_collection,		
		nuts_id as label,
		nuts_id || ' - ' || name_latn as description,
		nuts_id as label_en,
		nuts_id || ' - ' || name_latn as description_en
from
		csv_server_gisdata.nuts_eurostat4aux order by id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
create table csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect as
with
w1 as	(
		select * from csv_server_gisdata.nuts_eurostat4aux_hierarchy
		)
,w2 as	(
		select * from csv_server_gisdata.inspire_grid_1km
		)
,w3 as	(
		select
				w1.id as nuts3_id,
				w1.nuts3,
				w1.nuts2,
				w1.nuts1,
				w1.nuts0,
				w2.gid,
				w2.label,
				st_setsrid(w2.geom,3035) as geom
		from w1, w2
		where w1.geom && w2.geom
		and st_intersects(w1.geom, st_setsrid(st_centroid(w2.geom),3035))
		)	
select
		row_number() over() as id,
		w3.nuts3_id,
		w3.nuts3,
		w3.nuts2,
		w3.nuts1,
		w3.nuts0,
		w3.gid,
		w3.label,
		w3.geom
from
		w3; -- 3 934 692r

ALTER TABLE csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect ADD PRIMARY KEY (id);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_nuts3_id ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING btree (nuts3_id);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_nuts3 ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING btree (nuts3);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_nuts2 ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING btree (nuts2);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_nuts1 ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING btree (nuts1);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_nuts0 ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING btree (nuts0);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_gid   ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING btree (gid);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_label ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING btree (label);
CREATE INDEX idx_nuts_eurostat4aux_hierarchy_intersect_geom  ON csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect USING gist (geom);
alter table csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect owner to adm_nfiesta_gisdata;
grant select on table csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect to public;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
insert into gisdata.c_estimation_cell(id,estimation_cell_collection,label,description,label_en,description_en)
select
		id + (select max(id) from gisdata.c_estimation_cell) as id,
		(select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE') as estimation_cell_collection,	
		label as label,
		label as description,
		label label_en,
		label as description_en
from
		csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect order by id;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select t1.label, t1.geom, t2.id as id_estimation_cell from
		(select label, st_multi(geom) as geom from csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect) as t1
		inner join (select cec.* from gisdata.c_estimation_cell as cec where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE')) as t2
		on t1.label = t2.label
		)
insert into gisdata.f_a_cell(geom,estimation_cell)
select w1.geom, w1.id_estimation_cell from w1 order by id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select nuts3 as label, st_multi(st_union(geom)) as geom
		from csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect
		group by nuts3
		)
,w2 as	(
		select w1.label, w1.geom, t1.id as id_estimation_cell from w1
		inner join	(
					select cec.* from gisdata.c_estimation_cell as cec
					where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS3')
					) as t1
		on w1.label = t1.label
		)
insert into gisdata.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select nuts2 as label, st_multi(st_union(geom)) as geom
		from csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect
		group by nuts2
		)
,w2 as	(
		select w1.label, w1.geom, t1.id as id_estimation_cell from w1
		inner join	(
					select cec.* from gisdata.c_estimation_cell as cec
					where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS2')
					) as t1
		on w1.label = t1.label
		)
insert into gisdata.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select nuts1 as label, st_multi(st_union(geom)) as geom
		from csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect
		group by nuts1
		)
,w2 as	(
		select w1.label, w1.geom, t1.id as id_estimation_cell from w1
		inner join	(
					select cec.* from gisdata.c_estimation_cell as cec
					where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS1')
					) as t1
		on w1.label = t1.label
		)
insert into gisdata.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select nuts0 as label, st_multi(st_union(geom)) as geom
		from csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect
		group by nuts0
		)
,w2 as	(
		select w1.label, w1.geom, t1.id as id_estimation_cell from w1
		inner join	(
					select cec.* from gisdata.c_estimation_cell as cec
					where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS0')
					) as t1
		on w1.label = t1.label
		)
insert into gisdata.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2 order by w2.id_estimation_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select
				(
				select cec.id from gisdata.c_estimation_cell as cec
				where estimation_cell_collection = (select id from gisdata.c_estimation_cell_collection where label = 'NUTS')
				) as id_estimation_cell,
				geom
		from
				csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect
		)
,w2 as	(
		select w1.id_estimation_cell, st_multi(st_union(w1.geom)) as geom from w1 group by w1.id_estimation_cell
		)
insert into gisdata.f_a_cell(geom,estimation_cell)
select w2.geom, w2.id_estimation_cell from w2;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze gisdata.f_a_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
with
w_nuts as	(
			select gid as cell, null::integer as cell_sup from gisdata.f_a_cell
			where estimation_cell =
				(
				select id from gisdata.c_estimation_cell where estimation_cell_collection =
				(select id from gisdata.c_estimation_cell_collection where label = 'NUTS')
				)
			)
,w_nuts0 as	(
			select
					gid as cell,
					(
						select gid as cell from gisdata.f_a_cell
						where estimation_cell =
							(
							select id from gisdata.c_estimation_cell where estimation_cell_collection =
							(select id from gisdata.c_estimation_cell_collection where label = 'NUTS')
							)					
					) as cell_sup
			from gisdata.f_a_cell where estimation_cell in
			(select id from gisdata.c_estimation_cell where estimation_cell_collection =
			(select id from gisdata.c_estimation_cell_collection where label = 'NUTS0'))
			)
,w_nuts1 as	(	
			select
					--t1.id,
					--t1.nuts1,
					--t2.nuts0,
					--t3.id,
					t4.gid as cell,
					t5.gid as cell_sup
			from
					(
					select id, label as nuts1 from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1')
					) as t1
			inner join
					(
					select distinct nuts1, nuts0 from csv_server_gisdata.nuts_eurostat4aux_hierarchy where nuts1
					in (select label from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1'))
					) as t2
					on t1.nuts1 = t2.nuts1
					inner join
					(
					select id, label from gisdata.c_estimation_cell where estimation_cell_collection =
						(select id from gisdata.c_estimation_cell_collection where label = 'NUTS0')
					) as t3
					on t2.nuts0 = t3.label
			inner join gisdata.f_a_cell as t4 on t1.id = t4.estimation_cell
			inner join gisdata.f_a_cell as t5 on t3.id = t5.estimation_cell
			)
,w_nuts2 as (
			select
					--t1.id,
					--t1.nuts2,
					--t2.nuts1,
					--t3.id,
					t4.gid as cell,
					t5.gid as cell_sup
			from
					(
					select id, label as nuts2 from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS2')
					) as t1
			inner join
					(
					select distinct nuts2, nuts1 from csv_server_gisdata.nuts_eurostat4aux_hierarchy where nuts2
					in (select label from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS2'))
					) as t2
					on t1.nuts2 = t2.nuts2
					inner join
					(
					select id, label from gisdata.c_estimation_cell where estimation_cell_collection =
						(select id from gisdata.c_estimation_cell_collection where label = 'NUTS1')
					) as t3
					on t2.nuts1 = t3.label
			inner join gisdata.f_a_cell as t4 on t1.id = t4.estimation_cell
			inner join gisdata.f_a_cell as t5 on t3.id = t5.estimation_cell
			)
,w_nuts3 as (
			select
					--t1.id,
					--t1.nuts3,
					--t2.nuts2,
					--t3.id,
					t4.gid as cell,
					t5.gid as cell_sup
			from
					(
					select id, label as nuts3 from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS3')
					) as t1
			inner join
					(
					select distinct nuts3, nuts2 from csv_server_gisdata.nuts_eurostat4aux_hierarchy where nuts3
					in (select label from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS3'))
					) as t2
					on t1.nuts3 = t2.nuts3
					inner join
					(
					select id, label from gisdata.c_estimation_cell where estimation_cell_collection =
						(select id from gisdata.c_estimation_cell_collection where label = 'NUTS2')
					) as t3
					on t2.nuts2 = t3.label
			inner join gisdata.f_a_cell as t4 on t1.id = t4.estimation_cell
			inner join gisdata.f_a_cell as t5 on t3.id = t5.estimation_cell
			)
,w_nuts3_1km as	(
				select
						t1.label as label_1km,
						t2.nuts3,
						t3.id as id_down,
						t4.id as id_up,
						t5.gid as cell,
						t6.gid as cell_sup
				from
					(select label, nuts3_id from csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect) as t1
					inner join csv_server_gisdata.nuts_eurostat4aux_hierarchy as t2 on t1.nuts3_id = t2.id
				inner join
					(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS3-1km-inspire-APPROXIMATE')
					) as t3 on t1.label = t3.label
				inner join
					(
					select * from gisdata.c_estimation_cell where estimation_cell_collection =
					(select id from gisdata.c_estimation_cell_collection where label = 'NUTS3')
					) as t4 on t2.nuts3 = t4.label
				inner join gisdata.f_a_cell as t5 on t3.id = t5.estimation_cell
				inner join gisdata.f_a_cell as t6 on t4.id = t6.estimation_cell
				)
,w_res as	(
			select cell, cell_sup from w_nuts  union all
			select cell, cell_sup from w_nuts0 union all
			select cell, cell_sup from w_nuts1 union all
			select cell, cell_sup from w_nuts2 union all
			select cell, cell_sup from w_nuts3 union all
			select cell, cell_sup from w_nuts3_1km
			)
insert into gisdata.cm_f_a_cell(cell,cell_sup)
select cell, cell_sup from w_res order by cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
analyze gisdata.cm_f_a_cell;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--DROP FOREIGN TABLE csv_server_gisdata.mask;
DROP FOREIGN TABLE csv_server_gisdata.nuts_eurostat;
drop table csv_server_gisdata.nuts_eurostat4aux;
drop table csv_server_gisdata.nuts_eurostat4aux_hierarchy;
--drop table csv_server_gisdata.nuts_eurostat4aux_hierarchy_intersect;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
