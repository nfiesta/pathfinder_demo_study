#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
from datetime import datetime
import csv
import re

def check_unique(conn, prep_cmd, columns, tablename, err_f):
    from pathfinder_import_ds import run_sql
    run_sql(conn, prep_cmd, (None, ))
    cmd = '''
        select
            %s,
            count(*)
        from csv_fk_check_country.%s
        group by %s
        having count(*) > 1;
    ''' % (columns, tablename, columns)
    out_prl = run_sql(conn, cmd, (None, ))
    if len(out_prl) > 0:
        print('\nunique test failed on CSV %s' % tablename, file=err_f)
        print('cmd: %s' % cmd, file=err_f)
        print('%s records not unique:' % len(out_prl), file=err_f)
        err_fname = '%s_%s_unique.csv' % (err_f.name[:-4], tablename) #re.search(r'csv_fk_check_country.(?P<table_name>[^ ]+)', cmd).groupdict()['table_name'])
        print('full data written in CSV: %s' % err_fname, file=err_f)
        print('----------------------------------------------------', file=err_f)
        with open(err_fname, 'w', newline='') as f:
            csv.writer(f, delimiter =';').writerows(out_prl)
        conn.commit()
        return 1
    conn.commit()
    return 0

def check_fkey(conn, ltablename, rtablename, joincond, isnullcolumn, uniquecmd, fkeycmd, err_f):
    import psycopg2
    import psycopg2.extras
    import psycopg2.errorcodes
    from pathfinder_import_ds import run_sql
    #print('-----------------------------check_fkey %s -> %s' % (ltablename, rtablename))
    cmd = '''
        select
            %s.*
        from %s
        left join %s on (%s)
        where %s;''' % (ltablename, ltablename, rtablename, joincond, isnullcolumn)
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    try:
        cur.execute(cmd)
    except psycopg2.Error as e:
        print("pathfinder_import_ds_fkey.py, SQL error:")
        print(e)
        sys.exit(1)
    if cur.description != None:
        out_prl = cur.fetchall()
        column_names = [desc[0] for desc in cur.description]
    else:
        path_row_list = None
    cur.close()

    if len(out_prl) > 0:
        print('\nfkey test failed between CSVs %s -> %s' % (ltablename, rtablename), file=err_f)
        print('\ncmd: %s' % cmd, file=err_f)
        print('%s records not found:' % len(out_prl), file=err_f)
        err_fname = '%s_%s.csv' % (err_f.name[:-4], re.search(r'(?P<fkey_name>[^ ]+_fkey)', fkeycmd).groupdict()['fkey_name'])
        print('full data written in CSV: %s' % err_fname, file=err_f)
        print('----------------------------------------------------', file=err_f)
        with open(err_fname, 'w', newline='') as f:
            csv.writer(f, delimiter =';').writerows([column_names])
            csv.writer(f, delimiter =';').writerows(out_prl)
        conn.commit()
        return 1
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    try:
        if uniquecmd != '':
            cur.execute(uniquecmd, (None, ))
        cur.execute(fkeycmd, (None, ))
    except psycopg2.Error as e:
        if e.pgcode == psycopg2.errorcodes.INVALID_FOREIGN_KEY:
            print('it is not possible to create foreign key', file=err_f)
            print(e, file=err_f)
            print('uniquecmd: %s' % uniquecmd, file=err_f)
            print('fkeycmd: %s' % fkeycmd, file=err_f)
        else:
            print("pathfinder_import_ds_fkey.py, SQL error:")
            print(e)
            sys.exit(1)
    conn.commit()
    return 0

def etl_check_country_fkeys(conn, country_r, dir_version, log_f):
    from pathfinder_import_ds import run_sql
    from pathfinder_import_ds import pf_print
    pf_print('==============================country %s fkey check=============================' % (country_r), log_f)
    number_of_unique_exceptions = 0
    number_of_fkey_exceptions = 0
    err_log_name = os.path.join('countries', country_r, 'log', 'FKEYs_errors.txt')
    err_f = open(err_log_name, "a")
    err_f.seek(0, 0)
    err_f.truncate()
    print("log timestamp: %s" % datetime.now().strftime("%Y-%b-%d %H:%M:%S"), file=err_f)
    res = run_sql(conn, '''drop schema if exists csv_fk_check_country cascade;create schema csv_fk_check_country;''', (None, ))
    conn.commit()
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.countries as select label from csv_server.countries;
    ''', 'label', 'countries', err_f)
    #--strata_sets
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.strata_sets	as select country, strata_set from csv_country.strata_sets;
    ''', 'country, strata_set', 'strata_sets', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.strata_sets', 'csv_fk_check_country.countries', 'strata_sets.country = countries.label', 'countries.label is null', '''
alter table csv_fk_check_country.countries 	add constraint countries_label_key 		unique (label);
''','''
alter table csv_fk_check_country.strata_sets 	add constraint strata_sets_country_fkey		foreign key (country) references csv_fk_check_country.countries (label);
    ''', err_f)
    #--strata
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.strata 	as select country, strata_set, stratum from csv_country.strata;
    ''', 'country, strata_set, stratum', 'strata', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.strata', 'csv_fk_check_country.strata_sets', 'strata.country = strata_sets.country and strata.strata_set = strata_sets.strata_set', 'strata_sets.strata_set is null', '''
alter table csv_fk_check_country.strata_sets 	add constraint strata_sets_country_strata_set_key	unique (country, strata_set);
''','''
alter table csv_fk_check_country.strata 	add constraint strata_country_strata_set_fkey 		foreign key (country, strata_set) references csv_fk_check_country.strata_sets (country, strata_set);
    ''', err_f)
    #--cluster_configurations
    if dir_version == '1_0':
        number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.cluster_configurations	as select country, cluster_configuration from csv_country.cluster_configurations;
    ''', 'country, cluster_configuration', 'cluster_configurations', err_f)
        number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.cluster_configurations', 'csv_fk_check_country.countries', 'cluster_configurations.country = countries.label', 'countries.label is null', '','''
alter table csv_fk_check_country.cluster_configurations		add constraint cluster_configurations_country_fkey foreign key (country) references csv_fk_check_country.countries (label);
    ''', err_f)
    elif dir_version == '1_5':
        number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.cluster_configurations	as select country, strata_set, stratum, cluster_configuration from csv_country.cluster_configurations;
    ''', 'country, strata_set, stratum, cluster_configuration', 'cluster_configurations', err_f)
        number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.cluster_configurations', 'csv_fk_check_country.strata', 'cluster_configurations.country = strata.country and cluster_configurations.strata_set = strata.strata_set and cluster_configurations.stratum = strata.stratum', 'strata.stratum is null', '''
alter table csv_fk_check_country.strata 	add constraint strata_country_strata_set_stratum_key2 	unique (country, strata_set, stratum);
    ''','''
alter table csv_fk_check_country.cluster_configurations		add constraint cluster_configurations_stratum_fkey foreign key (country, strata_set, stratum) references csv_fk_check_country.strata (country, strata_set, stratum);
    ''', err_f)
    #--panels
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.panels 	as select country, strata_set, stratum, cluster_configuration, panel from csv_country.panels;
    ''', 'country, strata_set, stratum, cluster_configuration, panel', 'panels', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.panels', 'csv_fk_check_country.strata', 'panels.country = strata.country and panels.strata_set = strata.strata_set and panels.stratum = strata.stratum', 'strata.stratum is null', '''
alter table csv_fk_check_country.strata 	add constraint strata_country_strata_set_stratum_key 	unique (country, strata_set, stratum);
    ''','''
alter table csv_fk_check_country.panels 	add constraint panels_country_strata_set_stratum_fkey 	foreign key (country, strata_set, stratum) references csv_fk_check_country.strata (country, strata_set, stratum);
    ''', err_f)
    if dir_version == '1_0':
        number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.panels', 'csv_fk_check_country.cluster_configurations', 'panels.country = cluster_configurations.country and panels.cluster_configuration = cluster_configurations.cluster_configuration', 'cluster_configurations.cluster_configuration is null', '''
alter table csv_fk_check_country.cluster_configurations add constraint cluster_configurations_country_cluster_configuration_key	unique (country, cluster_configuration);
''','''
alter table csv_fk_check_country.panels 	add constraint panels_country_cluster_configuration_fkey	foreign key (country, cluster_configuration) references csv_fk_check_country.cluster_configurations (country, cluster_configuration);
    ''', err_f)
    elif dir_version == '1_5':
        number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.panels', 'csv_fk_check_country.cluster_configurations', 'panels.country = cluster_configurations.country and panels.strata_set = cluster_configurations.strata_set and panels.stratum = cluster_configurations.stratum and panels.cluster_configuration = cluster_configurations.cluster_configuration', 'cluster_configurations.cluster_configuration is null', '''
alter table csv_fk_check_country.cluster_configurations add constraint cluster_configurations_country_cluster_configuration_key	unique (country, strata_set, stratum, cluster_configuration);
''','''
alter table csv_fk_check_country.panels 	add constraint panels_country_strata_set_stratum_cluster_configuration_fkey	foreign key (country, strata_set, stratum, cluster_configuration) references csv_fk_check_country.cluster_configurations (country, strata_set, stratum, cluster_configuration);
    ''', err_f)
    #--clusters
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.clusters 	as select country, strata_set, stratum, panel, cluster from csv_country.clusters;
    ''', 'country, strata_set, stratum, panel, cluster', 'clusters', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.clusters', 'csv_fk_check_country.panels', 'clusters.country = panels.country and clusters.strata_set = panels.strata_set and clusters.stratum = panels.stratum and clusters.panel = panels.panel', 'panels.panel is null', '''
alter table csv_fk_check_country.panels 	add constraint panels_country_strata_set_stratum_panel_key unique (country, strata_set, stratum, panel);
''','''
alter table csv_fk_check_country.clusters	add constraint clusters_country_strata_set_stratum_panel_fkey	foreign key (country, strata_set, stratum, panel) references csv_fk_check_country.panels (country, strata_set, stratum, panel);
    ''', err_f)
    #--plots_pathfinder_ds
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.plots_pathfinder_ds	as select country, strata_set, stratum, panel, cluster, plot from csv_country.plots_pathfinder_ds;
    ''', 'country, strata_set, stratum, panel, cluster, plot', 'plots_pathfinder_ds', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plots_pathfinder_ds', 'csv_fk_check_country.clusters', 'plots_pathfinder_ds.country = clusters.country and plots_pathfinder_ds.strata_set = clusters.strata_set and plots_pathfinder_ds.stratum = clusters.stratum and plots_pathfinder_ds.panel = clusters.panel and plots_pathfinder_ds.cluster = clusters.cluster', 'clusters.cluster is null', '''
alter table csv_fk_check_country.clusters	add constraint clusters_country_strata_set_stratum_panel_cluster_key unique (country, strata_set, stratum, panel, cluster);
''','''
alter table csv_fk_check_country.plots_pathfinder_ds	add constraint plots_country_strata_set_stratum_panel_cluster_fkey	foreign key (country, strata_set, stratum, panel, cluster) references csv_fk_check_country.clusters (country, strata_set, stratum, panel, cluster);
    ''', err_f)
    #--inventory_campaigns
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.inventory_campaigns	as select country, inventory_campaign from csv_country.inventory_campaigns;
    ''', 'country, inventory_campaign', 'inventory_campaigns', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.inventory_campaigns', 'csv_fk_check_country.countries', 'inventory_campaigns.country = countries.label', 'countries.label is null', '','''
alter table csv_fk_check_country.inventory_campaigns		add constraint inventory_campaigns_country_fkey foreign key (country) references csv_fk_check_country.countries (label);
    ''', err_f)
    #--reference_year_sets
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.reference_year_sets 	as select country, inventory_campaign, reference_year_set, inventory_campaign_begin, inventory_campaign_end, reference_year_set_begin, reference_year_set_end from csv_country.reference_year_sets;
    ''', 'country, inventory_campaign, reference_year_set, inventory_campaign_begin, inventory_campaign_end, reference_year_set_begin, reference_year_set_end', 'reference_year_sets', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.reference_year_sets', 'csv_fk_check_country.inventory_campaigns', 'reference_year_sets.country = inventory_campaigns.country and reference_year_sets.inventory_campaign = inventory_campaigns.inventory_campaign', 'inventory_campaigns.inventory_campaign is null', '''
alter table csv_fk_check_country.inventory_campaigns 	add constraint inventory_campaigns_country_inventory_campaign_key unique (country, inventory_campaign);
''','''
alter table csv_fk_check_country.reference_year_sets	add constraint reference_year_sets_country_inventory_campaign_fkey	foreign key (country, inventory_campaign) references csv_fk_check_country.inventory_campaigns (country, inventory_campaign);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.reference_year_sets', 'csv_fk_check_country.inventory_campaigns', 'reference_year_sets.country = inventory_campaigns.country and reference_year_sets.inventory_campaign_begin = inventory_campaigns.inventory_campaign', 'reference_year_sets.inventory_campaign_begin is not null and inventory_campaigns.inventory_campaign is null', '','''
alter table csv_fk_check_country.reference_year_sets	add constraint reference_year_sets_country_inventory_campaign_begin_fkey	foreign key (country, inventory_campaign_begin) references csv_fk_check_country.inventory_campaigns (country, inventory_campaign);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.reference_year_sets', 'csv_fk_check_country.inventory_campaigns', 'reference_year_sets.country = inventory_campaigns.country and reference_year_sets.inventory_campaign_end = inventory_campaigns.inventory_campaign', 'reference_year_sets.inventory_campaign_end is not null and inventory_campaigns.inventory_campaign is null', '','''
alter table csv_fk_check_country.reference_year_sets	add constraint reference_year_sets_country_inventory_campaign_end_fkey	foreign key (country, inventory_campaign_end) references csv_fk_check_country.inventory_campaigns (country, inventory_campaign);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.reference_year_sets', 'csv_fk_check_country.reference_year_sets as reference_year_sets2', 'reference_year_sets.country = reference_year_sets2.country and reference_year_sets.inventory_campaign_begin = reference_year_sets2.inventory_campaign and reference_year_sets.reference_year_set_begin = reference_year_sets2.reference_year_set', 'reference_year_sets.inventory_campaign_begin is not null and reference_year_sets.reference_year_set_begin is not null and reference_year_sets2.reference_year_set is null', '''
alter table csv_fk_check_country.reference_year_sets 	add constraint reference_year_sets_c_ic_rys_key unique (country, inventory_campaign, reference_year_set);
''','''
alter table csv_fk_check_country.reference_year_sets	add constraint reference_year_sets_c_ic_rysb_fkey	foreign key (country, inventory_campaign_begin, reference_year_set_begin) references csv_fk_check_country.reference_year_sets (country, inventory_campaign, reference_year_set);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.reference_year_sets', 'csv_fk_check_country.reference_year_sets as reference_year_sets2', 'reference_year_sets.country = reference_year_sets2.country and reference_year_sets.inventory_campaign_end = reference_year_sets2.inventory_campaign and reference_year_sets.reference_year_set_end = reference_year_sets2.reference_year_set', 'reference_year_sets.inventory_campaign_end is not null and reference_year_sets.reference_year_set_end is not null and reference_year_sets2.reference_year_set is null', '','''
alter table csv_fk_check_country.reference_year_sets	add constraint reference_year_sets_c_ic_ryse_fkey	foreign key (country, inventory_campaign_end, reference_year_set_end) references csv_fk_check_country.reference_year_sets (country, inventory_campaign, reference_year_set);
    ''', err_f)
    #--refyearset2panel
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.refyearset2panel	as select country, inventory_campaign, reference_year_set, strata_set, stratum, panel from csv_country.refyearset2panel;
    ''', 'country, inventory_campaign, reference_year_set, strata_set, stratum, panel', 'refyearset2panel', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.refyearset2panel', 'csv_fk_check_country.reference_year_sets', 'refyearset2panel.country = reference_year_sets.country and refyearset2panel.inventory_campaign = reference_year_sets.inventory_campaign and refyearset2panel.reference_year_set = reference_year_sets.reference_year_set', 'reference_year_sets.reference_year_set is null', '','''
alter table csv_fk_check_country.refyearset2panel	add constraint refyearset2panel_c_ic_rys_fkey	foreign key (country, inventory_campaign, reference_year_set) references csv_fk_check_country.reference_year_sets (country, inventory_campaign, reference_year_set);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.refyearset2panel', 'csv_fk_check_country.panels', 'refyearset2panel.country = panels.country and refyearset2panel.strata_set = panels.strata_set and refyearset2panel.stratum = panels.stratum and refyearset2panel.panel = panels.panel', 'panels.panel is null', '','''
alter table csv_fk_check_country.refyearset2panel	add constraint refyearset2panel_c_sts_s_p_fkey	foreign key (country, strata_set, stratum, panel) references csv_fk_check_country.panels (country, strata_set, stratum, panel);
    ''', err_f)
    #--plot_measurement_dates
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.plot_measurement_dates	as select country, inventory_campaign, reference_year_set, strata_set, stratum, panel, cluster, plot from csv_country.plot_measurement_dates;
    ''', 'country, inventory_campaign, reference_year_set, strata_set, stratum, panel, cluster, plot', 'plot_measurement_dates', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_measurement_dates', 'csv_fk_check_country.reference_year_sets', 'plot_measurement_dates.country = reference_year_sets.country and plot_measurement_dates.inventory_campaign = reference_year_sets.inventory_campaign and plot_measurement_dates.reference_year_set = reference_year_sets.reference_year_set', 'reference_year_sets.reference_year_set is null', '','''
alter table csv_fk_check_country.plot_measurement_dates		add constraint plot_measurement_dates_c_ic_rys_fkey	foreign key (country, inventory_campaign, reference_year_set) references csv_fk_check_country.reference_year_sets (country, inventory_campaign, reference_year_set);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_measurement_dates', 'csv_fk_check_country.plots_pathfinder_ds', 'plot_measurement_dates.country = plots_pathfinder_ds.country and plot_measurement_dates.strata_set = plots_pathfinder_ds.strata_set and plot_measurement_dates.stratum = plots_pathfinder_ds.stratum and plot_measurement_dates.panel = plots_pathfinder_ds.panel and plot_measurement_dates.cluster = plots_pathfinder_ds.cluster and plot_measurement_dates.plot = plots_pathfinder_ds.plot', 'plots_pathfinder_ds.plot is null', '''
alter table csv_fk_check_country.plots_pathfinder_ds	 	add constraint plots_pathfinder_ds_c_sts_s_p_c_p_key unique (country, strata_set, stratum, panel, cluster, plot);
''','''
alter table csv_fk_check_country.plot_measurement_dates		add constraint plot_measurement_dates_c_sts_s_p_c_p_fkey	foreign key (country, strata_set, stratum, panel, cluster, plot) references csv_fk_check_country.plots_pathfinder_ds (country, strata_set, stratum, panel, cluster, plot);
    ''', err_f)
    #--plot_cell_associations
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.plot_cell_associations	as select country, strata_set, stratum, panel, cluster, plot, cell_collection, cell from csv_country.plot_cell_associations;
    ''', 'country, strata_set, stratum, panel, cluster, plot, cell_collection, cell', 'plot_cell_associations', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_cell_associations', 'csv_fk_check_country.plots_pathfinder_ds', 'plot_cell_associations.country = plots_pathfinder_ds.country and plot_cell_associations.strata_set = plots_pathfinder_ds.strata_set and plot_cell_associations.stratum = plots_pathfinder_ds.stratum and plot_cell_associations.panel = plots_pathfinder_ds.panel and plot_cell_associations.cluster = plots_pathfinder_ds.cluster and plot_cell_associations.plot = plots_pathfinder_ds.plot', 'plots_pathfinder_ds.plot is null', '','''
alter table csv_fk_check_country.plot_cell_associations		add constraint plot_cell_associations_c_sts_s_p_c_p_fkey	foreign key (country, strata_set, stratum, panel, cluster, plot) references csv_fk_check_country.plots_pathfinder_ds (country, strata_set, stratum, panel, cluster, plot);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_cell_associations', 'csv_fk_check_codelist.cell_collection_cells', 'plot_cell_associations.cell_collection = cell_collection_cells.cell_collection and plot_cell_associations.cell = cell_collection_cells.cell', 'cell_collection_cells.cell is null', '','''
alter table csv_fk_check_country.plot_cell_associations		add constraint plot_cell_associations_cc_c_fkey		foreign key (cell_collection, cell) references csv_fk_check_codelist.cell_collection_cells (cell_collection, cell);
    ''', err_f)
    #--plot_target_data
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.plot_target_data		as 
with w_data as (
    select 
        country,
        inventory_campaign, reference_year_set,
        strata_set, stratum, panel, cluster, plot, 
        coalesce(target_variable, 'altogether') as target_variable, 
        coalesce(sub_population, 'altogether') as sub_population, 
        coalesce(sub_population_category, 'altogether') as sub_population_category, 
        coalesce(area_domain, 'altogether') as area_domain, 
        coalesce(area_domain_category, 'altogether') as area_domain_category,
        value
    from csv_country.plot_target_data
)
select
    country,
    inventory_campaign, reference_year_set,
    strata_set, stratum, panel, cluster, plot, 
    target_variable,
    array_to_string(array(select unnest(string_to_array(sub_population, '+')) order by 1), '+') as sub_population,
    array_to_string(array(select unnest(string_to_array(sub_population_category, '+')) order by 1), '+') as sub_population_category,
    array_to_string(array(select unnest(string_to_array(area_domain, '+')) order by 1), '+') as area_domain,
    array_to_string(array(select unnest(string_to_array(area_domain_category, '+')) order by 1), '+') as area_domain_category
from w_data
;
    ''', 'country, inventory_campaign, reference_year_set, strata_set, stratum, panel, cluster, plot, target_variable, sub_population, sub_population_category, area_domain, area_domain_category', 'plot_target_data', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_target_data', 'csv_fk_check_country.reference_year_sets', 'plot_target_data.country = reference_year_sets.country and plot_target_data.inventory_campaign = reference_year_sets.inventory_campaign and plot_target_data.reference_year_set = reference_year_sets.reference_year_set', 'reference_year_sets.reference_year_set is null', '','''
alter table csv_fk_check_country.plot_target_data		add constraint plot_target_data_c_ic_rys_fkey	foreign key (country, inventory_campaign, reference_year_set) references csv_fk_check_country.reference_year_sets (country, inventory_campaign, reference_year_set);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_target_data', 'csv_fk_check_country.plots_pathfinder_ds', 'plot_target_data.country = plots_pathfinder_ds.country and plot_target_data.strata_set = plots_pathfinder_ds.strata_set and plot_target_data.stratum = plots_pathfinder_ds.stratum and plot_target_data.panel = plots_pathfinder_ds.panel and plot_target_data.cluster = plots_pathfinder_ds.cluster and plot_target_data.plot = plots_pathfinder_ds.plot', 'plots_pathfinder_ds.plot is null', '','''
alter table csv_fk_check_country.plot_target_data		add constraint plot_target_data_c_sts_s_p_c_p_fkey	foreign key (country, strata_set, stratum, panel, cluster, plot) references csv_fk_check_country.plots_pathfinder_ds (country, strata_set, stratum, panel, cluster, plot);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_target_data', 'csv_fk_check_codelist.variable_target', 'plot_target_data.target_variable = variable_target.target_variable and plot_target_data.sub_population = variable_target.sub_population and plot_target_data.sub_population_category = variable_target.sub_population_category and plot_target_data.area_domain = variable_target.area_domain and plot_target_data.area_domain_category = variable_target.area_domain_category', 'variable_target.target_variable is null', '','''
alter table csv_fk_check_country.plot_target_data		add constraint plot_target_data_tv_sp_spc_ad_adc_fkey	foreign key (target_variable, sub_population, sub_population_category, area_domain, area_domain_category) references csv_fk_check_codelist.variable_target (target_variable, sub_population, sub_population_category, area_domain, area_domain_category);
    ''', err_f)
    #--plot_target_data_availability
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.plot_target_data_availability		as 
with w_data as (
    select 
        country,
        inventory_campaign, reference_year_set,
        strata_set, stratum, panel, 
        coalesce(target_variable, 'altogether') as target_variable, 
        coalesce(sub_population, 'altogether') as sub_population, 
        coalesce(sub_population_category, 'altogether') as sub_population_category, 
        coalesce(area_domain, 'altogether') as area_domain, 
        coalesce(area_domain_category, 'altogether') as area_domain_category
    from csv_country.plot_target_data_availability
)
select
    country,
    inventory_campaign, reference_year_set,
    strata_set, stratum, panel,
    target_variable,
    array_to_string(array(select unnest(string_to_array(sub_population, '+')) order by 1), '+') as sub_population,
    array_to_string(array(select unnest(string_to_array(sub_population_category, '+')) order by 1), '+') as sub_population_category,
    array_to_string(array(select unnest(string_to_array(area_domain, '+')) order by 1), '+') as area_domain,
    array_to_string(array(select unnest(string_to_array(area_domain_category, '+')) order by 1), '+') as area_domain_category
from w_data
;
    ''', 'country, inventory_campaign, reference_year_set, strata_set, stratum, panel, target_variable, sub_population, sub_population_category, area_domain, area_domain_category', 'plot_target_data_availability', err_f)
#    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_target_data_availability', 'csv_fk_check_country.reference_year_sets', 'plot_target_data_availability.country = reference_year_sets.country and plot_target_data_availability.inventory_campaign = reference_year_sets.inventory_campaign and plot_target_data_availability.reference_year_set = reference_year_sets.reference_year_set', 'reference_year_sets.reference_year_set is null', '','''
#alter table csv_fk_check_country.plot_target_data_availability		add constraint plot_target_data_availability_c_ic_rys_fkey	foreign key (country, inventory_campaign, reference_year_set) references csv_fk_check_country.reference_year_sets (country, inventory_campaign, reference_year_set);
#    ''', err_f)
#    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_target_data_availability', 'csv_fk_check_country.panels', 'plot_target_data_availability.country = panels.country and plot_target_data_availability.strata_set = panels.strata_set and plot_target_data_availability.stratum = panels.stratum and plot_target_data_availability.panel = panels.panel', 'panels.panel is null', '','''
#alter table csv_fk_check_country.plot_target_data_availability		add constraint plot_target_data_availability_c_sts_s_p_fkey	foreign key (country, strata_set, stratum, panel) references csv_fk_check_country.panels (country, strata_set, stratum, panel);
#    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_target_data_availability', 'csv_fk_check_codelist.variable_target', 'plot_target_data_availability.target_variable = variable_target.target_variable and plot_target_data_availability.sub_population = variable_target.sub_population and plot_target_data_availability.area_domain = variable_target.area_domain', 'variable_target.target_variable is null', '','''
alter table csv_fk_check_country.plot_target_data_availability		add constraint plot_target_data_availability_tv_sp_ad_fkey	foreign key (target_variable, sub_population, area_domain) references csv_fk_check_codelist.variable_target (target_variable, sub_population, area_domain);
    ''', err_f)
    #--plot_auxiliary_data
    number_of_unique_exceptions += check_unique(conn, '''
create table csv_fk_check_country.plot_auxiliary_data		as 
select 
    country, strata_set, stratum, panel, cluster, plot, tile, 
    coalesce(auxiliary_variable, 'altogether') as auxiliary_variable, 
    coalesce(auxiliary_variable_category, 'altogether')  as auxiliary_variable_category
from csv_country.plot_auxiliary_data
where auxiliary_variable in ('tcd', 'fty', 'gfc', 'coordinate_quality') and auxiliary_variable_category not in ('n.pixels','sum.weights','n.pixels.all','sum.weights.all');
    ''', 'country, strata_set, stratum, panel, cluster, plot, auxiliary_variable, auxiliary_variable_category, tile', 'plot_auxiliary_data', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_auxiliary_data', 'csv_fk_check_country.plots_pathfinder_ds', 'plot_auxiliary_data.country = plots_pathfinder_ds.country and plot_auxiliary_data.strata_set = plots_pathfinder_ds.strata_set and plot_auxiliary_data.stratum = plots_pathfinder_ds.stratum and plot_auxiliary_data.panel = plots_pathfinder_ds.panel and plot_auxiliary_data.cluster = plots_pathfinder_ds.cluster and plot_auxiliary_data.plot = plots_pathfinder_ds.plot', 'plots_pathfinder_ds.plot is null', '','''
alter table csv_fk_check_country.plot_auxiliary_data		add constraint plot_auxiliary_data_c_sts_s_p_c_p_fkey	foreign key (country, strata_set, stratum, panel, cluster, plot) references csv_fk_check_country.plots_pathfinder_ds (country, strata_set, stratum, panel, cluster, plot);
    ''', err_f)
    number_of_fkey_exceptions += check_fkey(conn, 'csv_fk_check_country.plot_auxiliary_data', 'csv_fk_check_codelist.variable_aux', 'plot_auxiliary_data.auxiliary_variable = variable_aux.auxiliary_variable and plot_auxiliary_data.auxiliary_variable_category = variable_aux.auxiliary_variable_category', 'variable_aux.auxiliary_variable_category is null', '','''
alter table csv_fk_check_country.plot_auxiliary_data		add constraint plot_auxiliary_data_av_avc_fkey	foreign key (auxiliary_variable, auxiliary_variable_category) references csv_fk_check_codelist.variable_aux (auxiliary_variable, auxiliary_variable_category);
    ''', err_f)
    conn.commit()
    err_f.close()
    pf_print('number of unique exceptions: %s' % number_of_unique_exceptions, log_f)
    pf_print('number of fkey exceptions: %s' % number_of_fkey_exceptions, log_f)
    if number_of_unique_exceptions == 0 and number_of_fkey_exceptions == 0:
        os.remove(err_log_name)
        dir_name = os.path.join('countries', country_r, 'log')
        log_dir = os.listdir(dir_name)
        for item in log_dir:
            if item.endswith('.csv'):
                os.remove(os.path.join(dir_name, item))
    else:
        pf_print('For more info on FKEYs errors see %s' % err_log_name, log_f)
        sys.exit(1)
