DROP SCHEMA IF EXISTS csv_server CASCADE;
CREATE SCHEMA csv_server;
grant usage on schema csv_server to public;

\set afile `pwd` '/server_side/countries.csv'
create foreign table csv_server.countries (
	id		integer			not null,
	label		character varying(20)	not null,
	description	character varying(20)	not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

\set afile `pwd` '/server_side/cell_collections.csv'
create foreign table csv_server.cell_collections (
	id		integer			not null,
	label		character varying(20)	not null,
	description	character varying(100)	not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

\set afile `pwd` '/server_side/target_variable_types.csv'
create foreign table csv_server.target_variable_types (
	id		integer			not null,
	label		character varying(20)	not null,
	description	character varying(500)	not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

--broad definition / harmonisation limits;subpopulations / area domains
\set afile `pwd` '/server_side/target_variables.csv'
create foreign table csv_server.target_variables (
	id		integer			not null,
	label		character varying(50)	not null,
	variable_type	character varying(20)   not null,
	unit_of_measure	character varying(50)   not null,
	broad_def	text			not null,
	harm_limits	text			not null,
	sub_populations text   			not null,
	area_domains	text   			not null,
	metadata	json			not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

\set afile `pwd` '/server_side/sub_populations.csv'
create foreign table csv_server.sub_populations (
	id		integer			not null,
	label		character varying(50)	not null,
	description	text			not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

\set afile `pwd` '/server_side/sub_population_categories.csv'
create foreign table csv_server.sub_population_categories (
	id		integer			not null,
	sub_population	character varying(50)	not null,
	label		character varying(50)	not null,
	description	text			not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

\set afile `pwd` '/server_side/area_domains.csv'
create foreign table csv_server.area_domains (
	id		integer			not null,
	label		character varying(50)	not null,
	description	text			not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

\set afile `pwd` '/server_side/area_domain_categories.csv'
create foreign table csv_server.area_domain_categories (
	id		integer			not null,
	area_domain	character varying(50)	not null,
	label		character varying(120)	not null,
	description	text			not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );
	
\set afile `pwd` '/server_side/auxiliary_variables.csv'
create foreign table csv_server.auxiliary_variables (
	id		integer			not null,
	label		character varying(20)	not null,
	variable_type	character varying(20)   not null,
	description	text			not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );

\set afile `pwd` '/server_side/auxiliary_variable_categories.csv'
create foreign table csv_server.auxiliary_variable_categories (
	id			integer			not null,
	auxiliary_variable	character varying(20)	not null,
	label			character varying(20)   not null,
	description	text			not null
) server csv_files
options ( header 'true', format 'csv', delimiter ';', filename :'afile' );
-------------------------------------------------------------

alter role adm_nfiesta with nosuperuser;
