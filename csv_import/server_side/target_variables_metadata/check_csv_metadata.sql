--run from bash with duckdb < check_csv_metadata.sql

with csv_fixed as (
  select
    label,
    metadata::json as metadata
  from '../target_variables.csv'
  --where label = 'agb'
)
, json_file as (
select
  regexp_replace(filename, '\.json', '') as filename_pf_label,
  en->>'pf_label' as pf_label,
  json({'cs': cs, 'en': en}) as metadata
from read_json('*.json', filename=true)
)
select
  label,
  json_file.filename_pf_label = json_file.pf_label as filename_OK,
  csv_fixed.metadata = json_file.metadata as metadata_OK,
  --csv_fixed.metadata as csv_metadata,
  --json_file.metadata as json_metadata
from csv_fixed join json_file on (csv_fixed.label = json_file.filename_pf_label);
