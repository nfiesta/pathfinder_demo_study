#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from datetime import datetime
import duckdb

duckdb.install_extension('postgres')
duckdb.load_extension("postgres")
duckdb.sql('''ATTACH 'dbname=pathfinder_ds host=/var/run/postgresql user=vagrant' AS pgdb (TYPE postgres);''')

countries = duckdb.sql('''select description from pgdb.sdesign.c_country /*where label = 'CH'*/;''').fetchall()
duckdb.sql('''DETACH pgdb;''')

def check_change_status(plot_target_data_fn, reference_year_sets_fn):
    plot_target_data=duckdb.read_csv(plot_target_data_fn, delimiter = ';', header = True, all_varchar = True)
    reference_year_sets=duckdb.read_csv(reference_year_sets_fn, delimiter = ';', header = True, all_varchar = True)
    res = duckdb.sql('''
    with w_data as (
        select 
            country, 
            inventory_campaign, reference_year_set, 
            reference_year_sets.label, reference_year_sets.status_variables, 
            reference_year_sets.reference_date_begin, reference_year_sets.reference_date_end,
            reference_year_sets.inventory_campaign_begin, reference_year_sets.reference_year_set_begin, 
            reference_year_sets.inventory_campaign_end, reference_year_sets.reference_year_set_end,
            plot_target_data.strata_set, plot_target_data.stratum, plot_target_data.panel,
            plot_target_data.target_variable,/*
            plot_target_data.sub_population, plot_target_data.sub_population_category,
            plot_target_data.area_domain, plot_target_data.area_domain_category,*/
            count(*) as local_densities_count
        from plot_target_data 
        join reference_year_sets using (country, inventory_campaign, reference_year_set)
        group by all
    )
    , w_status as (
        select
            country, inventory_campaign, reference_year_set,
            reference_date_begin, reference_date_end, 
            strata_set, stratum, panel,
            target_variable, /*sub_population, sub_population_category, area_domain, area_domain_category,*/
            local_densities_count
        from w_data
        where status_variables
    )
    , w_change as (
        select
            country, inventory_campaign, reference_year_set,
            inventory_campaign_begin, reference_year_set_begin, 
            inventory_campaign_end, reference_year_set_end,
            strata_set, stratum, panel,
            target_variable, /*sub_population, sub_population_category, area_domain, area_domain_category,*/
            local_densities_count
        from w_data
        where not status_variables
    )
    select 
        w_change.country, w_change.inventory_campaign, w_change.reference_year_set,
        w_change.strata_set, w_change.stratum, w_change.panel,
        w_change.target_variable, /*w_change.sub_population, w_change.sub_population_category,*/
        w_change.local_densities_count,
        w_status_begin.target_variable as status_begin_target_variable,
        w_status_begin.local_densities_count as status_begin_local_densities_count,
        w_status_end.target_variable as status_end_target_variable,
        w_status_end.local_densities_count as status_end_local_densities_count
    from w_change
    left join w_status as w_status_begin on (
                                            w_change.country = w_status_begin.country and
                                            w_change.inventory_campaign_begin = w_status_begin.inventory_campaign and
                                            w_change.reference_year_set_begin = w_status_begin.reference_year_set and
                                            w_change.strata_set = w_status_begin.strata_set and
                                            w_change.stratum = w_status_begin.stratum and
                                            w_change.panel = w_status_begin.panel and
                                            regexp_replace(w_change.target_variable, '_change', '') = w_status_begin.target_variable /*and
                                            w_change.sub_population = w_status_begin.sub_population and
                                            w_change.sub_population_category = w_status_begin.sub_population_category and
                                            w_change.area_domain = w_status_begin.area_domain and
                                            w_change.area_domain_category = w_status_begin.area_domain_category*/
                                            )
    left join w_status as w_status_end on (
                                            w_change.country = w_status_end.country and
                                            w_change.inventory_campaign_end = w_status_end.inventory_campaign and
                                            w_change.reference_year_set_end = w_status_end.reference_year_set and
                                            w_change.strata_set = w_status_end.strata_set and
                                            w_change.stratum = w_status_end.stratum and
                                            w_change.panel = w_status_end.panel and
                                            regexp_replace(w_change.target_variable, '_change', '') = w_status_end.target_variable
                                            )
    where (w_status_begin.local_densities_count is null) or (w_status_end.local_densities_count is null)
    ;
    ''').show()

def check_forest_plots(plot_target_data_filename, plots_filename):
    plot_target_data=duckdb.read_csv(plot_target_data_filename, delimiter = ';', header = True, all_varchar = True)
    plots=duckdb.read_csv(plots_filename, delimiter = ';', header = True, all_varchar = True)
    #res = duckdb.sql('''with w_data as(
    #    select 
    #        country, strata_set, stratum, panel, cluster, plot, 
    #        coalesce(target_variable, 'not found') as target_variable, coalesce(value, 'not found') as value
    #    from plots
    #    left join (select * from plot_target_data where target_variable = 'forest') as plot_target_data using (country, strata_set, stratum, panel, cluster, plot)
    #    )
    #    select target_variable, (select count(*) from plots) as plots_total, value, count(*) from w_data group by target_variable, value
    #    ;''').show()
    res = duckdb.sql('''with w_data as(
        select
            country, strata_set, stratum, panel, cluster, plot,
            reference_year_set,
            coalesce(target_variable, 'not found') as target_variable, coalesce(value, 'not found') as value
        from plots
        left join (select * from plot_target_data where target_variable = 'forest') as plot_target_data using (country, strata_set, stratum, panel, cluster, plot)
        )
        select
            country, strata_set, stratum, panel, reference_year_set, target_variable, value, count(*)
            from w_data
            group by country, strata_set, stratum, panel, reference_year_set, target_variable, value
            order by country, strata_set, stratum, panel, reference_year_set, target_variable, value
        ;''').show()

for c in countries:
    print(c[0])
    path = os.path.join('/home/vagrant', 'pathfinder_demo_study/csv_import', 'countries', c[0])
    plot_target_data_fn = os.path.join(path, 'plot_target_data.csv')
    reference_year_sets_fn = os.path.join(path, 'reference_year_sets.csv')
    plots_filename = os.path.join(path, 'plots.csv')
    if (os.path.isfile(plot_target_data_fn) and os.path.isfile(reference_year_sets_fn) and os.path.isfile(plots_filename)):
        print("%s call fn" % datetime.now().strftime("%Y-%b-%d %H:%M:%S.%f"))
        #check_change_status(plot_target_data_fn, reference_year_sets_fn)
        check_forest_plots(plot_target_data_fn, plots_filename)
        print("%s call fn finished" % datetime.now().strftime("%Y-%b-%d %H:%M:%S.%f"))
    else:
        print('files do not exist')
