------------------------------------------------------------------------------------------------------------------------------------
-- target variables dwb_pathfinder, source: production nfiesta, DB nfi_esta, server bran-pga 
------------------------------------------------------------------------------------------------------------------------------------
/*

SELECT array_to_string(array_agg(subpop ORDER BY i DESC),'+') FROM  unnest(regexp_split_to_array('deadwood type+diameter class', '\+')) WITH ORDINALITY AS a(subpop, i)

SELECT * FROM nfiesta.c_area_domain ORDER BY label;

SELECT * FROM nfiesta.c_sub_population ORDER BY label;

SELECT 
	t1.id,
	t1.metadata #> '{en, indicator, label}' AS indicator,
	t1.metadata #> '{en, local densities, 0, version, label}' AS version
FROM 
	nfiesta.c_target_variable   AS t1
WHERE
	t1.metadata #>> '{en, indicator, label}' like '%dead wood forms%' AND t1.metadata #>> '{en, state or change, label}' = 'state variable';	
*/



-- NFI3 dwb_pathfinder, 445 185
SELECT
	 -- count(*) /*
	'CZ' AS country,
	'CZ-NFI3(2016-2020)' AS inventory_campaign,
	t3.reference_year_set AS reference_year_set,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	t4.panel,
	t5.plot AS cluster,
	t5.plot AS plot, 
	'dwb_pathfinder' AS target_variable,
	CASE 
		WHEN t7.sub_population = 130 THEN 'deadwood type+diameter class'
		WHEN t7.sub_population = 128 THEN 'coniferous or broadleaved+deadwood type'
		WHEN t7.sub_population = 127 THEN 'coniferous or broadleaved+diameter class'
		WHEN t7.sub_population = 122 THEN 'deadwood type'
		WHEN t7.sub_population = 44 THEN 'coniferous or broadleaved'
		WHEN t7.sub_population = 131 THEN 'diameter class'
		WHEN t7.sub_population IS NULL THEN 'altogether'
		ELSE 'error'
	END AS sub_population,
		CASE WHEN t7.sub_population = 128 THEN 
		(SELECT array_to_string(array_agg(subpop ORDER BY i DESC),'+') FROM  unnest(regexp_split_to_array(t7.label_en, '\;')) WITH ORDINALITY AS a(subpop, i))
	ELSE
			coalesce(replace(t7.label_en, ';', '+'), 'altogether')
	END AS sub_population_category,
	CASE 
		WHEN t8.area_domain = 111 THEN 'L1 forest type' 
		WHEN t8.area_domain IS NULL THEN 'altogether'
		ELSE 'error' 
	END AS area_domain,
	coalesce(t8.label_en, 'altogether') AS area_domain_category,
	t1.value AS value,
	NULL AS comment -- */
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id AND t1.is_latest
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id AND t3.inventory_campaign = 3
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid  -- (SELECT * FROM sdesign.f_p_plot WHERE gid/100.0 = round(gid/100.0)) AS t5 ON t1.plot = t5.gid -- selection of 1 out of 100 records
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id AND t6.target_variable = 121 AND 
	(t6.sub_population_category IS NULL OR t6.sub_population_category IN (SELECT id FROM nfiesta.c_sub_population_category WHERE sub_population IN (130,128,127,122,44,131))) AND 
	(t6.area_domain_category IS NULL OR t6.area_domain_category IN (SELECT id FROM nfiesta.c_area_domain_category WHERE area_domain IN (111)))
LEFT JOIN nfiesta.c_sub_population_category AS t7 ON t6.sub_population_category = t7.id
LEFT JOIN nfiesta.c_area_domain_category AS t8 ON t6.area_domain_category = t8.id


UNION ALL

-- NFI2 dwb_pathfinder, 228 494
SELECT
	 -- count(*) /*
	'CZ' AS country,
	'CZ-NFI2(2011-2015)' AS inventory_campaign,
	t3.reference_year_set AS reference_year_set,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	t4.panel,
	t5.plot AS cluster,
	t5.plot AS plot, 
	'dwb_pathfinder' AS target_variable,
	CASE 
		WHEN t7.sub_population = 130 THEN 'deadwood type+diameter class'
		WHEN t7.sub_population = 128 THEN 'coniferous or broadleaved+deadwood type'
		WHEN t7.sub_population = 127 THEN 'coniferous or broadleaved+diameter class'
		WHEN t7.sub_population = 122 THEN 'deadwood type'
		WHEN t7.sub_population = 44 THEN 'coniferous or broadleaved'
		WHEN t7.sub_population = 131 THEN 'diameter class'
		WHEN t7.sub_population IS NULL THEN 'altogether'
		ELSE 'error'
	END AS sub_population,
	CASE WHEN t7.sub_population = 128 THEN 
		(SELECT array_to_string(array_agg(subpop ORDER BY i DESC),'+') FROM  unnest(regexp_split_to_array(t7.label_en, '\;')) WITH ORDINALITY AS a(subpop, i))
	ELSE
			coalesce(replace(t7.label_en, ';', '+'), 'altogether')
	END AS sub_population_category,
	CASE 
		WHEN t8.area_domain = 111 THEN 'L1 forest type' 
		WHEN t8.area_domain IS NULL THEN 'altogether'
		ELSE 'error'
	END AS area_domain,
	coalesce(t8.label_en, 'altogether') AS area_domain_category,
	t1.value AS value,
	NULL AS comment -- */
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id AND t1.is_latest
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id AND t3.inventory_campaign = 2
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid  -- (SELECT * FROM sdesign.f_p_plot WHERE gid/100.0 = round(gid/100.0)) AS t5 ON t1.plot = t5.gid -- selection of 1 out of 100 records
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id AND t6.target_variable = 122 AND 
	(t6.sub_population_category IS NULL OR t6.sub_population_category IN (SELECT id FROM nfiesta.c_sub_population_category WHERE sub_population IN (130,128,127,122,44,131))) AND 
	(t6.area_domain_category IS NULL OR t6.area_domain_category IN (SELECT id FROM nfiesta.c_area_domain_category WHERE area_domain IN (111)))
LEFT JOIN nfiesta.c_sub_population_category AS t7 ON t6.sub_population_category = t7.id
LEFT JOIN nfiesta.c_area_domain_category AS t8 ON t6.area_domain_category = t8.id
