------------------------------------------------------------------------------------------------------------------------------------
-- target variable forest, source: production nfiesta, DB nfi_esta, server bran-pga 
------------------------------------------------------------------------------------------------------------------------------------
/*
SELECT * FROM nfiesta.c_area_domain ORDER BY label 

SELECT 
	t1.id,
	t1.metadata #> '{cs, local densities, 0, version, label}' AS version
FROM 
	nfiesta.c_target_variable   AS t1
WHERE 
	t1.metadata #>> '{cs, indicator, label}' = 'plocha lesa' AND t1.metadata #>> '{cs, state or change, label}' = 'state variable'	
*/
	
-- NFI3 forest, target_variable.id = 13 (no backward correction)
SELECT -- 29 212 records with forest = 1
	'CZ' AS country,
	'CZ-NFI3(2016-2020)' AS inventory_campaign,
	t3.reference_year_set AS reference_year_set,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	t4.panel,
	t5.plot AS cluster,
	t5.plot AS plot, 
	'forest' AS target_variable,
	CASE
		WHEN t7.sub_population IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population,
	CASE
		WHEN t7.label_en IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population_category,
	CASE 
		WHEN t8.area_domain = 110 THEN 'L1 forest type' 
		WHEN t8.area_domain IS NULL THEN 'altogether'
		ELSE 'error'
	END AS area_domain,
	coalesce(t8.label_en, 'altogether') AS area_domain_category,
	t1.value AS value,
	NULL AS comment
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id AND t1.is_latest
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id AND t3.inventory_campaign = 3
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid  -- (SELECT * FROM sdesign.f_p_plot WHERE gid/100.0 = round(gid/100.0)) AS t5 ON t1.plot = t5.gid -- selection of 1 out of 100 records
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id AND t6.target_variable = 13 AND 
	(t6.sub_population_category IS NULL OR t6.sub_population_category IN (SELECT id FROM nfiesta.c_sub_population_category WHERE sub_population IN (-1))) AND 
	(t6.area_domain_category IS NULL OR t6.area_domain_category IN (SELECT id FROM nfiesta.c_area_domain_category WHERE area_domain IN (110)))
LEFT JOIN nfiesta.c_sub_population_category AS t7 ON t6.sub_population_category = t7.id
LEFT JOIN nfiesta.c_area_domain_category AS t8 ON t6.area_domain_category = t8.id

UNION ALL 

-- NFI2 forest, target_variable.id = 14 (backward correction)
SELECT -- 14 472 records with forest = 1
	'CZ' AS country,
	'CZ-NFI2(2011-2015)' AS inventory_campaign,
	t3.reference_year_set AS reference_year_set,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	t4.panel,
	t5.plot AS cluster,
	t5.plot AS plot, 
	'forest' AS target_variable,
	CASE
		WHEN t7.sub_population IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population,
	CASE
		WHEN t7.label_en IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population_category,
	CASE 
		WHEN t8.area_domain = 110 THEN 'L1 forest type' 
		WHEN t8.area_domain IS NULL THEN 'altogether'
		ELSE 'error'
	END AS area_domain,
	coalesce(t8.label_en, 'altogether') AS area_domain_category,
	t1.value AS value,
	NULL AS comment
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id AND t1.is_latest
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id AND t3.inventory_campaign = 2
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid  -- (SELECT * FROM sdesign.f_p_plot WHERE gid/100.0 = round(gid/100.0)) AS t5 ON t1.plot = t5.gid -- selection of 1 out of 100 records
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id AND t6.target_variable = 14 AND 
	(t6.sub_population_category IS NULL OR t6.sub_population_category IN (SELECT id FROM nfiesta.c_sub_population_category WHERE sub_population IN (-1))) AND 
	(t6.area_domain_category IS NULL OR t6.area_domain_category IN (SELECT id FROM nfiesta.c_area_domain_category WHERE area_domain IN (110)))
LEFT JOIN nfiesta.c_sub_population_category AS t7 ON t6.sub_population_category = t7.id
LEFT JOIN nfiesta.c_area_domain_category AS t8 ON t6.area_domain_category = t8.id