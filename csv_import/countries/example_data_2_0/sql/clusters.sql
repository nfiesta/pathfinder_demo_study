WITH w_clusters AS (
SELECT DISTINCT ON (id_duplex) 
	id_duplex,
	myear,
	mseason
FROM 
gisdata_internal.f_p_nfi1_grid
)
SELECT
	'CZ' AS country,
	'CZ-STS-SNIL1' AS strata_set,
	'CZ-SNIL1' AS stratum,
	concat('y', myear - 2010,'_s',mseason) AS panel,
	concat('duplex_',id_duplex) AS cluster,
	round(7892000.0::numeric/(count(*) OVER (PARTITION BY myear, mseason))::numeric,5) AS sampling_weight_ha,
	NULL AS comment
FROM 
w_clusters

UNION ALL

SELECT 
	'CZ' AS country,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	concat('y', myear,'_s',mseason,'_s2', CASE WHEN s2a THEN 'a' WHEN s2b THEN 'b' END) AS panel,
	concat('pid_',pid) AS cluster,
	round(7886683.5::numeric/(count(*) OVER (PARTITION BY myear, mseason, s2a))::numeric,5) AS sampling_weight_ha,
	NULL AS comment
FROM 
	gisdata_internal.f_p_nfi2_grid 
WHERE s2;