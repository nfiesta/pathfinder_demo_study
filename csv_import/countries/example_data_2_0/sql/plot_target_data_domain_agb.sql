------------------------------------------------------------------------------------------------------------------------------------
-- target variable domain_agb, source: production nfiesta, DB nfi_esta, server bran-pga 
------------------------------------------------------------------------------------------------------------------------------------
/*
SELECT * FROM nfiesta.c_area_domain ORDER BY label;

SELECT * FROM nfiesta.c_sub_population ORDER BY label;

SELECT 
	t1.id,
	t1.metadata #> '{cs, indicator, label}' AS indicator,
	t1.metadata #> '{cs, local densities, 0, version, label}' AS version
FROM 
	nfiesta.c_target_variable   AS t1
WHERE
	t1.metadata #>> '{cs, indicator, label}' like '%nadzemní biomasa%' AND t1.metadata #>> '{cs, state or change, label}' = 'state variable';	
*/

-- NFI3 domain_agb data
SELECT	-- 28 474 records for domain_agb, twice the number of plots in the domain
	'CZ' AS country,
	'CZ-NFI3(2016-2020)' AS inventory_campaign,
	t3.reference_year_set AS reference_year_set,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	t4.panel,
	t5.plot AS cluster,
	t5.plot AS plot, 
	'domain_agb' AS target_variable,
	CASE
		WHEN t7.sub_population IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population,
	CASE
		WHEN t7.label_en IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population_category,
	CASE 
		WHEN t8.area_domain = 110 THEN 'L1 forest type' 
		WHEN t8.area_domain IS NULL THEN 'altogether'
		ELSE 'error'
	END AS area_domain,
	coalesce(t8.label_en, 'altogether') AS area_domain_category,
	t1.value AS value,
	NULL AS comment
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id AND t1.is_latest
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id AND t3.inventory_campaign = 3
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid  -- (SELECT * FROM sdesign.f_p_plot WHERE gid/100.0 = round(gid/100.0)) AS t5 ON t1.plot = t5.gid -- selection of 1 out of 100 records
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id AND t6.target_variable = 18 AND 
	(t6.sub_population_category IS NULL OR t6.sub_population_category IN (SELECT id FROM nfiesta.c_sub_population_category WHERE sub_population IN (-1))) AND 
	(t6.area_domain_category IS NULL OR t6.area_domain_category IN (SELECT id FROM nfiesta.c_area_domain_category WHERE area_domain IN (110)))
LEFT JOIN nfiesta.c_sub_population_category AS t7 ON t6.sub_population_category = t7.id
LEFT JOIN nfiesta.c_area_domain_category AS t8 ON t6.area_domain_category = t8.id

/* check agb per 1 ha, period NFI3 (2016-2020)
SELECT	 -- 3123440.883475185
	sum(t1.value)
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid 
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id
INNER JOIN nfiesta.c_target_variable AS t7 ON t6.target_variable = t7.id
LEFT JOIN nfiesta.c_sub_population_category AS t8 ON t6.sub_population_category = t8.id 
LEFT JOIN nfiesta.c_sub_population AS t9 ON t8.sub_population = t9.id
LEFT JOIN nfiesta.c_area_domain_category AS t10 ON t6.area_domain_category = t10.id 
LEFT JOIN nfiesta.c_area_domain AS t11 ON t10.area_domain = t11.id
WHERE  t1.is_latest AND t3.inventory_campaign = 3 AND t7.id = 20 AND t9.id IS NULL AND t11.id IS NULL;

-- per ha agb: 3123440.883475185 / 14 237 = 219.389 t / ha
-- SELECT 3123440.883475185 / 14237.0 

*/

UNION ALL 

-- NFI2 domain_agb data
SELECT	-- 14 168 records for domain_agb, twice the number of plots in the domain
	'CZ' AS country,
	'CZ-NFI2(2011-2015)' AS inventory_campaign,
	t3.reference_year_set AS reference_year_set,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	t4.panel,
	t5.plot AS cluster,
	t5.plot AS plot, 
	'domain_agb' AS target_variable,
		CASE
		WHEN t7.sub_population IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population,
	CASE
		WHEN t7.label_en IS NULL THEN  'altogether'
		ELSE 'error'
	END AS sub_population_category,
	CASE 
		WHEN t8.area_domain = 110 THEN 'L1 forest type' 
		WHEN t8.area_domain IS NULL THEN 'altogether'
		ELSE 'error'
	END AS area_domain,
	coalesce(t8.label_en, 'altogether') AS area_domain_category,
	t1.value AS value,
	NULL AS comment
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id AND t1.is_latest
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id AND t3.inventory_campaign = 2
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid  -- (SELECT * FROM sdesign.f_p_plot WHERE gid/100.0 = round(gid/100.0)) AS t5 ON t1.plot = t5.gid -- selection of 1 out of 100 records
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id AND t6.target_variable = 89 AND 
	(t6.sub_population_category IS NULL OR t6.sub_population_category IN (SELECT id FROM nfiesta.c_sub_population_category WHERE sub_population IN (-1))) AND 
	(t6.area_domain_category IS NULL OR t6.area_domain_category IN (SELECT id FROM nfiesta.c_area_domain_category WHERE area_domain IN (110)))
LEFT JOIN nfiesta.c_sub_population_category AS t7 ON t6.sub_population_category = t7.id
LEFT JOIN nfiesta.c_area_domain_category AS t8 ON t6.area_domain_category = t8.id

/* kontrola agb na ha v obdobi NIL2
SELECT	 -- 1548881.687536089 (previously 1547260.5575457606)
	sum(t1.value)
FROM nfiesta.t_target_data AS t1
INNER JOIN nfiesta.t_available_datasets AS t2 ON  t1.available_datasets = t2.id
INNER JOIN sdesign.t_reference_year_set AS t3 ON t2.reference_year_set = t3.id
INNER JOIN sdesign.t_panel AS t4 ON t2.panel = t4.id
INNER JOIN sdesign.f_p_plot AS t5 ON t1.plot = t5.gid 
INNER JOIN nfiesta.t_variable AS t6 ON t2.variable = t6.id
INNER JOIN nfiesta.c_target_variable AS t7 ON t6.target_variable = t7.id
LEFT JOIN nfiesta.c_sub_population_category AS t8 ON t6.sub_population_category = t8.id 
LEFT JOIN nfiesta.c_sub_population AS t9 ON t8.sub_population = t9.id
LEFT JOIN nfiesta.c_area_domain_category AS t10 ON t6.area_domain_category = t10.id 
LEFT JOIN nfiesta.c_area_domain AS t11 ON t10.area_domain = t11.id
WHERE  t1.is_latest AND t3.inventory_campaign = 2 AND t7.id = 68 AND t9.id IS NULL AND t11.id IS NULL;

-- per ha agb: 1548881.687536089 / 7084.0 = 218.6450716454106437 t / ha
-- SELECT 1548881.687536089 / 7084.0
*/