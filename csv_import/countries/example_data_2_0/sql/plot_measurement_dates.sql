SELECT -- NFI1 grid, NFI1 cycle
	'CZ' AS country,
	'CZ-NFI1(2001-2004)' AS inventory_campaign,
	'2001-2004' AS reference_year_set,
	'CZ-STS-SNIL1' AS strata_set,
	'CZ-SNIL1' AS stratum,
	concat('y', t1.myear - 2010,'_s',t1.mseason) AS panel,
	concat('duplex_',t1.id_duplex) AS cluster,
	concat('pid_', t1.pid) AS plot,
	to_char(t2.measurement_date, 'YYYY-MON-DD') AS measurement_date,
	'date of field survey for forest plots, date of pre-assessment of land category for other plots' AS comment
FROM 
	gisdata_internal.f_p_nfi1_grid AS t1
INNER JOIN
	analytical.t_plots_field_data AS t2
ON
	t1.pid = t2.pid AND 
	t2.is_latest AND 
	t2.nfi_grid = 100 AND 
	t2.nfi_cycle = 100 AND 
	t2.version = 200 -- 39 460 plots
WHERE t1.nuts4 IS NOT NULL AND t2.measurement_date IS NOT NULL -- 39 431, excluding one plot with unknown measurement/pre-assessment date 

UNION 

SELECT -- NFI1 grid, NFI2 cycle, measurement date from the field survey or photogrammetric interpretation
	'CZ' AS country,
	'CZ-NFI2(2011-2015)' AS inventory_campaign,
	concat(t1.myear,'-',t1.mseason) AS reference_year_set,
	'CZ-STS-SNIL1' AS strata_set,
	'CZ-SNIL1' AS stratum,
	concat('y', t1.myear - 2010,'_s',t1.mseason) AS panel,
	concat('duplex_',t1.id_duplex) AS cluster,
	concat('pid_', t1.pid) AS plot,
	to_char(t2.measurement_date, 'YYYY-MON-DD') AS measurement_date,
	CASE WHEN t3.id IS NOT NULL THEN 'field survey date' ELSE 'date of photogrammetric interpretation' END AS comment
FROM 
gisdata_internal.f_p_nfi1_grid AS t1
INNER JOIN 
	analytical.t_plots_field_data AS t2
ON t1.pid = t2.pid AND t2.nfi_cycle = 200 AND t2.is_latest AND t2.version = 100
LEFT JOIN 
	field_data_nfi2_gnfi1.t_plots AS t3
ON t3.pid = t1.pid
WHERE t1.nuts4 IS NOT NULL -- 39 432 plots

UNION ALL

SELECT -- NFI2 grid and survey, measurment date from the field survey or photogrammetric interpretation  
	'CZ' AS country,
	'CZ-NFI2(2011-2015)' AS inventory_campaign,
	concat(t1.myear + 2010,'-', t1.mseason) AS reference_year_set, 
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	concat('y', myear,'_s',mseason,'_s2', CASE WHEN s2a THEN 'a' WHEN s2b THEN 'b' END) AS panel,
	concat('pid_',t1.pid) AS cluster,
	concat('pid_',t1.pid) AS plot,
	to_char(t2.measurement_date, 'YYYY-MON-DD') AS measurement_date,
	CASE WHEN t3.id IS NOT NULL THEN 'field survey date' ELSE 'date of photogrammetric interpretation' END AS comment
FROM 
	gisdata_internal.f_p_nfi2_grid AS t1
INNER JOIN 
	analytical.t_plots_field_data AS t2
ON t1.pid = t2.pid AND t2.nfi_cycle = 200 AND t2.is_latest AND t2.version = 200
LEFT JOIN 
	field_data_nfi2_gnfi2.t_plots AS t3
ON t3.pid = t1.pid
WHERE t1.s2a -- 19 727 plots 

UNION ALL 

SELECT -- NFI2 grid and NFI3 survey, measurment date from the field survey or photogrammetric interpretation  
	'CZ' AS country,
	'CZ-NFI3(2016-2020)' AS inventory_campaign,
	concat(t1.myear + 2015,'-', t1.mseason) AS reference_year_set, 
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	concat('y', myear,'_s',mseason,'_s2', CASE WHEN s2a THEN 'a' WHEN s2b THEN 'b' END) AS panel,
	concat('pid_',t1.pid) AS cluster,
	concat('pid_',t1.pid) AS plot,
	to_char(t2.measurement_date, 'YYYY-MON-DD') AS measurement_date,
	CASE WHEN t3.id IS NOT NULL THEN 'field survey date' ELSE 'date of photogrammetric interpretation' END AS comment
FROM 
	gisdata_internal.f_p_nfi2_grid AS t1
INNER JOIN 
	analytical.t_plots_field_data AS t2
ON t1.pid = t2.pid AND t2.nfi_cycle = 300 AND t2.is_latest AND t2.version = 100
LEFT JOIN 
	field_data_nfi3_gnfi2.t_plots AS t3
ON t3.pid = t1.pid
WHERE t1.s2 -- 37 407 plots