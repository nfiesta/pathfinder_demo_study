SELECT
	'CZ' AS country,
	'CZ-STS-SNIL1' AS strata_set,
	'CZ-SNIL1' AS stratum,
	concat('y', myear - 2010,'_s',mseason) AS panel,
	concat('duplex_',id_duplex) AS cluster,
	concat('pid_', pid) AS plot,
	'1km-INSPIRE' AS cell_collection,
	concat('1kmN', floor(ST_Y(ST_Transform(geom,3035))/1000),'E',floor(ST_X(ST_Transform(geom,3035))/1000)) AS cell,
	NULL AS comment
FROM 
gisdata_internal.f_p_nfi1_grid WHERE nuts4 IS NOT NULL 

UNION ALL

SELECT 
	'CZ' AS country,
	'CZ-STS-SNIL2' AS strata_set,
	'CZ-SNIL2' AS stratum,
	concat('y', myear,'_s',mseason,'_s2', CASE WHEN s2a THEN 'a' WHEN s2b THEN 'b' END) AS panel,
	concat('pid_',pid) AS cluster,
	concat('pid_',pid) AS plot,
	'1km-INSPIRE' AS cell_collection,
	concat('1kmN', floor(ST_Y(ST_Transform(geom,3035))/1000),'E',floor(ST_X(ST_Transform(geom,3035))/1000)) AS cell,
	NULL AS comment
FROM 
	gisdata_internal.f_p_nfi2_grid 
WHERE s2;