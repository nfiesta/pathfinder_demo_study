--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

DROP SCHEMA IF EXISTS csv_country CASCADE;
CREATE SCHEMA csv_country;
grant usage on schema csv_country to public;

----------------------------------------------------------------------------NFI spatial design CSVs
--derived from https://gitlab.com/nfiesta/nfiesta_sdesign/-/blob/main/sql/csv.sql

CREATE FOREIGN TABLE csv_country.strata_sets (
	country		character varying(20)		not null,
	strata_set	character varying(20)		not null,
	label		character varying(120)		not null,
	comment		text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/strata_sets.csv');

CREATE FOREIGN TABLE csv_country.strata (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	label			character varying(120)		not null,
	geometry		text				not null,
	area_ha			double precision,
	frame_area_ha		double precision,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/strata.csv' );

CREATE FOREIGN TABLE csv_country.cluster_configurations (
	country			character varying(20)		not null,
	cluster_configuration	character varying(20)		not null,
	label			character varying(120)		not null,
	cluster_design		boolean				not null,
	cluster_geom		text,
	cluster_rotation	boolean				not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/cluster_configurations.csv');

CREATE FOREIGN TABLE csv_country.panels (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	cluster_configuration	character varying(20)		not null,
	panel			character varying(20)		not null,
	label			character varying(120)		not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/panels.csv' );

CREATE FOREIGN TABLE csv_country.clusters (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	sampling_weight_ha	float				not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/clusters.csv');

CREATE FOREIGN TABLE csv_country.plots_pathfinder_ds (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	plot			character varying(20)		not null,
	/*plot_geometry		text,
	coordinates_degraded	boolean,*/
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/plots.csv' );

CREATE VIEW csv_country.plots AS
SELECT
	country,
	strata_set,
	stratum,
	panel,
	cluster,
	plot,
	NULL::text as plot_geometry,
	NULL::boolean as coordinates_degraded,
	comment
FROM csv_country.plots_pathfinder_ds;

----------------------------------------------------------------------------NFI temporal design CSVs

CREATE FOREIGN TABLE csv_country.inventory_campaigns (
	country				character varying(20)		not null,
	inventory_campaign		character varying(20)		not null,
	label				character varying(120)		not null,
	status_variables		boolean				not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/inventory_campaigns.csv' );

CREATE FOREIGN TABLE csv_country.reference_year_sets (
	country				character varying(20)		not null,
	inventory_campaign		character varying(20)		not null,
	reference_year_set		character varying(20)		not null,
	label				character varying(120)		not null,
	status_variables		boolean				not null,
	reference_date_begin		date				not null,
	reference_date_end		date				not null,
	inventory_campaign_begin	character varying(20),
	reference_year_set_begin	character varying(20),
	inventory_campaign_end 		character varying(20),
	reference_year_set_end 		character varying(20),
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/reference_year_sets.csv' );

CREATE FOREIGN TABLE csv_country.refyearset2panel (
	country			character varying(20)		not null,
	inventory_campaign	character varying(20)		not null,
	reference_year_set	character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/reference_year_sets_panels_associations.csv' );

CREATE FOREIGN TABLE csv_country.plot_measurement_dates (
	country			character varying(20)		not null,
	inventory_campaign	character varying(20)		not null,
	reference_year_set	character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	plot			character varying(20)		not null,
	measurement_date	date,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/plot_measurement_dates.csv' );

----------------------------------------------------------------------------nfiesta_pg part

CREATE FOREIGN TABLE csv_country.plot_cell_associations (
	country			character varying(20)	not null,
	strata_set		character varying(20)	not null,
	stratum			character varying(20)	not null,
	panel			character varying(20)	not null,
	cluster			character varying(20)	not null,
	plot			character varying(20)	not null,
	cell_collection		character varying(20)	not null,
	cell			character varying(20)	not null,
	comment 		text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/plot_cell_associations.csv' );

CREATE FOREIGN TABLE csv_country.plot_target_data (
	country			character varying(20)	not null,
	inventory_campaign	character varying(20)	not null,
	reference_year_set	character varying(20)	not null,
	strata_set		character varying(20)	not null,
	stratum			character varying(20)	not null,
	panel			character varying(20)	not null,
	cluster			character varying(20)	not null,
	plot			character varying(20)	not null,
	target_variable		character varying(30)	not null,
	sub_population		character varying(30)	not null,
	sub_population_category	character varying(30)	not null,
	area_domain		character varying(30)	not null,
	area_domain_category	character varying(30)	not null,
	value			double precision not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/plot_target_data.csv' );

CREATE FOREIGN TABLE csv_country.plot_auxiliary_data (
	country				character varying(20)	not null,
	strata_set			character varying(20)	not null,
	stratum				character varying(20)	not null,
	panel				character varying(20)	not null,
	cluster				character varying(20)	not null,
	plot				character varying(20)	not null,
	tile				character varying(50),
	auxiliary_variable		character varying(30)	not null,
	auxiliary_variable_category	character varying(30)	not null,
	value				double precision	not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename '/home/vagrant/pathfinder_demo_study/csv_import/countries/example_data_1_0/plot_auxiliary_data.csv' );
